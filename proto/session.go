package proto

import (
	"gopkg.in/gorp.v1"
	"time"
)

type Session struct {
	Id      int    `db:"id"` // corresponds to user ID for seshmgr
	Key     string `db:"key"`
	Expires int64  `db:"expires"`
}

func (sesh *Session) FromKey(key string, dbmap *gorp.DbMap) error {
	err := dbmap.SelectOne(&sesh, "SELECT `id`,`key`,`expires` FROM sessions WHERE `key`=? AND `expires`>?", key, time.Now().Unix())
	return err
}

// doesn't check against timestamp - do not use for session validation
func (sesh *Session) FromIdWithoutTimestamp(id int, dbmap *gorp.DbMap) error {
	err := dbmap.SelectOne(&sesh, "SELECT `id`,`key`,`expires` FROM sessions WHERE `id`=? AND `expires`>?", id, time.Now().Unix())
	return err
}

func (sesh *Session) Create(dbmap *gorp.DbMap) error {
	return dbmap.Insert(sesh)
}

func (sesh *Session) Delete(dbmap *gorp.DbMap) error {
	_, err := dbmap.Delete(sesh)
	return err
}

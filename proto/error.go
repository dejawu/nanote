package proto

import "net/http"

type ProtoError struct {
	Err error // the actual error
	DisplayMessage string // the error message we can give publicly
	StatusCode int
}

// implement DisplayableError interface
func (err ProtoError) Error() string {
	return err.Err.Error()
}

// "upgrades" a normal error into a ProtoError
// for use if the error message and display message the same. This happens pretty often.
func MakeProtoError(err error) ProtoError {
	return ProtoError{
		Err: err,
		DisplayMessage: err.Error(),
		StatusCode: http.StatusBadRequest,
	}
}
package proto

import (
	"database/sql"
	"errors"

	"gopkg.in/gorp.v1"
	"net/http"
)

var ErrCategoryNameExists = errors.New("A category with this name already exists.")
var ErrCategoryDoesNotExist = errors.New("This category does not exist.") // used in proto.Note
var ErrCategoryNotOwned = errors.New("User does not own this category.")

type Category struct {
	Id    int    `db:"id" json:"cat_id"`
	Name  string `db:"name" json:"cat_name"`
	Owner int    `db:"owner" json:"cat_owner"`
}

func (c *Category) FromId(id int, owner int, dbmap *gorp.DbMap) error {
	err := dbmap.SelectOne(c, "SELECT id, name, owner FROM categories WHERE id=? AND owner=?", id, owner)
	return err
}

// Creates the category with given parameters, and saves it to the database.
func (c *Category) Create(name string, u *User, dbmap *gorp.DbMap) error {
	// check for one with a duplicate name
	count, err := dbmap.SelectInt("SELECT COUNT(id) FROM categories WHERE `name`=? AND owner=?", name, u.Id)
	if count != 0 {
		return MakeProtoError(ErrCategoryNameExists)
	}

	if err != nil && err != sql.ErrNoRows {
		return ProtoError{
			Err: err,
			DisplayMessage: "Internal server error.",
			StatusCode: http.StatusInternalServerError,
		}
	}

	// insert new entry into category table
	c.Name = name
	c.Owner = u.Id

	err = dbmap.Insert(c)
	return err
}

func (c *Category) Update(name string, u *User, dbmap *gorp.DbMap) error {
	if !c.verifyOwnership(u.Id, dbmap) {
		return ProtoError{
			Err: ErrCategoryNotOwned,
			DisplayMessage: "Category does not exist.",
			StatusCode: http.StatusBadRequest,
		}
	}

	// check for one with a duplicate name
	count, err := dbmap.SelectInt("SELECT COUNT(id) FROM categories WHERE `name`=? AND owner=?", name, u.Id)
	if count != 0 {
		return MakeProtoError(ErrCategoryNameExists)
	}

	if err != nil {
		return ProtoError{
			Err: err,
			DisplayMessage: "Internal server error.",
			StatusCode: http.StatusBadRequest,
		}
	}

	c.Name = name

	_, err = dbmap.Update(c)
	return err
}

func (c *Category) Delete(u *User, dbmap *gorp.DbMap) error {
	if !c.verifyOwnership(u.Id, dbmap) {
		return ProtoError{
			Err: ErrCategoryNotOwned,
			DisplayMessage: "Category does not exist.",
			StatusCode: http.StatusBadRequest,
		}
	}

	_, err := dbmap.Exec("DELETE FROM categories WHERE id=?", c.Id)
	_, err = dbmap.Exec("DELETE FROM catmap WHERE cat_id=?", c.Id)

	if err != nil {
		return ProtoError{
			Err: err,
			DisplayMessage: "Internal server error.",
			StatusCode: http.StatusInternalServerError,
		}
	} else {
		return nil
	}
}

func (c *Category) List(notes *[]Note, u *User, dbmap *gorp.DbMap, options ...string) error {
	if !c.verifyOwnership(u.Id, dbmap) {
		return ProtoError{
			Err: ErrCategoryNotOwned,
			DisplayMessage: "Category does not exist.",
			StatusCode: http.StatusBadRequest,
		}
	}

	var err error
	var filter string

	if len(options) >= 1 {
		filter = options[0]
	} else {
		filter = "none"
	}

	switch filter{
	case "none":
		_, err = dbmap.Select(notes, "SELECT p.id,p.title,p.slug,p.privacy,p.created,p.updated FROM catmap cm, notes p WHERE cm.note_id = p.id AND cm.cat_id = ?", c.Id)
	case "visible":
		_, err = dbmap.Select(notes, "SELECT p.id,p.title,p.slug,p.privacy,p.created,p.updated FROM catmap cm, notes p WHERE cm.note_id = p.id AND cm.cat_id = ? AND p.privacy = 'public'", c.Id)
	default:
		return MakeProtoError(ErrInvalidFilter)
	}

	if err != nil && err != sql.ErrNoRows {
		return ProtoError{
			Err: err,
			DisplayMessage: "Internal server error.",
			StatusCode: http.StatusInternalServerError,
		}
	} else {
		return nil
	}
}

func (c *Category) verifyOwnership(uId int, dbmap *gorp.DbMap) bool {
	count, err := dbmap.SelectInt("SELECT COUNT(id) FROM categories WHERE `id`=? AND owner=?", c.Id, uId)

	if err != nil {
		return false
	}

	if count != 1 {
		return false
	} else {
		return true
	}
}

// returns true if this category has at least one public (visible) note, false otherwise.
func (c *Category) IsPubliclyVisible(u *User, dbmap *gorp.DbMap) bool {
	notes := make([]Note, 0)
	c.List(&notes, u, dbmap)

	// it is more efficient to simply push to a new array and erase that
	for i := 0; i < len(notes); i++ {
		if notes[i].Privacy == "public" {
			return true
		}
	}

	return false
}

package proto

import (
	"database/sql"
	"errors"
	"gopkg.in/gorp.v1"
	"regexp"
	"strings"
	"time"
	"net/http"
)

var ErrNoteDoesNotExist = errors.New("Note does not exist.")
var ErrNoteURLExists = errors.New("A note with this URL already exists.")
var ErrNoteURLInvalid = errors.New("URL is invalid.")
var ErrNoteNotOwned = errors.New("User does not own note.")

var ErrNoteAlreadyHasCat = errors.New("Note already has this category.")

// these errors are used in User and Category, not in Note. They apply to Notes, so they're here.
var ErrInvalidNoteSort = errors.New("Invalid sort option.")
var ErrInvalidNoteFilter = errors.New("Invalid filter option.")

type Note struct {
	Id      int    `db:"id" json:"id"`
	Owner   int    `db:"owner" json:"owner"`
	Privacy string `db:"privacy" json:"privacy"`
	Title   string `db:"title" json:"title"`
	Slug    string `db:"slug" json:"slug"`
	Content string `db:"content" json:"content"`
	Created int    `db:"created" json:"created"`
	Updated int    `db:"updated" json:"updated"`

	sortNotesBy string `db:"-"`
}

// populate p data given a note ID
func (p *Note) FromId(id int, owner int, dbmap *gorp.DbMap) error {
	err := dbmap.SelectOne(p, "SELECT id,owner,privacy,title,slug,content,created,updated FROM notes WHERE id=? AND owner=?", id, owner)
	return err
}

// populate p data given a slug string
func (p *Note) FromSlug(slug string, owner int, dbmap *gorp.DbMap) error {
	err := dbmap.SelectOne(p, "SELECT id,owner,privacy,title,slug,content,created,updated FROM notes WHERE owner=? AND `slug`=?", owner, strings.ToLower(slug))
	return err
}

// Initializes the contents of the note object, and attributes it to the given user.
func (n *Note) Create(u *User, dbmap *gorp.DbMap) error {
	n.Title = "Untitled note"
	n.Slug = ""
	n.Content = ""
	n.Privacy = "private" // always create note private! Create() does not check for URL validity
	n.Created = int(time.Now().Unix())
	n.Updated = int(time.Now().Unix())
	n.Owner = u.Id

	err := dbmap.Insert(n)
	return err
}

func (p *Note) Update(u *User, dbmap *gorp.DbMap) error {
	if !p.verifyOwnership(u.Id, dbmap) {
		return ProtoError{
			Err: ErrNoteNotOwned,
			DisplayMessage: "Note does not exist.",
			StatusCode: http.StatusBadRequest,
		}
	}

	// URL slug only matters if the note is publicly accessible
	if !validSlug(strings.ToLower(p.Slug)) && p.Privacy != "private" {
		return MakeProtoError(ErrNoteURLInvalid) // invalid URL
	}

	count, err := dbmap.SelectInt("SELECT id FROM notes WHERE `slug`=? AND owner=? AND id != ?", p.Slug, u.Id, p.Id)
	if count != 0 && p.Privacy != "private" {
		return MakeProtoError(ErrNoteURLExists)
	}

	_, err = dbmap.Exec("UPDATE notes SET privacy=?, title=?, slug=?, content=?, updated=? WHERE id=? AND owner=?", p.Privacy, p.Title, p.Slug, p.Content, p.Updated, p.Id, u.Id)
	if err != nil {
		return ProtoError{
			Err: err,
			DisplayMessage: "Internal server error.",
			StatusCode: http.StatusInternalServerError,
		}
	}

	return nil
}

func (p *Note) Delete(u *User, dbmap *gorp.DbMap) error {
	if !p.verifyOwnership(u.Id, dbmap) {
		return ProtoError{
			Err: ErrNoteNotOwned,
			DisplayMessage: "Note does not exist.",
			StatusCode: http.StatusBadRequest,
		}
	}

	// manually exec delete where note = note and owner = user id (should affect exactly one row)
	_, err := dbmap.Exec("DELETE FROM notes WHERE id=? AND owner=?", p.Id, u.Id)
	if err != nil {
		return ProtoError{
			Err: err,
			DisplayMessage: "Internal server error.",
			StatusCode: http.StatusInternalServerError,
		}
	}

	// delete all catmap entries
	_, err = dbmap.Exec("DELETE FROM catmap WHERE note_id=?", p.Id)
	if err != nil {
		return ProtoError{
			Err: err,
			DisplayMessage: "Internal server error.",
			StatusCode: http.StatusInternalServerError,
		}
	}

	return nil
}

func (p *Note) ListCats(cats *[]Category, u *User, dbmap *gorp.DbMap) error {
	if !p.verifyOwnership(u.Id, dbmap) {
		return ProtoError{
			Err: ErrNoteNotOwned,
			DisplayMessage: "Note does not exist.",
			StatusCode: http.StatusBadRequest,
		}
	}

	_, err := dbmap.Select(cats, "SELECT cm.cat_id Id, cg.name Name FROM catmap cm, categories cg WHERE cm.cat_id = cg.id AND cm.note_id = ? ORDER BY cm.cat_id ASC", p.Id)

	if err != nil && err != sql.ErrNoRows {
		return ProtoError{
			Err: err,
			DisplayMessage: "Internal server error.",
			StatusCode: http.StatusInternalServerError,
		}
	} else {
		return nil
	}
}

func (p *Note) AddCat(category int, u *User, dbmap *gorp.DbMap) error {
	if !p.verifyOwnership(u.Id, dbmap) {
		return ProtoError{
			Err: ErrNoteNotOwned,
			DisplayMessage: "Note does not exist.",
			StatusCode: http.StatusBadRequest,
		}
	}

	count, err := dbmap.SelectInt("SELECT COUNT(id) FROM catmap WHERE note_id=? AND cat_id=?", p.Id, category)

	if count != 0 {
		return MakeProtoError(ErrNoteAlreadyHasCat)
	}

	if err != nil && err != sql.ErrNoRows {
		return ProtoError{
			Err: err,
			DisplayMessage: "Internal server error.",
			StatusCode: http.StatusInternalServerError,
		}
	}

	cm := Catmap{}

	cm.CatId = category
	cm.NoteId = p.Id

	err = dbmap.Insert(&cm)
	if err != nil {
		return ProtoError{
			Err: err,
			DisplayMessage: "Internal server error.",
			StatusCode: http.StatusInternalServerError,
		}
	}

	return nil
}

func (p *Note) RemoveCat(category int, u *User, dbmap *gorp.DbMap) error {
	if !p.verifyOwnership(u.Id, dbmap) {
		return ProtoError{
			Err: ErrNoteNotOwned,
			DisplayMessage: "Note does not exist.",
			StatusCode: http.StatusBadRequest,
		}
	}

	// make sure note exists
	count, err := dbmap.SelectInt("SELECT COUNT(id) FROM notes WHERE id=? AND owner=?", p.Id, u.Id)
	if count != 1 {
		return MakeProtoError(ErrNoteDoesNotExist)
	}

	// make sure category exists
	count, err = dbmap.SelectInt("SELECT COUNT(id) FROM categories WHERE id=? AND owner=?", category, u.Id)
	if count != 1 {
		return MakeProtoError(ErrCategoryDoesNotExist)
	}

	// check for one with a duplicate name
	count, err = dbmap.SelectInt("SELECT COUNT(id) FROM catmap WHERE note_id=? AND cat_id=?", p.Id, category)
	if count != 1 {
		// note does not have category
		return MakeProtoError(errors.New("Note does not have this category."))
	}
	if err != nil {
		return ProtoError{
			Err: err,
			DisplayMessage: "Internal server error.",
			StatusCode: http.StatusInternalServerError,
		}
	}

	_, err = dbmap.Exec("DELETE from catmap where note_id=? AND cat_id=?", p.Id, category)
	if err != nil {
		return ProtoError{
			Err: err,
			DisplayMessage: "Internal server error.",
			StatusCode: http.StatusInternalServerError,
		}
	}

	return nil
}

// returns true/false whether the slug is valid and URL-safe.
// this does NOT return a URL-safe slug - that is the responsibility of the client!
// it only tells whether the slug is URL-safe or not.
func validSlug(slug string) bool {
	matched, err := regexp.MatchString("^[a-z0-9]([a-z0-9-]*[a-z0-9])?$", strings.ToLower(slug))
	if err != nil {
		return false
	}
	return matched
}

func (p *Note) verifyOwnership(uId int, dbmap *gorp.DbMap) bool {
	// verify ownership
	count, err := dbmap.SelectInt("SELECT COUNT(id) FROM notes WHERE id=? AND owner=?", p.Id, uId)
	if err != nil {
		return false
	}
	if count != 1 {
		return false
	} else {
		return true
	}
}

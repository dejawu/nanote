package proto

import (
	"crypto/rand"
	"crypto/sha256"
	"encoding/base64"
	"errors"
	mrand "math/rand"
	"regexp"
	"time"

	"gopkg.in/gorp.v1"

	"golang.org/x/crypto/pbkdf2"
	"net/http"
)

type User struct {
	Id       int    `db:"id"`
	Username string `db:"username"`
	Private  bool   `db:"private"`
	Email    string `db:"email"`
	Passhash string `db:"passhash"`
	Passsalt string `db:"passsalt"`
	ApiKey   string `db:"api_key"`
	Verify   string `db:"verify"`
	Recover  string `db:"recover"`
	StripeId string `db:"stripe_id"`
}

// populates user given a username
func (u *User) FromUsername(username string, dbmap *gorp.DbMap) error {
	err := dbmap.SelectOne(u, "SELECT id,username,email,private FROM users WHERE `username`=? AND verify=''", username)
	return err
}

// populates given an id
func (u *User) FromId(id int, dbmap *gorp.DbMap) error {
	err := dbmap.SelectOne(u, "SELECT id,username,email,private FROM users WHERE id=? AND verify=''", id)
	return err
}

// populates user from API key
func (u *User) FromApiKey(username string, apikey string, dbmap *gorp.DbMap) error {
	err := dbmap.SelectOne(u, "SELECT id,username,email,private FROM users WHERE `username`=? AND BINARY `api_key`=? AND verify=''", username, apikey)
	return err
}

// fetch for more data than FromUsername would, use for settings
func (u *User) FromUsernameForSettings(username string, dbmap *gorp.DbMap) error {
	err := dbmap.SelectOne(u, "SELECT username,email,private,api_key,stripe_id FROM users WHERE `username`=? AND verify=''", username)
	return err
}

/*
populates user, but DOES NOT INSERT. Must already have the following fields:
	Username
	Email
*/
func (u *User) FromNew(password string, dbmap *gorp.DbMap) error {
	// check username for invalid characters
	if regexp.MustCompile(`[^a-zA-Z0-9]+`).MatchString(u.Username) {
		return MakeProtoError(errors.New("Usernames can only contain letters and numbers."))
	}

	// check username length
	if len(u.Username) > 50 {
		return MakeProtoError(errors.New("This username is too long."))
	}

	// check username for availability
	count, err := dbmap.SelectInt("SELECT count(id) FROM users WHERE `username`=?", u.Username)
	if count != 0 {
		return MakeProtoError(errors.New("This username is not available."))
	}

	// check email address for availability
	count, err = dbmap.SelectInt("SELECT count(id) FROM users WHERE `email`=?", u.Email)
	if count != 0 {
		return MakeProtoError(errors.New("This email address is already in use."))
	}

	// generate password salt and hash
	salt, err := generateRandomBytes()
	if err != nil {
		return ProtoError{
			Err: errors.New("Error encountered when generating salt: " + err.Error()),
			DisplayMessage: "Internal server error.",
			StatusCode: http.StatusInternalServerError,
		}
	}

	u.Passsalt = base64.URLEncoding.EncodeToString(salt)
	u.Passhash = generateSHA256Key(password, salt)

	// by default private mode is off
	u.Private = false

	// generate random string for verification
	mrand.Seed(time.Now().UTC().UnixNano())
	const charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
	verifyCode := make([]byte, 16)

	for i := 0; i < 16; i++ {
		verifyCode[i] = charset[mrand.Intn(len(charset))]
	}

	u.Verify = string(verifyCode)

	// generate random API key
	apiKey := make([]byte, 64)

	for i := 0; i < 64; i++ {
		apiKey[i] = charset[mrand.Intn(len(charset))]
	}

	u.ApiKey = string(apiKey)

	return err
}

var ErrInvalidFilter = errors.New("Invalid filter type.")

func (u *User) Notes(notes *[]Note, dbmap *gorp.DbMap, options ...string) error {
	var err error
	var filter string

	if len(options) >= 1 {
		filter = options[0]
	} else {
		filter = "none"
	}

	switch filter{
	case "none":
		_, err = dbmap.Select(notes, "SELECT id,privacy,title,slug,created,updated FROM notes WHERE owner=? ORDER BY `updated` DESC", u.Id)
	case "uncategorized":
		_, err = dbmap.Select(notes, "SELECT notes.id,privacy,title,slug,created,updated FROM notes LEFT JOIN catmap ON catmap.note_id = notes.id WHERE catmap.note_id IS NULL AND notes.owner=? ORDER BY `updated` DESC", u.Id)
	// used for listing on profile
	case "public":
		_, err = dbmap.Select(notes, "SELECT id,title,slug,created,updated FROM `notes` WHERE (owner=? AND privacy='public') ORDER BY `title` ASC", u.Id)
	default:
		return MakeProtoError(ErrInvalidFilter)
	}

	if err != nil {
		return ProtoError{
			Err: err,
			DisplayMessage: "Internal server error.",
			StatusCode: http.StatusInternalServerError,
		}
	}
	return nil
}

func (u *User) Categories(categories *[]Category, dbmap *gorp.DbMap, options ...string) error {
	var err error
	if len(options) == 0 { _, err = dbmap.Select(categories, "SELECT name,id,owner FROM categories WHERE owner=? ORDER BY name ASC", u.Id)
	} else {
		filter := options[0]
		switch filter {
		case "visible":
			// make a separate array containing all categories, then push public ones to the used value
			allCats := make([]Category, 0)
			_, err = dbmap.Select(&allCats, "SELECT name,id,owner FROM categories WHERE owner=? ORDER BY name ASC", u.Id)

			for i := 0; i < len(allCats); i++ {
				if allCats[i].IsPubliclyVisible(u, dbmap) {
					// push to categories
					*categories = append(*categories, allCats[i])
				}
			}
		default:
			return MakeProtoError(ErrInvalidFilter)
		}
	}
	return err
}

// only updates Private setting right now.
func (u *User) Update(dbmap *gorp.DbMap) error {
	_, err := dbmap.Exec("UPDATE users SET private=? WHERE id=?", u.Private, u.Id)
	return err
}

// username auth stuff

// thank you Jorge
func generateRandomBytes() ([]byte, error) {
	b := make([]byte, 32)

	_, err := rand.Read(b)
	if err != nil {
		return nil, err
	}

	return b, nil
}

func generateSHA256Key(plain string, salt []byte) string {
	key := pbkdf2.Key([]byte(plain),
		salt,
		2048,
		32,
		sha256.New)
	out := base64.URLEncoding.EncodeToString(key)
	return out[:len(out) - 4]
}

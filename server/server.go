package server

import (
	"html/template"
	"net/http"
	"os"
	"strconv"
	"strings"

	"github.com/gorilla/mux"
	"gopkg.in/gorp.v1"

	"github.com/op/go-logging"

	"kywu.org/nanote/proto"
	"kywu.org/nanote/server/api"
	"kywu.org/nanote/seshmgr"
)

// keys for secure cookies. eventually will go in their own file

type Server struct {
	dbmap     *gorp.DbMap
	api       *api.Api // may need to rename package later; this is confusing
	rt        *mux.Router
	domain    string
	sesh      *seshmgr.SessionManager
	env       string
	stripeKey string
	log       *logging.Logger
}

func Init(dbmap *gorp.DbMap, domain string, secrets map[string]string) *Server {
	r := mux.NewRouter()
	r.StrictSlash(true)
	sesh := seshmgr.Init(dbmap)

	s := &Server{
		dbmap:     dbmap,
		api:       nil,
		rt:        r,
		domain:    domain,
		sesh:      sesh,
		env:       secrets["env"],
		stripeKey: secrets["stripe_apiKey"],
		log: logging.MustGetLogger("server"),
	}

	s.log.SetBackend(logging.AddModuleLevel(logging.NewBackendFormatter(
		logging.NewLogBackend(os.Stderr, "", 0),
		logging.MustStringFormatter(`%{shortfile} %{callpath} %{color} %{level} %{color:reset}
	%{message}`),
	)))

	s.api = api.Init(dbmap, sesh, secrets, s.log)

	s.log.Debug("Initializing server.")

	// subdomains
	subR := r.MatcherFunc(func(r *http.Request, rm *mux.RouteMatch) bool {
		return strings.Split(r.Host, ".")[0] != strings.Split(domain, ".")[0] // if the first URL element is not the root domain, then it's a subdomain.
	}).Subrouter()

	subR.HandleFunc("/", s.subFront)
	subR.HandleFunc("/n/{note}", s.subNote)

	subR.HandleFunc("/c/{cat}", s.subCat)

	subR.HandleFunc("/static/{type}/{file}", s.handleStatic)
	// must be last subdomain function
	subR.HandleFunc("/{_dummy:.*}", s.handleNotFound)

	// "my" methods
	myR := r.PathPrefix("/my").Methods("GET").Subrouter()

	myR.HandleFunc("/dash", s.myDash)
	myR.HandleFunc("/dashboard", func(w http.ResponseWriter, r *http.Request) {
		http.Redirect(w, r, "/my/dash", http.StatusMovedPermanently)
	})

	// create new note and opens editor with that note
	myR.HandleFunc("/editor/new", func(w http.ResponseWriter, r *http.Request) {
		u, err := sesh.User(r)
		if err != nil {
			http.Redirect(w, r, "/", http.StatusFound)
			return
		}

		n := proto.Note{}
		err = n.Create(u, dbmap)

		// redirect to editor for note
		http.Redirect(w, r, "/my/editor/" + strconv.Itoa(n.Id), http.StatusFound)
	})
	// open editor with note
	myR.HandleFunc("/editor/{note}", s.myEditor)
	// create note with category, then open editor with that note
	myR.HandleFunc("/editor/new/c/{cat}", func(w http.ResponseWriter, r *http.Request) {
		u, err := sesh.User(r)
		if err != nil {
			http.Redirect(w, r, "/", http.StatusFound)
			return
		}

		// get category
		catId, err := strconv.Atoi(mux.Vars(r)["cat"])
		if err != nil {
			s.log.Warning("Failed to fetch category list for note with preset category: " + err.Error())
			http.Redirect(w, r, "/my/dash", http.StatusFound)
			return
		}

		c := proto.Category{}
		err = c.FromId(catId, u.Id, dbmap)

		// category is invalid for any reason
		if err != nil {
			s.log.Warning("User sent to new note with a preset category that is invalid: " + err.Error())
			http.Redirect(w, r, "/my/dash", http.StatusFound)
			return
		}

		// create note
		n := proto.Note{}
		err = n.Create(u, dbmap)
		if err != nil {
			http.Redirect(w, r, "/my/dash", http.StatusFound)
			return
		}

		n.AddCat(c.Id, u, dbmap)

		http.Redirect(w, r, "/my/editor/" + strconv.Itoa(n.Id), http.StatusFound)
	})

	myR.HandleFunc("/editor", func(w http.ResponseWriter, r *http.Request) {
		http.Redirect(w, r, "/my/editor/new", http.StatusMovedPermanently)
	})

	// catch any patterns that didn't match the above
	myR.HandleFunc("/editor/{_dummy:.*}", func(w http.ResponseWriter, r *http.Request) {
		http.Redirect(w, r, "/my/editor/new", http.StatusMovedPermanently)
	})
	myR.HandleFunc("/editor/{note}/{_dummy:.*}", func(w http.ResponseWriter, r *http.Request) {
		note := mux.Vars(r)["note"]
		http.Redirect(w, r, "/my/editor/" + note, http.StatusMovedPermanently)
	})

	// redirect to user's profile
	myR.HandleFunc("/profile", func(w http.ResponseWriter, r *http.Request) {
		if s.sesh.Check(r) {
			u, err := s.sesh.User(r)

			if err != nil {
				s.log.Error("Error sending logged-in user to their own profile: " + err.Error())
				http.Redirect(w, r, "/my/dash", http.StatusFound)
			} else {
				http.Redirect(w, r, "https://" + u.Username + "." + domain, http.StatusFound)
			}
		} else {
			s.sesh.End(r, w)
			http.Redirect(w, r, "/", http.StatusFound)
		}
	})

	myR.HandleFunc("/settings", s.mySettings)

	r.NotFoundHandler = http.HandlerFunc(s.handleNotFound)

	// api routing
	apiR := r.PathPrefix("/api").Methods("POST").Subrouter()
	s.api.Handle(apiR)

	// registration note
	r.HandleFunc("/register", func(w http.ResponseWriter, r *http.Request) {
		if s.sesh.Check(r) {
			http.Redirect(w, r, "/my/dash", http.StatusFound)
			return
		} else {
			pageData := map[string]interface{}{
				"title":  "Register",
				"authed": false,
				"css": []string{
					"lib.evil-icons",
					"register",
				},
				"js": []string{
					"lib.evil-icons",
					"register",
				},
			}
			tpl, err := template.ParseFiles("html/register.html", "html/head.html", "html/header.html")

			if err != nil {
				s.log.Error("Error rendering template: " + err.Error())
				s.handleInternalServerError(w, r)
				return
			}

			err = tpl.Execute(w, pageData)
			if err != nil {
				s.log.Error("Error rendering register template: " + err.Error())
			}
		}
	})

	r.HandleFunc("/verify/{email}&{code}", func(w http.ResponseWriter, r *http.Request) {
		if s.sesh.Check(r) {
			http.Redirect(w, r, "/my/dash", http.StatusFound)
			return
		} else {
			pageData := map[string]interface{}{
				"title":  "Verify account",
				"authed": false,
				"css": []string{
					"lib.evil-icons",
					"global",
				},
				"js": []string{
					"lib.evil-icons",
					"verify",
				},
				"email":         mux.Vars(r)["email"],
				"code":          mux.Vars(r)["code"],
			}
			tpl, err := template.ParseFiles("html/verify.html", "html/head.html", "html/header.html")

			if err != nil {
				s.log.Error("Error rendering template: " + err.Error())
				s.handleInternalServerError(w, r)
				return
			}

			err = tpl.Execute(w, pageData)
			if err != nil {
				s.log.Error("Error rendering verify template: " + err.Error())
			}
		}
	})

	// user has to provide the right username for the code
	r.HandleFunc("/recover/{code}", func(w http.ResponseWriter, r *http.Request) {
		if s.sesh.Check(r) {
			http.Redirect(w, r, "/my/dash", http.StatusFound)
			return
		}

		pageData := map[string]interface{}{
			"title":  "recover password",
			"authed": false,
			"css": []string{
				"lib.evil-icons",
				"recover",
			},
			"js": []string{
				"lib.evil-icons",
				"recover",
			},
			"code": mux.Vars(r)["code"],
		}
		tpl, err := template.ParseFiles("html/recover.html", "html/head.html", "html/header.html")

		if err != nil {
			s.log.Error("Error rendering template: " + err.Error())
			s.handleInternalServerError(w, r)
			return
		}

		err = tpl.Execute(w, pageData)
		if err != nil {
			s.log.Error("Error rendering recover template: " + err.Error())
		}
	})

	// root handlers should come last
	r.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		if s.sesh.Check(r) {
			// if logged in, go straight to dashboard
			http.Redirect(w, r, "/my/dash", http.StatusFound)
			return
		} else {
			err := s.sesh.End(r,w) // clean up lingering sessions
			if err != nil {
				s.log.Error("Failed to clean up lingering session: " + err.Error())
			}
			pageData := map[string]interface{}{
				"title":         "Nanote: no-nonsense note taking.",
				"authed":        false,
				"suppressTitle": true,
				"suppressModal": true,
				"css": []string{
					"lib.evil-icons",
					"lib.codemirror",
					"index",
				},
				"js": []string{
					"markdown",
					"lib.purify",
					"lib.evil-icons",
					"index",
					"lib.codemirror",
				},
			}
			tpl, err := template.ParseFiles("html/index.html", "html/head.html", "html/header.html")

			if err != nil {
				s.log.Error("Error rendering template: " + err.Error())
				s.handleInternalServerError(w, r)
				return
			}

			err = tpl.Execute(w, pageData)
			if err != nil {
				s.log.Error("Error rendering frontpage! " + err.Error())
			}
		}
	})

	// static resource files
	r.HandleFunc("/static/{type}/{file}", s.handleStatic)

	return s
}

func (s Server) Start() {
	http.Handle("/", s.rt)
	http.ListenAndServe(":8080", nil)
}

func (s Server) handleNotFound(w http.ResponseWriter, r *http.Request) {
	tpl, err := template.ParseFiles("html/error.html", "html/head.html", "html/header.html")

	if err != nil {
		s.log.Error("Error rendering template: " + err.Error())
		s.handleInternalServerError(w, r)
		return
	}

	user, _ := s.sesh.User(r)

	pageData := map[string]interface{}{
		"title":         "404 not found!",
		"suppressTitle": true,
		"authed":        s.sesh.Check(r),
		"css": []string{
			"error",
		},
		"js": []string{
			"auth",
		},
	}

	if pageData["authed"].(bool) {
		if err != nil {
			s.log.Error("User is authed, but could not retrieve user data: " + err.Error())
			s.handleInternalServerError(w, r)
			return
		} else {
			pageData["username"] = user.Username
		}
	}

	w.WriteHeader(http.StatusNotFound)
	err = tpl.Execute(w, pageData)
	if err != nil {
		s.log.Error("Error rendering 404 template: " + err.Error())
	}
}

func (s Server) handleError(w http.ResponseWriter, r *http.Request, title string, httpCode int) {
	tpl, err := template.ParseFiles("html/error.html", "html/head.html", "html/header.html")

	if err != nil {
		s.log.Error("Error rendering template: " + err.Error())
		s.handleInternalServerError(w, r)
		return
	}

	pageData := map[string]interface{}{
		"title":    title,
		"authed":   s.sesh.Check(r),
		"username": "",
		"css": []string{
			"error",
		},
		"js": []string{
			"auth",
		},
	}

	user, err := s.sesh.User(r)
	if err == nil {
		pageData["username"] = user.Username
		pageData["private"] = user.Private
	}

	w.WriteHeader(httpCode)
	err = tpl.Execute(w, pageData)
	if err != nil {
		s.log.Error("Error rendering error template: " + err.Error())
	}
}

func (s Server) handleInternalServerError(w http.ResponseWriter, r *http.Request) {
	s.handleError(w, r, "Internal server error.", http.StatusInternalServerError)
}

func (s Server) handleStatic(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)

	filepath := "static/" + params["type"] + "/" + params["file"]
	_, err := os.Stat(filepath)
	if err != nil {
		s.handleNotFound(w, r)
		return
	}

	http.ServeFile(w, r, "static/" + params["type"] + "/" + params["file"])
}

package server

import (
	"database/sql"
	"html/template"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/dustin/go-humanize"

	"github.com/stripe/stripe-go"
	"github.com/stripe/stripe-go/customer"

	"kywu.org/nanote/proto"
)

func (s Server) myDash(w http.ResponseWriter, r *http.Request) {
	u, err := s.sesh.User(r)
	if err != nil {
		s.log.Info("Couldn't get session: " + err.Error())
		http.Redirect(w, r, "/", http.StatusFound)
		return
	}

	tpl, err := template.ParseFiles("html/dash.html", "html/head.html", "html/header.html")
	if err != nil {
		s.log.Error("Error rendering template: " + err.Error())
		s.handleInternalServerError(w, r)
		return
	}

	categories := make([]proto.Category, 0)
	err = u.Categories(&categories, s.dbmap)

	if err != nil && err != sql.ErrNoRows {
		s.log.Error("Error retrieving categories for dashboard: " + err.Error())
		return
	}

	// grab notes
	var notes []proto.Note
	err = u.Notes(&notes, s.dbmap)

	if err != nil && err != sql.ErrNoRows {
		dispErr, canConvert := err.(proto.ProtoError)
		s.log.Error("Error retrieving notes for dashboard: " + err.Error())
		if canConvert {
			s.handleError(w, r, dispErr.DisplayMessage, dispErr.StatusCode)
		} else {
			s.handleError(w, r, "Internal server error.", http.StatusInternalServerError)
		}
		return
	}

	type noteEntry struct {
		Created      string
		Updated      string
		CreatedHuman string
		UpdatedHuman string
		Title        string
		Id           int
	}

	noteEntries := make([]noteEntry, 0)

	// process all notes
	for _, pg := range notes {
		noteEntries = append(noteEntries, noteEntry{
			Created: time.Unix(int64(pg.Created), 0).Format("3:04 PM Jan 2 2006"),
			Updated: time.Unix(int64(pg.Updated), 0).Format("3:04 PM Jan 2 2006"),
			CreatedHuman: humanize.Time(time.Unix(int64(pg.Created), 0)),
			UpdatedHuman: humanize.Time(time.Unix(int64(pg.Updated), 0)),
			Title: pg.Title,
			Id: pg.Id,
		})
	}

	pageData := map[string]interface{}{
		"title":    "Dashboard",
		"authed":   true,
		"username": u.Username,
		"private": u.Private,
		"css": []string{
			"lib.evil-icons",
			"dash",
		},
		"notes":      noteEntries,
		"categories": categories,
		"js": []string{
			"lib.evil-icons",
			"lib.purify",
			"markdown",
			"dash",
		},
	}
	err = tpl.Execute(w, pageData)
	if err != nil {
		s.log.Error("Error executing dashboard template: " + err.Error())
	}
}

func (s Server) myEditor(w http.ResponseWriter, r *http.Request) {
	u, err := s.sesh.User(r)
	if err != nil {
		s.log.Error("Couldn't initialize user:" + err.Error())
		http.Redirect(w, r, "/", http.StatusFound)
		return
	}

	tpl, err := template.ParseFiles("html/editor.html", "html/head.html", "html/header.html")

	if err != nil {
		s.log.Error("Error rendering template: " + err.Error())
		s.handleInternalServerError(w, r)
		return
	}

	// gets the last element in the URL
	urlTail := strings.Split(r.URL.EscapedPath(), "/")[len(strings.Split(r.URL.EscapedPath(), "/")) - 1]

	// find all categories that this note has
	var cats []proto.Category
	noteId, err := strconv.Atoi(urlTail)
	if err != nil {
		s.log.Warning("Editor loaded with invalid note, id " + strconv.Itoa(noteId) + ": " + err.Error())
		http.Redirect(w, r, "/", http.StatusFound)
		return
	}

	n := proto.Note{}
	err = n.FromId(noteId, u.Id, s.dbmap)
	if err != nil {
		// user doesn't own note
		s.log.Warning("Editor loaded with note that user does not own, note ID " + strconv.Itoa(noteId) + ": " + err.Error())
		http.Redirect(w, r, "/", http.StatusFound)
		return
	}

	err = n.ListCats(&cats, u, s.dbmap)

	if err != nil {
		s.log.Error("Failed to retrieve category list for note " + strconv.Itoa(noteId) + ": " + err.Error())
		s.handleError(w, r, "Internal server error.", http.StatusInternalServerError)
		return
	}

	// structure for joined results
	pageData := map[string]interface{}{
		"title":    "Editor",
		"authed":   true,
		"username": u.Username,
		"private": u.Private,
		"css": []string{
			"lib.evil-icons",
			"lib.codemirror",
			"editor",
		},
		"js": []string{
			"lib.evil-icons",
			"lib.purify",
			"lib.codemirror",
			"markdown",
			"editor",
		},
		"categories": cats,
	}
	err = tpl.Execute(w, pageData)
	if err != nil {
		s.handleError(w, r, "Internal server error.", http.StatusInternalServerError)
		s.log.Error("Error executing editor template: " + err.Error())
		return
	}
}

func (s Server) mySettings(w http.ResponseWriter, r *http.Request) {
	user, err := s.sesh.User(r)
	if err != nil {
		http.Redirect(w, r, "/", http.StatusFound)
	}

	stripe.Key = s.stripeKey

	// make a manual query that fetches more data than FromUsername would
	err = user.FromUsernameForSettings(user.Username, s.dbmap)

	if err != nil {
		s.log.Error("Error loading user data for settings: " + err.Error())
		s.handleError(w, r, "Internal server error", http.StatusInternalServerError)
		return
	}

	stripeInfo := struct {
		Plan      string
		Fee       uint64
		PeriodEnd string
		TrialEnd  string
	}{}

	if user.StripeId == "" {
		stripeInfo.Plan = "Beta tester"
		stripeInfo.Fee = 0
		stripeInfo.PeriodEnd = "Your plan never expires!"
		stripeInfo.TrialEnd = ""
	} else {
		cust, err := customer.Get(user.StripeId, nil)
		if err != nil {
			s.log.Error("Error fetching Stripe customer data for settings: " + err.Error())
			s.handleError(w, r, "Internal server error!", http.StatusInternalServerError)
			return
		}

		if len(cust.Subs.Values) != 1 {
			s.log.Error("User should have 1 plan but instead has " + strconv.Itoa(int(cust.Subs.Count)))
			s.handleError(w, r, "Internal server error!", http.StatusInternalServerError)
			return
		}

		if cust.Subs.Values[0].Plan.ID == "nanote_free" {
			stripeInfo.Plan = "Free forever!"
			stripeInfo.Fee = 0
			stripeInfo.PeriodEnd = "Your plan never expires!"
			stripeInfo.TrialEnd = ""
		} else {
			stripeInfo.Plan = cust.Subs.Values[0].Plan.Name
			stripeInfo.Fee = cust.Subs.Values[0].Plan.Amount / 100
			if cust.Subs.Values[0].Status == "trialing" {
				stripeInfo.TrialEnd = time.Unix(cust.Subs.Values[0].TrialEnd, 0).Format("Jan 2 2006")
				stripeInfo.PeriodEnd = ""
			} else {
				stripeInfo.TrialEnd = ""
				stripeInfo.PeriodEnd = time.Unix(cust.Subs.Values[0].PeriodEnd, 0).Format("Jan 2 2006")
			}
		}
	}

	tpl, err := template.ParseFiles("html/settings.html", "html/head.html", "html/header.html")

	if err != nil {
		s.log.Error("Error rendering template: " + err.Error())
		s.handleInternalServerError(w, r)
		return
	}

	err = tpl.Execute(w, map[string]interface{}{
		"user":     user,
		"title":    "Settings",
		"authed":   true,
		"username": user.Username,
		"private": user.Private,
		"css": []string{
			"lib.evil-icons",
			"settings",
		},
		"js": []string{
			"lib.evil-icons",
			"settings",
		},
		"stripe": stripeInfo,
	})

	if err != nil {
		s.handleError(w, r, "Internal server error!", http.StatusInternalServerError)
		s.log.Error("Error rendering template for settings page: " + err.Error())
		return
	}
}

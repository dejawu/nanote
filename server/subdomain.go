package server

import (
	"database/sql"
	"html/template"
	"net/http"
	"strings"
	"time"

	"github.com/gorilla/mux"

	"kywu.org/nanote/proto"
	"strconv"
)

func (s Server) subFront(w http.ResponseWriter, r *http.Request) {
	username := strings.Split(r.Host, ".")[0]

	// refuse to serve some sensitive domains - just redirect to homepage
	for _, subdomain := range []string{"ssh", "www"} {
		if username == subdomain {
			http.Redirect(w, r, "https://"+s.domain, http.StatusFound)
			return
		}
	}

	u := proto.User{}
	err := u.FromUsername(username, s.dbmap)
	if err == sql.ErrNoRows {
		s.handleNotFound(w, r)
		return
	} else if err != nil {
		s.log.Error("Error retrieving user's data for profile, user " + u.Username + ": " + err.Error())
		return
	}

	if u.Private {
		s.handleNotFound(w, r)
		return
	}

	var notes []proto.Note
	err = u.Notes(&notes, s.dbmap, "public")

	if err != nil && err != sql.ErrNoRows {
		dispErr, canConvert := err.(proto.ProtoError)
		s.log.Error("Error retrieving user's notes for profile, user " + u.Username + ": " + err.Error())
		if canConvert {
			s.log.Error("Error getting user notes: " + err.Error())
			s.handleError(w, r, dispErr.DisplayMessage, dispErr.StatusCode)
		} else {
			s.log.Error("Error getting user notes: " + err.Error())
			s.handleError(w, r, "Internal server error.", http.StatusInternalServerError)
		}
		return
	}

	var cats []proto.Category
	u.Categories(&cats, s.dbmap, "visible")

	if err != nil && err != sql.ErrNoRows {
		s.log.Error("Error retrieving categories for profile, user " + u.Username + ": " + err.Error())
		return
	}

	tpl, err := template.ParseFiles("html/profile/user.html", "html/head.html", "html/header.html", "html/profile/footer.html")

	if err != nil {
		s.log.Error("Error rendering template: " + err.Error())
		s.handleInternalServerError(w, r)
		return
	}

	pageData := map[string]interface{}{
		"title":     u.Username + "'s profile",
		"username":  u.Username,
		"notes":     notes,
		"cats":      cats,
		"noteCount": len(notes),
		"catCount":  len(cats),
		"css": []string{
			"profile",
		},
	}

	err = tpl.Execute(w, pageData)

	if err != nil {
		s.handleError(w, r, "Internal server error.", http.StatusInternalServerError)
		s.log.Error("Error executing template for profile: " + err.Error())
	}
}

func (s Server) subNote(w http.ResponseWriter, r *http.Request) {
	slug := mux.Vars(r)["note"]
	username := strings.Split(r.Host, ".")[0]
	// query database for the requested user
	user := proto.User{}
	user.FromUsername(username, s.dbmap)

	if user.Private {
		// not authorized
		s.handleNotFound(w, r)
		return
	}

	note := proto.Note{}
	err := note.FromSlug(slug, user.Id, s.dbmap)

	if err == sql.ErrNoRows {
		s.handleNotFound(w, r)
		return
	} else if err != nil {
		s.handleNotFound(w, r)
		return
	}

	if note.Privacy == "private" {
		// note is private, act like it doesn't exist
		s.handleNotFound(w, r)
		return
	}

	tpl, err := template.ParseFiles("html/profile/note.html", "html/head.html", "html/header.html", "html/profile/footer.html")

	if err != nil {
		s.log.Error("Error rendering template: " + err.Error())
		s.handleInternalServerError(w, r)
		return
	}

	pageData := map[string]interface{}{
		"title":   note.Title,
		"content": note.Content,
		"privacy": note.Privacy,
		"created": time.Unix(int64(note.Created), 0).Format("3:04 PM, Jan 2, 2006"),
		"updated": time.Unix(int64(note.Updated), 0).Format("3:04 PM, Jan 2, 2006"),
		"owner":   user.Username,
		"css": []string{
			"profile",
		},
		"js": []string{
			"lib.purify",
			"markdown",
			"note",
		},
	}

	err = tpl.Execute(w, pageData)
	if err != nil {
		s.log.Error("Error executing template for note, user " + user.Username + " note " + strconv.Itoa(note.Id) + ": " + err.Error())
		s.handleError(w, r, "Internal server error.", http.StatusInternalServerError)
	}
}

// category list
func (s Server) subCat(w http.ResponseWriter, r *http.Request) {
	catSlug := mux.Vars(r)["cat"]

	catId, err := strconv.Atoi(catSlug)
	if err != nil {
		s.handleNotFound(w, r)
		return
	}

	username := strings.Split(r.Host, ".")[0]
	user := proto.User{}
	user.FromUsername(username, s.dbmap)

	if user.Private {
		// not authorized
		s.handleNotFound(w, r)
		return
	}

	cat := &proto.Category{}
	cat.FromId(catId, user.Id, s.dbmap)

	notes := make([]proto.Note, 0)
	cat.List(&notes, &user, s.dbmap, "visible")

	tpl, err := template.ParseFiles("html/profile/category.html", "html/head.html", "html/header.html", "html/profile/footer.html")

	if err != nil {
		s.log.Error("Error rendering category template: " + err.Error())
		s.handleError(w, r, "Internal server error.", http.StatusInternalServerError)
		return
	}

	pageData := map[string]interface{}{
	"title": cat.Name,
		"category": cat,
		"notes": notes,
		"css": []string{
		"profile",
		},
	}

	err = tpl.Execute(w, pageData)
	if err != nil {
		s.log.Error("Error executing template for category, user " + user.Username + " category " + strconv.Itoa(cat.Id) + ": " + err.Error())
		s.handleError(w, r, "Internal server error.", http.StatusInternalServerError)
		return
	}
}

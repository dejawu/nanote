package api

import (
	"bytes"
	"crypto/rand"
	"crypto/sha256"
	"database/sql"
	"encoding/base64"
	"encoding/json"
	"io/ioutil"
	mrand "math/rand"
	"net/http"
	"regexp"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/sendgrid/sendgrid-go"
	"github.com/sendgrid/sendgrid-go/helpers/mail"
	"github.com/stripe/stripe-go"
	"github.com/stripe/stripe-go/customer"

	"golang.org/x/crypto/pbkdf2"

	"kywu.org/nanote/proto"
	"strconv"
)

func (a Api) login(w http.ResponseWriter, r *http.Request) {
	username := r.FormValue("username")
	password := r.FormValue("password")

	if r.FormValue("api_key") != "" {
		jsonErrorOut(w, "This action cannot be performed through the API.", http.StatusBadRequest)
		return
	}

	// fetch salt given username
	u := proto.User{}
	err := a.dbmap.SelectOne(&u, "SELECT passsalt FROM users WHERE `username`=?", username)

	if err == sql.ErrNoRows {
		// user doesn't exist
		jsonErrorOut(w, "Invalid username or password.", http.StatusBadRequest)
		return
	} else if err != nil {
		a.log.Error("Failed to read database for user: " + err.Error())
		jsonErrorOut(w, "The server failed to log you in. Please try again later.", http.StatusInternalServerError)
		return
	}

	saltString, err := base64.URLEncoding.DecodeString(u.Passsalt)
	if err != nil {
		a.log.Critical("Fatal error: Failed to decode password salt: " + err.Error())
		jsonErrorOut(w, "Internal server error.", http.StatusInternalServerError)
		return
	}

	passhash := generateSHA256Key(password, saltString)

	// check hashed password against the username
	count, err := a.dbmap.SelectInt("SELECT COUNT(id) FROM users WHERE `username`=? AND passhash=?", username, passhash)
	// we won't even have to check for an error here, since the outcome behavior would be the same as for an invalid passhash
	if count != 1 {
		// this message must be the same as the earlier one for username not found
		jsonErrorOut(w, "Invalid username or password.", http.StatusBadRequest)
		return
	}

	err = u.FromUsername(username, a.dbmap)

	if err != nil {
		jsonErrorOut(w, "Invalid username or password.", http.StatusBadRequest)
		return
	}

	// start session. Must happen before we write anything else
	err = a.sesh.Start(u.Id, w)
	if err != nil {
		a.log.Error("Failed to start session: " + err.Error())
		jsonErrorOut(w, "Internal server error.", http.StatusInternalServerError)
		return
	}

	jsonSuccessOut(w, map[string]interface{}{})
}

func (a Api) logout(w http.ResponseWriter, r *http.Request) {
	a.sesh.End(r, w)

	v := map[string]interface{}{}

	jsonSuccessOut(w, v)
}

// TODO re-comment this whole function (it's a mess)
func (a Api) register(w http.ResponseWriter, r *http.Request) {
	// close registration
	jsonErrorOut(w, "Registration is currently disabled.", http.StatusBadRequest);
	return

	if r.FormValue("api_key") != "" {
		jsonErrorOut(w, "This action cannot be performed through the API.", http.StatusBadRequest)
		return
	}

	if !requireFields([]string{"username", "password", "email", "cc_name", "cc_number", "cc_month", "cc_year", "cc_code"}, w, r) {
		return
	}

	// check username against reserved names
	rn, err := ioutil.ReadFile("reserved-names.json")
	if err != nil {
		a.log.Critical("Error reading reserved names file!")
		jsonErrorOut(w, "Internal server error.", http.StatusInternalServerError)
		return
	}

	var names []string
	err = json.Unmarshal(rn, &names)

	if err != nil {
		a.log.Critical("Error parsing list of reserved names: " + err.Error())
		jsonErrorOut(w, "Internal server error.", http.StatusInternalServerError)
		return
	}

	for _, badname := range names {
		if r.FormValue("username") == badname {
			jsonErrorOut(w, "This username is not allowed.", http.StatusBadRequest)
			return
		}
	}

	// check credit card name
	if !regexp.MustCompile(`[ -~]*`).MatchString(r.FormValue("cc_name")) {
		jsonErrorOut(w, "Invalid credit card name.", http.StatusBadRequest)
		return
	}

	// create user object
	u := proto.User{}
	u.Email = r.FormValue("email")
	u.Username = r.FormValue("username")

	err = u.FromNew(r.FormValue("password"), a.dbmap)

	if err != nil {
		dispErr, canConvert := err.(proto.ProtoError)
		a.log.Error("Error adding user: " + err.Error())
		if canConvert {
			jsonProtoErrOut(w, dispErr)
		} else {
			jsonErrorOut(w, "Internal server error.", http.StatusInternalServerError)
		}
		return
	}

	// TODO validate email

	// create welcome note object
	welcome := proto.Note{}
	welcome.FromId(33, 4, a.dbmap) // hard-coded details for the "Welcome" note owned by user kw
	welcome.Id = 0                 // Id is set when the note is inserted
	welcome.Privacy = "private"

	// set up Stripe API call
	if a.stripeKey == "" {
		a.log.Critical("Stripe key not set!")
		jsonErrorOut(w, "Internal server error.", http.StatusInternalServerError)
		return
	}

	stripe.Key = a.stripeKey

	custParams := &stripe.CustomerParams{
		Email: u.Email,
		Plan:  "nanote_early",
	}

	// user ID is not known at this time
	custParams.AddMeta("n_username", u.Username)

	custParams.SetSource(&stripe.CardParams{
		Name:   r.FormValue("cc_name"),
		Number: r.FormValue("cc_number"),
		Month:  r.FormValue("cc_month"),
		Year:   r.FormValue("cc_year"),
		CVC:    r.FormValue("cc_code"),
	})

	// === EXECUTE: all setup and validation must have succeeded to this point.

	// charge credit card. in testing, Secrets should have the testing API key.
	cust, err := customer.New(custParams)

	if err != nil {
		a.log.Error("Failed to charge credit card: " + err.Error())

		v := map[string]interface{}{}
		err := json.Unmarshal([]byte(err.Error()), &v)

		if err != nil {
			jsonErrorOut(w, "Failed to charge credit card.", http.StatusInternalServerError)
			return
		}

		jsonErrorOut(w, v["message"].(string), http.StatusBadRequest)
		return
	}

	u.StripeId = cust.ID

	//  send email
	if a.sgKey == "" {
		a.log.Critical("Stripe API key note set!")
		jsonErrorOut(w, "Internal server error. Please report this error.", http.StatusInternalServerError)
		return
	}

	var messageHtml = "<p>Welcome, " + u.Username + "!</p>" +

		"<p>Please <a href='https://nanote.co/verify/" + u.Email + "&" + u.Verify + "'>click here</a> or visit the following location to verify your account:</p>" +

		"<p>https://nanote.co/verify/" + u.Email + "&" + u.Verify + "</p>" +

		"<p>In your account, you should find a Welcome note that will show you how to get started with Nanote. " +

		"If you need anything, feel free to <a href='mailto:kw@nanote.co'>email me</a> or tweet <a href='https://twitter.com/nanote_co'>@nanote_co</a>. I hope you find Nanote productive and useful!</p>" +

		"<p>Best regards, <br />Kevin Wu</p>"
	messageData := struct {
		sender    *mail.Email
		recipient *mail.Email
		subject   string
		message   *mail.Content
	}{
		sender:    mail.NewEmail("Nanote registration", "register@mail.nanote.co"),
		recipient: mail.NewEmail(u.Username, u.Email),
		subject:   "Welcome to Nanote!",
		message:   mail.NewContent("text/html", messageHtml),
	}

	message := mail.NewV3MailInit(messageData.sender, messageData.subject, messageData.recipient, messageData.message)

	request := sendgrid.GetRequest(a.sgKey, "/v3/mail/send", "https://api.sendgrid.com")
	request.Method = "POST"
	request.Body = mail.GetRequestBody(message)

	response, err := sendgrid.API(request)
	if err != nil {
		a.log.Critical("Error sending mail: " + err.Error())
		jsonErrorOut(w, "Internal server error.", http.StatusInternalServerError)
		return
	}

	// log successful response
	a.log.Info("Sendgrid call: " + strconv.Itoa(response.StatusCode))
	a.log.Info(response.Body)
	a.log.Info(response.Headers)

	// insert user
	err = a.dbmap.Insert(&u)
	if err != nil {
		jsonErrorOut(w, "Failed to create new user. Please report this error.", http.StatusInternalServerError)
		return
	}

	// u.Id isn't known until the user is created
	welcome.Owner = u.Id

	// insert welcome note
	err = a.dbmap.Insert(&welcome)
	if err != nil {
		jsonErrorOut(w, "Failed to create new user. Please try again later.", http.StatusInternalServerError)
		return
	}

	// create DNS entry for user's subdomain
	if a.env == "PRO" {
		var content = []byte(`{"type": "CNAME", "name": "` + u.Username + `", "content": "nanote.co", "proxied": true}`)
		req, err := http.NewRequest("POST", "https://api.cloudflare.com/client/v4/zones/"+a.cfZone+"/dns_records", bytes.NewBuffer(content))

		req.Header.Set("X-Auth-Email", "kevin@kevinywu.com") // don't make me regret this
		req.Header.Set("X-Auth-Key", a.cfKey)
		req.Header.Set("Content-Type", "application/json")

		cfClient := &http.Client{}
		resp, err := cfClient.Do(req)

		if err != nil {
			a.log.Error("Failed to make API call to register CloudFlare entry for username: " + u.Username)
			jsonErrorOut(w, "Could not register your username. Please report this error.", http.StatusInternalServerError)
			return
		}

		if resp.StatusCode != http.StatusOK {
			a.log.Error("Failed to register CloudFlare entry for username: " + u.Username)
			jsonErrorOut(w, "Could not register your username. Please report this error.", http.StatusInternalServerError)
			return
		}
	} else {
		a.log.Debug("In DEV mode, bypassing Cloudflare API call.")
		a.log.Debug("User's verification code: " + u.Verify)
	}

	jsonSuccessOut(w, map[string]interface{}{})
}

// update user password
func (a Api) setPass(w http.ResponseWriter, r *http.Request) {
	if !requireFields([]string{"old", "new"}, w, r) {
		return
	}

	if r.FormValue("api_key") != "" {
		jsonErrorOut(w, "This action cannot be performed through the API.", http.StatusBadRequest)
		return
	}

	user, err := a.sesh.User(r)

	if err != nil {
		jsonErrorOut(w, "Unauthorized.", http.StatusUnauthorized)
		return
	}

	// we make a manual query instead of FromUserId because we need the passhash and salt as well
	err = a.dbmap.SelectOne(&user, "SELECT id,username,passhash,passsalt FROM users WHERE id = ?", user.Id)

	if err != nil {
		a.log.Critical("Failed to retrieve user data for password change, even though user session has already been authenticated: " + err.Error())
		jsonErrorOut(w, "Not authorized.", http.StatusUnauthorized)
		return
	}

	saltString, err := base64.URLEncoding.DecodeString(user.Passsalt)
	if err != nil {
		a.log.Critical("Could not decrypt salt: " + err.Error())
		jsonErrorOut(w, "Could not secure your password. Please try again later.", http.StatusInternalServerError)
		return
	}
	passhash := generateSHA256Key(r.FormValue("old"), saltString)

	if passhash != user.Passhash {
		jsonErrorOut(w, "Original password does not match.", http.StatusBadRequest)
		return
	}

	// create a new salt
	salt, err := generateRandomBytes()
	if err != nil {
		jsonErrorOut(w, "Internal server error, please report this.", http.StatusInternalServerError)
		return
	}

	newPasssalt := base64.URLEncoding.EncodeToString(salt)
	newPasshash := generateSHA256Key(r.FormValue("new"), salt)

	_, err = a.dbmap.Exec("UPDATE users SET passhash=?, passsalt=? WHERE id=?", newPasshash, newPasssalt, user.Id)

	if err != nil {
		jsonErrorOut(w, "Internal server error, please report this.", http.StatusInternalServerError)
		return
	}

	jsonSuccessOut(w, map[string]interface{}{})
}

// verify a user
func (a Api) verify(w http.ResponseWriter, r *http.Request) {
	if !requireFields([]string{"email", "code"}, w, r) {
		return
	}

	if r.FormValue("api_key") != "" {
		jsonErrorOut(w, "This action cannot be performed through the API.", http.StatusBadRequest)
		return
	}

	u := proto.User{}

	// find matching entry
	err := a.dbmap.SelectOne(&u, "SELECT id,username FROM users WHERE `email`=? AND BINARY `verify`=?", r.FormValue("email"), r.FormValue("code"))

	if err == sql.ErrNoRows || r.FormValue("code") == "" {
		jsonErrorOut(w, "Invalid code or email address.", http.StatusBadRequest)
		return
	} else if err != nil {
		jsonErrorOut(w, "Internal server error. Please try again later.", http.StatusInternalServerError)
		return
	}

	_, err = a.dbmap.Exec("UPDATE users SET verify='' WHERE `email`=? AND BINARY `verify`=?", r.FormValue("email"), r.FormValue("code"))

	if err != nil {
		jsonErrorOut(w, "Internal server error. Please try again later.", http.StatusInternalServerError)
		a.log.Error("Failed to update database when verifying user: " + err.Error())
		return
	}

	a.sesh.Start(u.Id, w)

	jsonSuccessOut(w, map[string]interface{}{})
}

func (a Api) endSub(w http.ResponseWriter, r *http.Request) {
	if !requireFields([]string{"password"}, w, r) {
		return
	}

	if r.FormValue("api_key") != "" {
		jsonErrorOut(w, "This action cannot be performed through the API.", http.StatusBadRequest)
		return
	}

	u, err := a.sesh.User(r)
	uTemp := proto.User{} // only exists to hold pass salt. Is there a better way to do this?

	// verify user's identity
	err = a.dbmap.SelectOne(&uTemp, "SELECT passsalt FROM users WHERE `username`=?", u.Username)

	if err == sql.ErrNoRows {
		jsonErrorOut(w, "Invalid password.", http.StatusBadRequest)
		return
	} else if err != nil {
		jsonErrorOut(w, "Internal server error. Your account was not deleted.", http.StatusInternalServerError)
		return
	}

	saltString, err := base64.URLEncoding.DecodeString(uTemp.Passsalt)
	if err != nil {
		jsonErrorOut(w, "Internal server error. Your account was not deleted.", http.StatusInternalServerError)
		return
	}

	passhash := generateSHA256Key(r.FormValue("password"), saltString)

	// check hashed password against the username
	user := proto.User{}
	err = a.dbmap.SelectOne(&user, "SELECT id,stripe_id FROM users WHERE `username`=? AND BINARY `passhash`=?", u.Username, passhash)
	// we won't even have to check for an error here, since the outcome behavior would be the same as for an invalid passhash
	if err != nil {
		jsonErrorOut(w, "Invalid password.", http.StatusBadRequest)
		return
	}

	// at this point, the user exists and is legit

	// stripe API call
	if user.StripeId != "" {
		stripe.Key = a.stripeKey

		_, err = customer.Del(user.StripeId)

		if err != nil {
			jsonErrorOut(w, "Failed to end your billing subscription properly. Your account data was not deleted.", http.StatusInternalServerError)
			return
		}
	}

	// delete all catmap corresponding to user's notes:

	// delete user's notes
	_, err = a.dbmap.Exec("DELETE FROM notes WHERE owner=?", user.Id)

	// delete user's categories
	_, err = a.dbmap.Exec("DELETE FROM categories WHERE owner=?", user.Id)

	// clean up all orphaned catmaps
	_, err = a.dbmap.Exec("DELETE catmap FROM notes RIGHT OUTER JOIN catmap ON notes.id = catmap.note_id WHERE notes.id IS NULL")
	_, err = a.dbmap.Exec("DELETE catmap FROM categories RIGHT OUTER JOIN catmap ON categories.id = catmap.cat_id WHERE categories.id IS null")

	_, err = a.dbmap.Exec("DELETE FROM users WHERE id=?", user.Id)

	if err != nil {
		// user deletion succeeded so we can continue anyway, even if the database is unclean (i.e. there are orphans)
		a.log.Error("Failed to update customer data, check for likely orphans in the database: " + err.Error())
	}

	// end session
	a.sesh.End(r, w)

	jsonSuccessOut(w, map[string]interface{}{})
}

// sets up password recovery and sends an email to the user with the recovery link.
func (a Api) lostPass(w http.ResponseWriter, r *http.Request) {
	if r.FormValue("api_key") != "" {
		jsonErrorOut(w, "This action cannot be performed through the API.", http.StatusBadRequest)
		return
	}

	if a.sesh.Check(r) {
		jsonErrorOut(w, "Already logged in.", http.StatusBadRequest)
		return
	}

	if r.FormValue("username") == "" {
		jsonErrorOut(w, "Missing field: username", http.StatusBadRequest)
		return
	}

	// query for username
	u := proto.User{}
	err := u.FromUsername(r.FormValue("username"), a.dbmap)

	if err != nil {
		// don't reveal valid usernames
		a.log.Warning("User attempted password recovery on nonexistent username: " + u.Username)
		jsonSuccessOut(w, map[string]interface{}{})
		return
	}

	// generate recovery key (to verify user's email)
	mrand.Seed(time.Now().UTC().UnixNano())
	const charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
	recoverKey := make([]byte, 32)

	for i := 0; i < len(recoverKey); i++ {
		recoverKey[i] = charset[mrand.Intn(len(charset))]
	}

	// insert recovery key into database
	_, err = a.dbmap.Exec("UPDATE users SET recover=? WHERE id=?", recoverKey, u.Id)

	if err != nil {
		jsonErrorOut(w, "Internal server error.", http.StatusInternalServerError)
		return
	}

	//  send email
	if a.sgKey == "" {
		a.log.Critical("Sendgrid key not set!")
		jsonErrorOut(w, "Internal server error.", http.StatusInternalServerError)
		return
	}

	var messageHtml = "<p>Hello, " + u.Username + "!</p>" +

		"<p>You're receiving this email because you requested a password reset.</p>" +

		"<p>If you did, please go to this link: https://nanote.co/recover/" + string(recoverKey) + " and follow the instructions to reset your password.</p>" +

		"<p>If you did not request a password reset, you can ignore this email. If you believe someone is trying to get into your account, please make sure your password is secure.</p>" +

		"<p>As always, if you need anything, feel free to <a href='mailto:kw@nanote.co'>email me</a> or tweet <a href='https://twitter.com/nanote_co'>@nanote_co</a>. I hope you find Nanote productive and useful!</p>" +

		"<p>Best regards, <br />Kevin Wu</p>"
	messageData := struct {
		sender    *mail.Email
		recipient *mail.Email
		subject   string
		message   *mail.Content
	}{
		sender:    mail.NewEmail("Nanote support", "support@mail.nanote.co"),
		recipient: mail.NewEmail(u.Username, u.Email),
		subject:   "Password reset",
		message:   mail.NewContent("text/html", messageHtml),
	}

	message := mail.NewV3MailInit(messageData.sender, messageData.subject, messageData.recipient, messageData.message)

	request := sendgrid.GetRequest(a.sgKey, "/v3/mail/send", "https://api.sendgrid.com")
	request.Method = "POST"
	request.Body = mail.GetRequestBody(message)

	response, err := sendgrid.API(request)

	if err != nil {
		a.log.Critical("Error sending mail: " + err.Error())
		jsonErrorOut(w, "Internal server error.", http.StatusInternalServerError)
		return
	}
	// log successful response
	a.log.Info("Sendgrid call: " + strconv.Itoa(response.StatusCode))
	a.log.Info(response.Body)
	a.log.Info(response.Headers)

	jsonSuccessOut(w, map[string]interface{}{})
}

// allows user to update to their new password.
func (a Api) recoverPass(w http.ResponseWriter, r *http.Request) {
	if r.FormValue("api_key") != "" {
		jsonErrorOut(w, "This action cannot be performed through the API.", http.StatusBadRequest)
		return
	}

	if a.sesh.Check(r) {
		jsonErrorOut(w, "Already logged in.", http.StatusBadRequest)
		return
	}

	// custom return for newpass
	if r.FormValue("newpass") == "" {
		jsonErrorOut(w, "Missing field: new password", http.StatusBadRequest)
		return
	}

	if r.FormValue("code") == "" {
		jsonErrorOut(w, "Missing code. Try reloading the note.", http.StatusBadRequest)
		return
	}

	if !requireFields([]string{"code", "username", "newpass"}, w, r) { // newpass and code arguments are redundant but i'm leaving them for future understanding
		return
	}

	u := proto.User{}

	// we make a manual query instead of FromUserId because we need the passhash and salt as well
	err := a.dbmap.SelectOne(&u, "SELECT id,username,passhash,passsalt FROM users WHERE `username`=? AND BINARY recover=?", r.FormValue("username"), r.FormValue("code"))

	if err != nil {
		// no recovery entry
		jsonErrorOut(w, "This recovery link is invalid. It may have expired.", http.StatusBadRequest)
		return
	}

	// generate password salt
	salt, err := generateRandomBytes()
	if err != nil {
		a.log.Critical("Fatal error: Failed to generate password salt in recoverPass() with recoverycode: " + r.FormValue("code") + " username: " + r.FormValue("username") + " Err: " + err.Error())
		jsonErrorOut(w, "Internal server error. Please try again later.", http.StatusInternalServerError)
		return
	}

	newPasssalt := base64.URLEncoding.EncodeToString(salt)
	newPasshash := generateSHA256Key(r.FormValue("newpass"), salt)

	_, err = a.dbmap.Exec("UPDATE users SET passhash=?, passsalt=?, recover='' WHERE id=?", newPasshash, newPasssalt, u.Id)

	if err != nil {
		jsonErrorOut(w, "Internal server error: "+err.Error(), http.StatusInternalServerError)
		return
	}

	jsonSuccessOut(w, map[string]interface{}{})

}

func (a Api) generateApiKey(w http.ResponseWriter, r *http.Request) {
	if r.FormValue("api_key") != "" {
		jsonErrorOut(w, "This action cannot be performed through the API.", http.StatusBadRequest)
		return
	}

	u, err := a.sesh.User(r)
	if err != nil {
		jsonErrorOut(w, "Unauthorized.", http.StatusUnauthorized)
		return
	}

	// generate random string for verification
	mrand.Seed(time.Now().UTC().UnixNano())
	const charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

	// generate random API key
	apiKey := make([]byte, 64)

	for i := 0; i < 64; i++ {
		apiKey[i] = charset[mrand.Intn(len(charset))]
	}

	a.dbmap.Exec("UPDATE users SET api_key=? WHERE `username`=?", apiKey, u.Username)

	jsonSuccessOut(w, map[string]interface{}{
		"api_key": u.Username + ":" + string(apiKey),
	})
}

// thank you Jorge
func generateRandomBytes() ([]byte, error) {
	b := make([]byte, 32)

	_, err := rand.Read(b)
	if err != nil {
		return nil, err
	}

	return b, nil
}

func generateSHA256Key(plain string, salt []byte) string {
	key := pbkdf2.Key([]byte(plain),
		salt,
		2048,
		32,
		sha256.New)
	out := base64.URLEncoding.EncodeToString(key)
	return out[:len(out)-4]
}

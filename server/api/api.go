package api

import (
	"encoding/json"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/op/go-logging"
	"gopkg.in/gorp.v1"

	"kywu.org/nanote/seshmgr"
	"kywu.org/nanote/proto"
)

type Api struct {
	dbmap     *gorp.DbMap
	sesh      *seshmgr.SessionManager
	cfKey     string
	cfZone    string
	sgKey     string
	stripeKey string
	env       string
	log       *logging.Logger
}

func Init(d *gorp.DbMap, sh *seshmgr.SessionManager, secrets map[string]string, log *logging.Logger) *Api {
	return &Api{
		dbmap:     d,
		sesh:      sh,
		cfKey:     secrets["cf_apiKey"],
		cfZone:    secrets["cf_zoneId"],
		sgKey:     secrets["sg_apiKey"],
		stripeKey: secrets["stripe_apiKey"],
		env:       secrets["env"],
		log:    log,
	}
}

// set routes for API handlers
func (a Api) Handle(r *mux.Router) {

	a.log.Debug("Initializing API.")

	r.StrictSlash(false)
	// functions involving notes
	r.HandleFunc("/note/create", a.createNote)     // create a new note
	r.HandleFunc("/note/view", a.viewNote)         // view a note
	r.HandleFunc("/note/update", a.updateNote)     // update an existing note
	r.HandleFunc("/note/delete", a.deleteNote)     // remove a note
	r.HandleFunc("/note/addcat", a.addNoteCat)     // add a category to a note
	r.HandleFunc("/note/listcats", a.listNoteCats) // list categories that a note has
	r.HandleFunc("/note/removecat", a.removeNoteCat)

	// functions involving categories
	r.HandleFunc("/cat/create", a.createCat) // create a new category
	r.HandleFunc("/cat/view", a.viewCat)     // view notes that have a category
	r.HandleFunc("/cat/delete", a.deleteCat) // delete a category and cleans up catmap orphans
	r.HandleFunc("/cat/update", a.updateCat) // update an existing category (rename, for now)

	// functions involving the logged-in user
	r.HandleFunc("/user/cats", a.listUserCats)   // list a user's categories
	r.HandleFunc("/user/notes", a.listUserNotes) // list a user's notes
	r.HandleFunc("/user/update", a.updateUser)
	r.HandleFunc("/user/export", a.exportUser)

	// auth functions
	r.HandleFunc("/auth/login", a.login)
	r.HandleFunc("/auth/logout", a.logout)
	r.HandleFunc("/auth/register", a.register)
	r.HandleFunc("/auth/setpass", a.setPass)
	r.HandleFunc("/auth/verify", a.verify)
	r.HandleFunc("/auth/genapikey", a.generateApiKey)
	r.HandleFunc("/auth/endsub", a.endSub)

	r.HandleFunc("/auth/lostpass", a.lostPass)
	r.HandleFunc("/auth/recover", a.recoverPass)

	r.HandleFunc("/{dummy:.*}", func(w http.ResponseWriter, r *http.Request) {
		jsonErrorOut(w, "Resource does not exist.", http.StatusNotFound)
		return
	})

}

// generic error-out with a message, used by both api and auth
// this should never need to be used by any non-api function
func jsonErrorOut(w http.ResponseWriter, message string, httpCode int) {
	v := map[string]interface{}{
		"success": false,
		"message": message,
	}
	j, _ := json.Marshal(v)

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(httpCode)
	w.Write(j)
}

func jsonProtoErrOut(w http.ResponseWriter, err proto.ProtoError) {
	v := map[string]interface{}{
		"success": false,
		"message": err.DisplayMessage,
	}

	j, _ := json.Marshal(v)

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(err.StatusCode)
	w.Write(j)
}

func jsonSuccessOut(w http.ResponseWriter, v map[string]interface{}) {

	v["success"] = true

	j, err := json.Marshal(v)

	if err != nil {
		jsonErrorOut(w, "Error returning response.", http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(j)
}

// returns true if all fields are found
func requireFields(reqdFields []string, w http.ResponseWriter, r *http.Request) bool {
	for field := range reqdFields {
		if r.FormValue(reqdFields[field]) == "" {
			jsonErrorOut(w, "Missing field: " + reqdFields[field], http.StatusBadRequest)
			return false
		}
	}
	return true
}

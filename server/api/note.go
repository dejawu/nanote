package api

import (
	"encoding/json"
	"net/http"
	"strconv"
	"strings"
	"time"

	"kywu.org/nanote/proto"
)

func (a Api) createNote(w http.ResponseWriter, r *http.Request) {
	u, err := a.sesh.User(r)
	if err != nil {
		jsonErrorOut(w, "Unauthorized.", http.StatusUnauthorized)
		return
	}

	n := proto.Note{}
	err = n.Create(u, a.dbmap)

	if err != nil {
		a.log.Error("Error creating note: " + err.Error())
		jsonErrorOut(w, "Internal server error.", http.StatusInternalServerError)
		return
	}

	v := map[string]interface{}{
		"id":      n.Id,
		"slug":    n.Slug,
		"updated": n.Updated,
	}

	jsonSuccessOut(w, v)
}

func (a Api) viewNote(w http.ResponseWriter, r *http.Request) {
	if !requireFields([]string{"note"}, w, r) {
		return
	}

	u, err := a.sesh.User(r)
	if err != nil {
		jsonErrorOut(w, "Unauthorized.", http.StatusUnauthorized)
		return
	}

	noteId, _ := strconv.Atoi(r.FormValue("note"))

	n := proto.Note{}
	err = n.FromId(noteId, u.Id, a.dbmap)

	if err != nil {
		jsonErrorOut(w, "The requested note does not exist.", http.StatusBadRequest)
		return
	}

	v := map[string]interface{}{
		"note": n,
	}

	jsonSuccessOut(w, v)
}

func (a Api) updateNote(w http.ResponseWriter, r *http.Request) {
	reqdFields := []string{"note", "title", "privacy"}
	for field := range reqdFields {
		if r.FormValue(reqdFields[field]) == "" {
			switch reqdFields[field] {
			case "title":
				jsonErrorOut(w, "Cannot save note without title.", http.StatusBadRequest)
				return
			case "note":
				jsonErrorOut(w, "No note ID given.", http.StatusBadRequest)
				return
			case "privacy":
				continue // allow this field to be omitted, default to private
			default:
				a.log.Error("How did you get here?")
				jsonErrorOut(w, "Internal server error.", http.StatusInternalServerError)
				return
			}
		}
	}

	u, err := a.sesh.User(r)
	if err != nil {
		jsonErrorOut(w, "Unauthorized.", http.StatusUnauthorized)
		return
	}

	n := proto.Note{}
	noteId, err := strconv.Atoi(r.FormValue("note"))

	if err != nil {
		jsonErrorOut(w, "Note id must be a number.", http.StatusBadRequest)
		return
	}

	err = n.FromId(noteId, u.Id, a.dbmap)
	if err != nil {
		jsonErrorOut(w, "Note does not exist.", http.StatusBadRequest)
		return
	}

	// the "updated" value for edit locking is optional. If omitted, the API will simply overwrite the existing content.
	if r.FormValue("updated") != "" {
		// check if note is working on latest copy
		clientUpdated, err := strconv.Atoi(r.FormValue("updated"))
		if err != nil {
			jsonErrorOut(w, "Invalid update timestamp.", http.StatusBadRequest)
			return
		}

		if clientUpdated < n.Updated {
			// this is a modified jsonErrorOut
			v := map[string]interface{}{
				"success": false,
				"message": "You are editing an outdated version of this note. ",
				"latest":  n.Updated, // provide the client the "correct" updated value so they can override if they want.
			}
			j, _ := json.Marshal(v)

			w.Header().Set("Content-Type", "application/json")
			w.WriteHeader(http.StatusBadRequest)
			w.Write(j)

			return
		}
	}

	n.Id = noteId
	n.Title = r.FormValue("title")
	n.Slug = strings.ToLower(r.FormValue("slug"))
	n.Content = r.FormValue("content")
	n.Updated = int(time.Now().Unix())

	switch r.FormValue("privacy") {
	case "private":
		n.Privacy = "private"
		break
	case "public":
		n.Privacy = "public"
		break
	case "unlisted":
		n.Privacy = "unlisted"
		break
	default:
		jsonErrorOut(w, "Privacy setting must be private, public, or unlisted.", http.StatusBadRequest)
		return
	}

	err = n.Update(u, a.dbmap)

	if err != nil {
		dispErr, canConvert := err.(proto.ProtoError)
		a.log.Error("Failed to update note: " + err.Error())
		if canConvert {
			jsonProtoErrOut(w, dispErr)
		} else {
			jsonErrorOut(w, "Failed to update note. Please try again later.", http.StatusInternalServerError)
		}
		return
	}

	jsonSuccessOut(w, map[string]interface{}{
		"updated": n.Updated,
	})
}

// deletes id given in note
func (a Api) deleteNote(w http.ResponseWriter, r *http.Request) {
	if !requireFields([]string{"note"}, w, r) {
		return
	}

	u, err := a.sesh.User(r)
	if err != nil {
		jsonErrorOut(w, "Unauthorized.", http.StatusUnauthorized)
		return
	}

	noteId, err := strconv.Atoi(r.FormValue("note"))

	if err != nil {
		jsonErrorOut(w, "Note id must be a number.", http.StatusBadRequest)
		return
	}

	n := proto.Note{}
	err = n.FromId(noteId, u.Id, a.dbmap)

	if err != nil {
		jsonErrorOut(w, "Note does not exist.", http.StatusBadRequest)
		return
	}

	err = n.Delete(u, a.dbmap)

	if err != nil {
		a.log.Error("Failed to update note: " + err.Error())
		jsonErrorOut(w, "Internal server error.", http.StatusBadRequest)
		return
	}

	jsonSuccessOut(w, map[string]interface{}{})
}

// upload an existing markdown file to be published
func (a Api) importNote(w http.ResponseWriter, r *http.Request) {
	if !requireFields([]string{"title", "slug"}, w, r) {
		return
	}

	// get uploaded file

	// validate and sanitize

	// insert
}

// note ==> category functions

// show categories this note has.
func (a Api) listNoteCats(w http.ResponseWriter, r *http.Request) {
	if !requireFields([]string{"note"}, w, r) {
		return
	}

	u, err := a.sesh.User(r)
	if err != nil {
		jsonErrorOut(w, "Unauthorized.", http.StatusUnauthorized)
		return
	}

	noteId, err := strconv.Atoi(r.FormValue("note"))
	if err != nil {
		jsonErrorOut(w, "Note id must be a number.", http.StatusUnauthorized)
		return
	}

	n := proto.Note{}
	n.FromId(noteId, u.Id, a.dbmap)

	var cats []proto.Category
	err = n.ListCats(&cats, u, a.dbmap)

	if err != nil {
		dispErr, canConvert := err.(proto.ProtoError)
		a.log.Error("Error retrieving categories for note: " + strconv.Itoa(n.Id) + " " + err.Error())
		if canConvert {
			jsonProtoErrOut(w, dispErr)
		} else {
			jsonErrorOut(w, "Internal server error.", http.StatusInternalServerError)
		}
		return
	}

	v := map[string]interface{}{
		"success": true,
		"cats":    cats,
	}

	jsonSuccessOut(w, v)
}

// add a category to this note
func (a Api) addNoteCat(w http.ResponseWriter, r *http.Request) {
	if !requireFields([]string{"note", "category"}, w, r) {
		return
	}

	u, err := a.sesh.User(r)
	if err != nil {
		jsonErrorOut(w, "Unauthorized.", http.StatusUnauthorized)
		return
	}

	noteId, err := strconv.Atoi(r.FormValue("note"))
	if err != nil {
		jsonErrorOut(w, "Note id must be a number.", http.StatusBadRequest)
		return
	}

	note := proto.Note{}
	note.FromId(noteId, u.Id, a.dbmap)

	catId, err := strconv.Atoi(r.FormValue("category"))
	if err != nil {
		jsonErrorOut(w, "Category id must be a number.", http.StatusBadRequest)
		return
	}

	err = note.AddCat(catId, u, a.dbmap)

	if err != nil {
		dispErr, canConvert := err.(proto.ProtoError)
		a.log.Error("Error adding category: " + strconv.Itoa(catId) + " " + err.Error())
		if canConvert {
			jsonProtoErrOut(w, dispErr)
		} else {
			jsonErrorOut(w, "Internal server error.", http.StatusInternalServerError)
		}
		return
	}

	jsonSuccessOut(w, map[string]interface{}{})
}

// remove a category from this note
func (a Api) removeNoteCat(w http.ResponseWriter, r *http.Request) {
	if !requireFields([]string{"note", "category"}, w, r) {
		return
	}

	u, err := a.sesh.User(r)
	if err != nil {
		jsonErrorOut(w, "Unauthorized.", http.StatusUnauthorized)
	}

	noteId, err := strconv.Atoi(r.FormValue("note"))
	if err != nil {
		jsonErrorOut(w, "Note id must be a number.", http.StatusBadRequest)
		return
	}

	catId, err := strconv.Atoi(r.FormValue("category"))
	if err != nil {
		jsonErrorOut(w, "Category id must be a number.", http.StatusBadRequest)
		return
	}

	n := proto.Note{}
	err = n.FromId(noteId, u.Id, a.dbmap)

	// goes here
	err = n.RemoveCat(catId, u, a.dbmap)

	if err != nil {
		dispErr, canConvert := err.(proto.ProtoError)
		a.log.Error("Error removing category: " + err.Error())
		if canConvert {
			jsonProtoErrOut(w, dispErr)
		} else {
			jsonErrorOut(w, "Internal server error.", http.StatusInternalServerError)
		}
		return
	}

	jsonSuccessOut(w, map[string]interface{}{})
}

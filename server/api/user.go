package api

import (
	"database/sql"
	"net/http"

	"kywu.org/nanote/proto"
	"strconv"
)

// get all of this user's notes
func (a Api) listUserNotes(w http.ResponseWriter, r *http.Request) {
	// optional field: "filter" can be of type "uncategorized", more in the future
	a.log.Info("Fetching user notes")
	user, err := a.sesh.User(r)
	if err != nil {
		jsonErrorOut(w, "Unauthorized.", http.StatusUnauthorized)
		return
	}

	var notes []proto.Note

	if r.FormValue("filter") == "" {
		err = user.Notes(&notes, a.dbmap, "none")
	} else if r.FormValue("filter") == "uncategorized" {
		err = user.Notes(&notes, a.dbmap, "uncategorized")
	} else {
		jsonErrorOut(w, "Invalid filter option.", http.StatusBadRequest)
		return
	}

	if err != nil && err != sql.ErrNoRows {
		dispErr, canConvert := err.(proto.ProtoError)
		a.log.Error("Error retrieving notes for user: " + strconv.Itoa(user.Id) + ": " + err.Error())
		if canConvert {
			jsonProtoErrOut(w, dispErr)
		} else {
			jsonErrorOut(w, "Internal server error.", http.StatusInternalServerError)
		}
		return
	}

	jsonSuccessOut(w, map[string]interface{}{
		"notes": &notes,
	})
}

// find all categories owned by the user
func (a Api) listUserCats(w http.ResponseWriter, r *http.Request) {
	// no required fields!

	user, err := a.sesh.User(r)
	if err != nil {
		jsonErrorOut(w, "Unauthorized.", http.StatusUnauthorized)
		return
	}

	var cats []proto.Category
	err = user.Categories(&cats, a.dbmap)

	if err != nil && err != sql.ErrNoRows {
		a.log.Error("Error retrieving categories for user: " + strconv.Itoa(user.Id) + ": " + err.Error())
		jsonErrorOut(w, "Internal database error: ", http.StatusInternalServerError)
		return
	}

	jsonSuccessOut(w, map[string]interface{}{
		"cats": &cats,
	})
}

func (a Api) updateUser(w http.ResponseWriter, r *http.Request) {
	if !requireFields([]string{"private"}, w, r) {
		return
	}

	user, err := a.sesh.User(r)
	if err != nil {
		jsonErrorOut(w, "Unauthorized.", http.StatusUnauthorized)
		return
	}

	// bias towards marking user as private if there's a mistake
	if r.FormValue("private") == "0" || r.FormValue("private") == "false" {
		user.Private = false
	} else {
		user.Private = true
	}

	err = user.Update(a.dbmap)

	if err != nil {
		jsonErrorOut(w, "Failed to update user.", http.StatusInternalServerError)
		return
	}

	jsonSuccessOut(w, map[string]interface{}{})
}

func (a Api) exportUser(w http.ResponseWriter, r *http.Request) {
	user, err := a.sesh.User(r)
	if err != nil {
		jsonErrorOut(w, "Unauthorized.", http.StatusUnauthorized)
		return
	}

	var notes []struct {
		Id      int    `db:"id" json:"id"`
		Privacy string `db:"privacy" json:"privacy"`
		Title   string `db:"title" json:"title"`
		Slug    string `db:"slug" json:"slug"`
		Content string `db:"content" json:"content"`
		Created int    `db:"created" json:"created"`
		Updated int    `db:"updated" json:"updated"`
		Cats    string `db:"cats" json:"categories"`
	}

	_, err = a.dbmap.Select(&notes, "SELECT notes.title,notes.slug,notes.privacy,notes.content,notes.created,notes.updated,COALESCE(GROUP_CONCAT(categories.name ORDER BY categories.name),'') AS cats FROM notes LEFT JOIN catmap ON catmap.note_id = notes.id LEFT JOIN categories ON catmap.cat_id = categories.id WHERE notes.owner=? GROUP BY notes.id", user.Id)

	if err != nil {
		jsonErrorOut(w, "Internal server error: "+err.Error(), http.StatusInternalServerError)
		return
	}

	jsonSuccessOut(w, map[string]interface{}{
		"notes": notes,
	})
}

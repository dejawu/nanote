package api

import (
	"net/http"
	"strconv"

	"kywu.org/nanote/proto"
)

func (a Api) createCat(w http.ResponseWriter, r *http.Request) {
	if !requireFields([]string{"name"}, w, r) {
		return
	}

	user, err := a.sesh.User(r)
	if err != nil {
		jsonErrorOut(w, "Unauthorized.", http.StatusUnauthorized)
		return
	}

	cat := proto.Category{}
	err = cat.Create(r.FormValue("name"), user, a.dbmap)

	if err != nil {
		a.log.Error("Error creating category: " + err.Error())
		dispErr, canConvert := err.(proto.ProtoError)
		if canConvert {
			jsonProtoErrOut(w, dispErr)
		} else {
			jsonErrorOut(w, "Internal server error.", http.StatusInternalServerError)
		}
		return
	}

	v := map[string]interface{}{
		"id": cat.Id,
	}

	jsonSuccessOut(w, v)
}

func (a Api) updateCat(w http.ResponseWriter, r *http.Request) {
	if !requireFields([]string{"category", "name"}, w, r) {
		return
	}

	u, err := a.sesh.User(r)
	if err != nil {
		jsonErrorOut(w, "Unauthorized.", http.StatusUnauthorized)
		return
	}

	catId, err := strconv.Atoi(r.FormValue("category"))
	if err != nil {
		jsonErrorOut(w, "Category id must be a number.", http.StatusBadRequest)
		return
	}

	c := proto.Category{}
	c.FromId(catId, u.Id, a.dbmap)
	err = c.Update(r.FormValue("name"), u, a.dbmap)

	if err != nil {
		dispErr, canConvert := err.(proto.ProtoError)
		a.log.Error("Error updating category: " + err.Error())
		if canConvert {
			jsonProtoErrOut(w, dispErr)
		} else {
			jsonErrorOut(w, "Internal server error.", http.StatusInternalServerError)
		}
		return
	}

	jsonSuccessOut(w, map[string]interface{}{})
}

func (a Api) deleteCat(w http.ResponseWriter, r *http.Request) {
	if !requireFields([]string{"category"}, w, r) {
		return
	}

	u, err := a.sesh.User(r)
	if err != nil {
		jsonErrorOut(w, "Unauthorized.", http.StatusUnauthorized)
		return
	}

	catId, err := strconv.Atoi(r.FormValue("category"))
	if err != nil {
		jsonErrorOut(w, "Category id must be a number.", http.StatusBadRequest)
		return
	}

	c := proto.Category{}
	c.FromId(catId, u.Id, a.dbmap)
	err = c.Delete(u, a.dbmap)

	if err != nil {
		dispErr, canConvert := err.(proto.ProtoError)
		a.log.Error("Error deleting category: " + err.Error())
		if canConvert {
			jsonProtoErrOut(w, dispErr)
		} else {

			jsonErrorOut(w, "Internal server error.", http.StatusInternalServerError)
		}
		return
	}

	jsonSuccessOut(w, map[string]interface{}{})
}

// list all notes in this category.
func (a Api) viewCat(w http.ResponseWriter, r *http.Request) {
	if !requireFields([]string{"category"}, w, r) {
		return
	}

	u, err := a.sesh.User(r)
	if err != nil {
		jsonErrorOut(w, "Unauthorized.", http.StatusUnauthorized)
		return
	}

	catId, err := strconv.Atoi(r.FormValue("category"))
	if err != nil {
		jsonErrorOut(w, "Category id must be a number.", http.StatusBadRequest)
		return
	}

	var notes []proto.Note

	cat := proto.Category{}
	err = cat.FromId(catId, u.Id, a.dbmap)
	if err != nil {
		a.log.Error("Failed to get category from ID: " + err.Error())
		jsonErrorOut(w, "Category does not exist.", http.StatusBadRequest)
		return
	}


	err = cat.List(&notes, u, a.dbmap)

	if err != nil {
		dispErr, canConvert := err.(proto.ProtoError)
		a.log.Error("Error retrieving notes in category: " + strconv.Itoa(cat.Id) + ": " + err.Error())
		if canConvert {
			jsonProtoErrOut(w, dispErr)
		} else {
			jsonErrorOut(w, "Internal server error.", http.StatusInternalServerError)
		}
		return
	}

	// sort
	if r.FormValue("sort") != "" {

	}

	v := map[string]interface{}{
		"notes": &notes,
		"name":  cat.Name,
	}

	jsonSuccessOut(w, v)
}

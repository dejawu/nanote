# Nanote

Nanote is a lightweight, no-frills, no-nonsense note taking webapp, driven by Markdown.

# Deployment
Run `deploy.sh`, then `systemctl restart nanote` as root.

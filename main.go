package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"flag"

	_ "github.com/go-sql-driver/mysql"
	"github.com/stripe/stripe-go"
	"github.com/stripe/stripe-go/customer"

	"github.com/sendgrid/sendgrid-go"
	"github.com/sendgrid/sendgrid-go/helpers/mail"

	"gopkg.in/gorp.v1"

	"kywu.org/nanote/proto"
	"kywu.org/nanote/server"
	"math/rand"
	"strconv"
	"time"
	"net/http"
	"bytes"
)

// Secrets is accessibly globally within main.go only
var Secrets map[string]string

func main() {
	fmt.Println("Nanote server v1.6")

	flag.Parse()

	s, err := ioutil.ReadFile("secrets.json")
	if err != nil {
		panic("Fatal error: Unable to read config file. " + err.Error())
	}
	err = json.Unmarshal(s, &Secrets)
	if err != nil {
		fmt.Println(err.Error())
		panic("Fatal error: Config file is formatted incorrectly. " + err.Error())
	}

	dbmap := initDb()
	defer dbmap.Db.Close()

	var command string
	if len(os.Args) == 1 {
		command = "run" // run by default
	} else {
		command = os.Args[1]
	}

	switch command {
	case "start":
	case "run":
		nanote := server.Init(dbmap, Secrets["domain"], Secrets)
		nanote.Start()
		fmt.Println("Server appears to have exit. Is there another server running?")
		return
	case "create-user":
		u := proto.User{}
		var input string

		fmt.Print("Username: ")
		fmt.Scanln(&input)
		u.Username = input

		fmt.Println("WARNING: Password will be visible in plaintext when typed!")
		fmt.Print("Initial password, or leave blank to generate:")
		fmt.Scanln(&input)

		var password string
		if input == u.Username {
			// generate a password
			rand.Seed(time.Now().UTC().UnixNano())
			const charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
			passSlice := make([]byte, 32)

			for i := 0; i < len(passSlice); i++ {
				passSlice[i] = charset[rand.Intn(len(charset))]
			}

			password = string(passSlice)
		} else {
			password = input
		}

		fmt.Print("Email: ")
		fmt.Scanln(&input)
		u.Email = input

		err := u.FromNew(password, dbmap)
		if err != nil {
			fmt.Println("Failed to create user: " + err.Error())
			return
		}

		fmt.Println("User initialization appears to have succeeded.")
		fmt.Println("You are about to create a user with the following attributes:")
		fmt.Printf("\n%+v\n\n", u)
		fmt.Print("Is this okay? [yN] ")
		fmt.Scanln(&input)

		if input != "y" && input != "yes" && input != "Y" {
			fmt.Println("Abort.")
			return
		}

		fmt.Println("Creating user.")

		if Secrets["env"] != "DEV" {
			stripe.Key = Secrets["stripe_apiKey"]

			custParams := &stripe.CustomerParams{}

			custParams.Email = u.Email
			custParams.Plan = "nanote_free"

			custParams.AddMeta("n_username", u.Username)

			cust, err := customer.New(custParams)

			if err != nil {
				fmt.Println("Stripe call failed: " + err.Error())
				return
			}

			fmt.Println("Stripe call succeeded!")

			u.StripeId = cust.ID
		} else {
			u.StripeId = "" // no Stripe ID in dev mode
			fmt.Println("Dev mode, skipping Stripe call.")
		}

		err = dbmap.Insert(&u)
		if err != nil {
			fmt.Println("Failed to insert new user: " + err.Error())
			return
		}

		// send email
		if Secrets["sg_apiKey"] == "" {
			fmt.Println("Stripe API key not set!")
			return
		}

		var messageHtml = "<p>Hello, " + u.Username + "!</p>" +
			"<p>Your Nanote account has been created!</p>" +
			"<p>Username: " + u.Username + "</p>" +
			"<p>Password: " + password + "</p>" +
			"<p><b>Please change this password immediately.</b> It has been transmitted in plain text and is no longer safe.</p>" +
			"<p>To activate your account, <a href='https://nanote.co/verify/" + u.Email + "&" + u.Verify + "'>click here</a> or visit the following url to verify your account:</p>" +
			"<p>https://nanote.co/verify/" + u.Email + "&" + u.Verify + "</p>" +
			"If you need anything, feel free to <a href='mailto:kevin@kevinywu.com'>email me</a> or tweet <a href='https://twitter.com/nanote_co'>@nanote_co</a>. I hope you find Nanote productive and useful!</p>" +
			"<p>Best regards, <br />Kevin Wu</p>"
		messageData := struct {
			sender    *mail.Email
			recipient *mail.Email
			subject   string
			message   *mail.Content
		}{
			sender: mail.NewEmail("Nanote registration", "register@mail.nanote.co"),
			recipient: mail.NewEmail(u.Username, u.Email),
			subject: "Welcome to Nanote!",
			message: mail.NewContent("text/html", messageHtml),
		}

		message := mail.NewV3MailInit(messageData.sender, messageData.subject, messageData.recipient, messageData.message)

		request := sendgrid.GetRequest(Secrets["sg_apiKey"], "/v3/mail/send", "https://api.sendgrid.com")
		request.Method = "POST"
		request.Body = mail.GetRequestBody(message)

		if Secrets["env"] != "DEV" {
			response, err := sendgrid.API(request)
			if err != nil {
				fmt.Println("Error sending mail: " + err.Error())
				return
			}

			fmt.Println("Sendgrid call: " + strconv.Itoa(response.StatusCode))
			fmt.Println(response.Body)
			fmt.Println(response.Headers)
		} else {
			fmt.Println("Dev environment, not sending email.")
		}

		// cloudflare update
		var content = []byte(`{"type": "CNAME", "name": "` + u.Username + `", "content": "nanote.co", "proxied": true}`)
		req, err := http.NewRequest("POST", "https://api.cloudflare.com/client/v4/zones/" + Secrets["cf_zoneId"] + "/dns_records", bytes.NewBuffer(content))
		if err != nil {
			fmt.Println("Failed to create HTTP request: " + err.Error())
			return
		}

		req.Header.Set("X-Auth-Email", "kevin@kevinywu.com") // don't make me regret this
		req.Header.Set("X-Auth-Key", Secrets["cf_apiKey"])
		req.Header.Set("Content-Type", "application/json")

		cfClient := &http.Client{}

		if Secrets["env"] == "PRO" {
			resp, err := cfClient.Do(req)

			if err != nil {
				fmt.Println("Failed to make API call to register CloudFlare entry for username: " + u.Username)
				return
			}

			if resp.StatusCode != http.StatusOK {
				fmt.Println("Failed to register CloudFlare entry for username: " + u.Username)
				return
			}
		} else {
			fmt.Println("Not production environment, not updating CloudFlare.")
		}
		fmt.Println("Success! Verification link: https://nanote.co/verify/" + u.Email + "&" + u.Verify)
		return
	case "sanity-check":
		fmt.Println("Sanity checking database...")

		fmt.Println("Checking for orphaned note-category mapping entries.")

		noteOrphans := make([]struct {
			NoteId   sql.NullInt64 `db:"note_id"`
			CatmapId sql.NullInt64 `db:"catmap_id"`
		}, 0)

		_, err := dbmap.Select(&noteOrphans, "SELECT notes.id AS note_id,catmap.id AS catmap_id FROM notes RIGHT OUTER JOIN catmap ON notes.id = catmap.note_id WHERE notes.id IS NULL")

		if err != nil {
			fmt.Println("Error: " + err.Error())
		}

		if len(noteOrphans) == 0 {
			fmt.Println("Looks good: No category mappings without notes.")
		} else {
			fmt.Println("WARNING: Category mappings found without notes:")
			fmt.Printf("\n%+v\n", &noteOrphans)
		}

		catOrphans := make([]struct {
			CatId    sql.NullInt64 `db:"category_id"`
			CatmapId sql.NullInt64 `db:"catmap_id"`
		}, 0)

		_, err = dbmap.Select(&catOrphans, "SELECT categories.id AS category_id,catmap.id AS catmap_id from categories RIGHT OUTER JOIN catmap ON categories.id = catmap.cat_id WHERE categories.id IS NULL")

		if err != nil {
			fmt.Println("Error: " + err.Error())
		}

		if len(catOrphans) == 0 {
			fmt.Println("Looks good: No category mappings without categories.")
		} else {
			fmt.Println("WARNING: Category mappings found without categories:")
			fmt.Printf("\n%+v\n", &catOrphans)
		}

		return
	case "help":
		return
	default:
		printHelp()
	}

}

func initDb() *gorp.DbMap {
	var dsn string

	if Secrets["env"] == "PRO" {
		dsn = "nanote:" + Secrets["mysql_pw"] + "@/nanote"
	} else {
		dsn = "nanote:nanote@/nanote"
	}

	db, err := sql.Open("mysql", dsn)
	if err != nil {
		fmt.Println("Failed to open database.")
		panic(err)
	}

	dbmap := &gorp.DbMap{
		Db:      db,
		Dialect: gorp.MySQLDialect{
			Engine: "InnoDB",
			Encoding: "utf8mb4",
		},
	}

	dbmap.AddTableWithName(proto.User{}, "users").SetKeys(true, "Id")
	dbmap.AddTableWithName(proto.Note{}, "notes").SetKeys(true, "Id")
	dbmap.AddTableWithName(proto.Category{}, "categories").SetKeys(true, "Id")
	dbmap.AddTableWithName(proto.Catmap{}, "catmap").SetKeys(true, "Id")
	dbmap.AddTableWithName(proto.Session{}, "sessions").SetKeys(false, "Id")

	_, err = dbmap.Exec("TRUNCATE sessions")
	if err != nil {
		fmt.Println("Warning: couldn't clean up session table before startup.")
		panic(err)
	}

	return dbmap
}

func printHelp() {
	fmt.Println(`Usage:
	nanote [command] [options] [flags]

If run without a command, Nanote will attempt to launch a server.

Available commands:
	create-user [username]
		Creates user with username [username].
	run
		Launches a server on port 8080.
	sanity-check
		Runs a sanity check on the database, checking for any orphaned map entries.
	start
		Alias for "run".
`)
}

#!/bin/bash

# a few sanity checks
if [ "$HOSTNAME" != "nanote" ]
then
	echo "Not running on deployment server!"
	exit 1
fi

if [ "$(whoami)" != "nanote" ]
then
	echo "Not running as nanote user!"
	exit 1
fi

if [ -n "$GOPATH" ]
then
	echo "GOPATH set to $GOPATH"
else
	echo "GOPATH not set! Setting it manually."
	export GOPATH=/home/nanote/go
fi

echo "Fetching dependencies."
go get
echo "Building."
go build
if [ $? -eq 0 ]; then
	echo "Build successful."
else
	echo "Build failed!"
	exit 1
fi

# If Gulp builds need to be run, do that here
echo "Minifying JS and CSS."
gulp deploy

rm -rf $GOPATH/bin/html
rm -rf $GOPATH/bin/static

cp -r html $GOPATH/bin
cp -r static $GOPATH/bin

cp reserved-names.json $GOPATH/bin

echo "Purging CloudFlare cache."

export CF_ZONEID=$(jq -r '.cf_zoneId' $GOPATH/bin/secrets.json)
export CF_APIKEY=$(jq -r '.cf_apiKey' $GOPATH/bin/secrets.json)

echo "Using Zone ID $CF_ZONEID"
echo "Using API Key $CF_APIKEY"

CF_SUCCESS=`curl -X DELETE "https://api.cloudflare.com/client/v4/zones/$CF_ZONEID/purge_cache" \
-H "X-Auth-Email: kevin@kevinywu.com" \
-H "X-Auth-Key: $CF_APIKEY" \
-H "Content-Type: application/json" \
--data '{"purge_everything":true}' | jq -r '.success'`

echo "Success: $CF_SUCCESS" 

if [ "$CF_SUCCESS" = "true" ]; then
	echo "CloudFlare cache purged successfully."
else
	echo "Failed to purge CloudFlare cache!"
	exit 1
fi

echo "Deployment complete. Run systemctl restart nanote to start the updated server!"

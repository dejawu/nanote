package seshmgr

import (
	"encoding/base64"
	"errors"
	"github.com/op/go-logging"
	"kywu.org/nanote/proto"
	"net/http"
	"strings"
	"time"

	"gopkg.in/gorp.v1"

	"github.com/gorilla/securecookie"
)

/*
This is a bit of a misnomer.
A more appropriate name would be something like "SessionManager"

This holds the server-side information to decrypt and handle users' secure cookies.
It also defines the hashkey and blockkey used to keep cookies secure.
*/
type SessionManager struct {
	cHandler *securecookie.SecureCookie
	dbmap    *gorp.DbMap
	log      *logging.Logger
}

func Init(d *gorp.DbMap) *SessionManager {
	return &SessionManager{
		cHandler: securecookie.New(
			securecookie.GenerateRandomKey(64),
			securecookie.GenerateRandomKey(32)),
		dbmap: d,
		log:   logging.MustGetLogger("seshmgr"),
	}
}

// begins a session for the user with given username and ID.
func (sm SessionManager) Start(uId int, w http.ResponseWriter) error {
	// clear existing session in DB if one exists
	existingSesh := &proto.Session{}
	err := existingSesh.FromIdWithoutTimestamp(uId, sm.dbmap)
	if err == nil {
		err = existingSesh.Delete(sm.dbmap)
		if err != nil {
			return err
		}
	} /*else {
		sm.log.Info("No existing session found in DB.")
	}*/

	key := base64.URLEncoding.EncodeToString(securecookie.GenerateRandomKey(64))

	sessData := map[string]string{
		"key": key,
	}

	encoded, err := sm.cHandler.Encode("nanote", sessData)
	if err != nil {
		return err
	}

	sesh := &proto.Session{
		Id:      uId,
		Key:     key,
		Expires: time.Now().Add(time.Hour * 24 * 7).Unix(),
	}

	err = sesh.Create(sm.dbmap)
	if err != nil {
		return err
	}

	cookie := &http.Cookie{
		Name:   "nanote",
		Value:  encoded,
		Path:   "/",
		MaxAge: 60 * 60 * 24 * 14,
		// Secure:   true, // secure isn't possible now because we don't have HTTPS/TLS
		HttpOnly: true,
	}

	http.SetCookie(w, cookie)

	return nil
}

func (sm SessionManager) End(r *http.Request, w http.ResponseWriter) error {
	sesh, err := sm.getSessionFromCookie(r)
	if err == nil { // there might not necessarily be a session to delete
		err = sesh.Delete(sm.dbmap)
		if err != nil {
			return err
		}
	} else {
		sm.log.Info("Couldn't find matching session to end: " + err.Error())
	}

	cookie := &http.Cookie{
		Name:   "nanote",
		Value:  "",
		Path:   "/",
		MaxAge: -1,
	}

	http.SetCookie(w, cookie)

	return nil
}

// TODO refactor
// returns true or false whether user is logged in
func (sm SessionManager) Check(r *http.Request) bool {
	u := proto.User{}

	id := -1

	if r.FormValue("api_key") != "" {
		keyArray := strings.Split(r.FormValue("api_key"), ":")

		if len(keyArray) != 2 {
			return false
		}

		un := keyArray[0]
		key := keyArray[1]

		err := u.FromApiKey(un, key, sm.dbmap)
		if err != nil {
			return false
		}

		id = u.Id
	} else {
		sesh, err := sm.getSessionFromCookie(r)
		if err != nil {
			return false
		}

		id = sesh.Id
	}

	return id != -1
}

// returns user struct populated with data of current user in session
func (sm SessionManager) User(r *http.Request) (*proto.User, error) {
	u := proto.User{}
	var err error
	err = nil

	if r.FormValue("api_key") != "" {
		keyArray := strings.Split(r.FormValue("api_key"), ":")

		if len(keyArray) != 2 {
			return nil, errors.New("Invalid API key.")
		}

		un := keyArray[0]
		key := keyArray[1]

		err = u.FromApiKey(un, key, sm.dbmap)
	} else {
		sesh, err := sm.getSessionFromCookie(r)

		if err != nil {
			return nil, errors.New("Invalid user session: " + err.Error())
		}

		err = u.FromId(sesh.Id, sm.dbmap)
	}

	return &u, err
}

// returns key stored in cookie
func (sm SessionManager) getSessionFromCookie(r *http.Request) (proto.Session, error) {
	cookie, err := r.Cookie("nanote")
	if err != nil {
		return proto.Session{}, err
	}

	decoded := make(map[string]string)
	err = sm.cHandler.Decode("nanote", cookie.Value, &decoded)
	key := decoded["key"]

	sesh := proto.Session{}
	err = sesh.FromKey(key, sm.dbmap)
	if err != nil {
		return proto.Session{}, err
	}

	return sesh, err
}

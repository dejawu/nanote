package main

import (
	"fmt"
	"net/http"
	"net/url"
	//"net/http/httptest"
	"database/sql"
	"encoding/json"
	"io/ioutil"
	"os"
	"strconv"
	"strings"
	"testing"
	"time"

	"kywu.org/nanote/proto"

	"gopkg.in/gorp.v1"

	_ "github.com/go-sql-driver/mysql"
	. "github.com/smartystreets/goconvey/convey"
)

var dbmap *gorp.DbMap

func TestMain(m *testing.M) {
	fmt.Println("Reading config.")
	s, err := ioutil.ReadFile("secrets.json")
	if err != nil {
		fmt.Println("Failed to read secrets file.")
		panic(err)
	}

	err = json.Unmarshal(s, &Secrets)

	if err != nil {
		panic("Error in Secrets file.")
	}

	if Secrets["env"] == "PRO" {
		panic("Test suite cannot be run in production environment!")
	}

	fmt.Println("Connecting to database.")

	// database setup MUST match the setup in main.go
	db, err := sql.Open("mysql", "nanote:nanote@/nanote")

	if err != nil {
		fmt.Println("Failed to open database.")
		panic(err)
	}

	dbmap = &gorp.DbMap{
		Db:      db,
		Dialect: gorp.MySQLDialect{"InnoDB", "UTF-8"},
	}

	dbmap.AddTableWithName(proto.User{}, "users").SetKeys(true, "Id")
	dbmap.AddTableWithName(proto.Note{}, "notes").SetKeys(true, "Id")
	dbmap.AddTableWithName(proto.Category{}, "categories").SetKeys(true, "Id")
	dbmap.AddTableWithName(proto.Catmap{}, "catmap").SetKeys(true, "Id")

	defer dbmap.Db.Close()

	fmt.Println("Cleaning up database.")

	_, err = dbmap.Exec("DELETE FROM `users` WHERE (username='autotest' OR email='test@nanote.co')")

	if err != nil && err != sql.ErrNoRows {
		panic("Failed to clean up database:" + err.Error())
	}

	fmt.Println("Starting tests.")

	// no idea if this is okay but it seems to work
	go main()

	// wait a few seconds to let the server start completely
	time.Sleep(time.Second * 2)

	os.Exit(m.Run())

	fmt.Println("Cleaning up.")
}

// response tests (ping Nanote server)
func TestAuthRegister(t *testing.T) {
	SkipConvey("Testing registration", t, func() {

		sampleFields := map[string]string{
			"username": "regtest",
			"password": "regtest",
			"email":    "test@nanote.co",
			// "cc_name":   "Otto Test",
			// "cc_number": "4242424242424242",
			// "cc_month":  "01",
			// "cc_year":   "18",
			// "cc_code":   "666",
		}

		textFields := []string{}

		Convey("Missing fields", func() {
			// make one test request for each key
			for testKey, _ := range sampleFields {
				form := url.Values{}
				// correctly fill in all keys except the one we're testing
				for key, val := range sampleFields {
					if key == testKey {
						// don't add anything
					} else {
						form.Add(key, val)
					}
				}
				req, err := http.NewRequest("POST", "http://"+Secrets["domain"]+"/api/auth/register", strings.NewReader(form.Encode()))
				req.Header.Add("Content-Type", "application/x-www-form-urlencoded")

				htClient := &http.Client{}
				resp, err := htClient.Do(req)

				So(err, ShouldBeNil)                                    // request should still succeed
				So(resp.StatusCode, ShouldEqual, http.StatusBadRequest) // but request should be bad
			}
		})

		// no fields are allowed to be empty
		Convey("Empty fields", func() {
			registerTryAllFields(sampleFields, "", http.StatusBadRequest)
		})

		textFields = []string{"username", "email", "cc_number", "cc_month", "cc_year", "cc_code"}
		Convey("Fields with length limits that are too long", func() {
			registerTrySomeFields(sampleFields, textFields, "99999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999", http.StatusBadRequest)
		})

		textFields = []string{"password", "cc_name"}
		Convey("Fields that should accept escape characters", func() {
			registerTrySomeFields(sampleFields, textFields, "~#$%&*+\\\\|<>;\"'?/={}[]*", http.StatusOK)
		})

		textFields = []string{"username", "email", "cc_number", "cc_month", "cc_year", "cc_code"}
		Convey("Fields that should refuse escape characters", func() {
			registerTrySomeFields(sampleFields, textFields, "~#$%&*+\\\\|<>;\"'?/={}[]*", http.StatusBadRequest)
		})

		textFields = []string{"password"}
		Convey("Non-ASCII characters in fields that should accept them", func() {
			registerTrySomeFields(sampleFields, textFields, "!\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOP", http.StatusOK)
			registerTrySomeFields(sampleFields, textFields, "QRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~", http.StatusOK)

			registerTrySomeFields(sampleFields, textFields, "☃¡¢£¤¥¦§¨©ª«¬­®¯°±²³´µ¶·¸¹º»¼½¾¿ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐ", http.StatusOK)
			registerTrySomeFields(sampleFields, textFields, "ÑÒÓÔÕÖ×ØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõö÷øùúûüýþÿ", http.StatusOK)

			registerTrySomeFields(sampleFields, textFields, "ĀāĂăĄąĆćĈĉĊċČčĎďĐđĒēĔĕĖėĘęĚěĜĝĞğĠġĢģĤĥĦħĨĩĪīĬĭĮįİ", http.StatusOK)
			registerTrySomeFields(sampleFields, textFields, "ıĲĳĴĵĶķĸĹĺĻļĽľĿŀŁłŃńŅņŇňŉŊŋŌōŎŏŐőŒœŔŕŖŗŘřŚśŜŝŞşŠšŢţŤťŦŧŨũŪūŬŭŮůŰűŲųŴŵŶŷŸŹźŻżŽžſ", http.StatusOK)

			registerTrySomeFields(sampleFields, textFields, "ƀƁƂƃƄƅƆƇƈƉƊƋƌƍƎƏƐƑƒƓƔƕƖƗƘƙƚƛƜƝƞƟƠơƢƣƤƥƦƧƨƩƪƫƬƭƮƯư", http.StatusOK)
			registerTrySomeFields(sampleFields, textFields, "ƱƲƳƴƵƶƷƸƹƺƻƼƽƾƿǀǁǂǃǄǅǆǇǈǉǊǋǌǍǎǏǐǑǒǓǔǕǖǗǘǙǚǛǜǝǞǟǠǡǢ", http.StatusOK)
			registerTrySomeFields(sampleFields, textFields, "ǣǤǥǦǧǨǩǪǫǬǭǮǯǰǱǲǳǴǵǶǷǸǹǺǻǼǽǾǿ", http.StatusOK)

			registerTrySomeFields(sampleFields, textFields, "ɐɑɒɓɔɕɖɗɘəɚɛɜɝɞɟɠɡɢɣɤɥɦɧɨɩɪɫɬɭɮɯɰɱɲɳɴɵɶɷɸɹɺɻɼɽɾɿʀ", http.StatusOK)
			registerTrySomeFields(sampleFields, textFields, "ʁʂʃʄʅʆʇʈʉʊʋʌʍʎʏʐʑʒʓʔʕʖʗʘʙʚʛʜʝʞʟʠʡʢʣʤʥʦʧʨʩʪʫʬʭ", http.StatusOK)

			registerTrySomeFields(sampleFields, textFields, "ʰʱʲʳʴʵʶʷʸʹʺʻʼʽʾʿˀˁ˂˃˄˅ˆˇˈˉˊˋˌˍˎˏːˑ˒˓˔˕˖˗˘˙˚˛˜˝˞˟ˠ", http.StatusOK)
			registerTrySomeFields(sampleFields, textFields, "ˡˢˣˤ˥˦˧˨˩˪˫ˬ˭ˮ", http.StatusOK)

			registerTrySomeFields(sampleFields, textFields, "̡̢̧̨̛̖̗̘̙̜̝̞̟̠̣̤̥̦̩̪̫̬̭̮̯̰̀́̂̃̄̅̆̇̈̉̊̋̌̍̎̏̐̑̒̓̔̕̚", http.StatusOK)
			registerTrySomeFields(sampleFields, textFields, "̴̵̶̷̸̱̲̳̹̺̻̼͇͈͉͍͎̽̾̿̀́͂̓̈́͆͊͋͌ͅ͏ͣͤͥͦͧͨͩͪͫͬͭͮͯ͢͠͡", http.StatusOK)

			registerTrySomeFields(sampleFields, textFields, "ʹ͵ͺ;΄΅Ά·ΈΉΊΌΎΏΐΑΒΓΔΕΖΗΘΙΚΛΜΝΞΟΠΡΣΤΥΦΧΨΩΪΫάέήίΰαβγ", http.StatusOK)
			registerTrySomeFields(sampleFields, textFields, "δεζηθικλμνξοπρςστυφχψωϊϋόύώϐϑϒϓϔϕϖϗϘϙϚϛϜϝϞϟϠϡϢϣϤϥϦ", http.StatusOK)
			registerTrySomeFields(sampleFields, textFields, "ϧϨϩϪϫϬϭϮϯϰϱϲϳϴϵ϶", http.StatusOK)

			registerTrySomeFields(sampleFields, textFields, "ЀЁЂЃЄЅІЇЈЉЊЋЌЍЎЏАБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯа", http.StatusOK)
			registerTrySomeFields(sampleFields, textFields, "бвгдежзийклмнопрстуфхцчшщъыьэюяѐёђѓєѕіїјљњћќѝўџѠѡѢ", http.StatusOK)
			registerTrySomeFields(sampleFields, textFields, "ѣѤѥѦѧѨѩѪѫѬѭѮѯѰѱѲѳѴѵѶѷѸѹѺѻѼѽѾѿ...", http.StatusOK)

			registerTrySomeFields(sampleFields, textFields, "ԱԲԳԴԵԶԷԸԹԺԻԼԽԾԿՀՁՂՃՄՅՆՇՈՉՊՋՌՍՎՏՐՑՒՓՔՕՖՙ՚՛՜՝՞՟աբգդ", http.StatusOK)
			registerTrySomeFields(sampleFields, textFields, "եզէըթժիլխծկհձղճմյնշոչպջռսվտրցւփքօֆև։֊", http.StatusOK)

			registerTrySomeFields(sampleFields, textFields, "ְֱֲֳִֵֶַָֹֻּֽ֑֖֛֣֤֥֦֧֪֚֭֮֒֓֔֕֗֘֙֜֝֞֟֠֡֨֩֫֬֯־ֿ׀ׁׂ׃", http.StatusOK)
			registerTrySomeFields(sampleFields, textFields, "ׄאבגדהוזחטיךכלםמןנסעףפץצקרשתװױײ׳״", http.StatusOK)

			registerTrySomeFields(sampleFields, textFields, "،؛؟ءآأؤإئابةتثجحخدذرزسشصضطظعغـفقكلمنهوىيًٌٍَُِّْٓ", http.StatusOK)
			registerTrySomeFields(sampleFields, textFields, "ٕٔ٠١٢٣٤٥٦٧٨٩٪٫٬٭ٮٯٰٱٲٳٴٵٶٷٸٹٺٻټٽپٿڀځڂڃڄڅچڇڈډڊڋڌڍڎڏ", http.StatusOK)
			registerTrySomeFields(sampleFields, textFields, "ڐڑڒړڔڕږڗژڙښڛڜڝڞڟڠڡڢڣڤڥڦڧڨکڪګڬ...", http.StatusOK)

			registerTrySomeFields(sampleFields, textFields, "܀܁܂܃܄܅܆܇܈܉܊܋܌܍܏ܐܑܒܓܔܕܖܗܘܙܚܛܜܝܞܟܠܡܢܣܤܥܦܧܨܩܪܫܬܱܴܰܲܳ", http.StatusOK)
			registerTrySomeFields(sampleFields, textFields, "ܷܸܹܻܼܾ݂݄݆݈ܵܶܺܽܿ݀݁݃݅݇݉݊", http.StatusOK)

			registerTrySomeFields(sampleFields, textFields, "ԀԁԂԃԄԅԆԇԈԉԊԋԌԍԎԏ", http.StatusOK)
		})

		textFields = []string{"username", "email", "cc_number", "cc_month", "cc_year", "cc_code"}
		Convey("Non-ASCII characters in fields that should NOT accept them", func() {
			registerTrySomeFields(sampleFields, textFields, "!\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOP", http.StatusBadRequest)
			registerTrySomeFields(sampleFields, textFields, "QRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~", http.StatusBadRequest)

			registerTrySomeFields(sampleFields, textFields, "☃¡¢£¤¥¦§¨©ª«¬­®¯°±²³´µ¶·¸¹º»¼½¾¿ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐ", http.StatusBadRequest)
			registerTrySomeFields(sampleFields, textFields, "ÑÒÓÔÕÖ×ØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõö÷øùúûüýþÿ", http.StatusBadRequest)

			registerTrySomeFields(sampleFields, textFields, "ĀāĂăĄąĆćĈĉĊċČčĎďĐđĒēĔĕĖėĘęĚěĜĝĞğĠġĢģĤĥĦħĨĩĪīĬĭĮįİ", http.StatusBadRequest)
			registerTrySomeFields(sampleFields, textFields, "ıĲĳĴĵĶķĸĹĺĻļĽľĿŀŁłŃńŅņŇňŉŊŋŌōŎŏŐőŒœŔŕŖŗŘřŚśŜŝŞşŠšŢţŤťŦŧŨũŪūŬŭŮůŰűŲųŴŵŶŷŸŹźŻżŽžſ", http.StatusBadRequest)

			registerTrySomeFields(sampleFields, textFields, "ƀƁƂƃƄƅƆƇƈƉƊƋƌƍƎƏƐƑƒƓƔƕƖƗƘƙƚƛƜƝƞƟƠơƢƣƤƥƦƧƨƩƪƫƬƭƮƯư", http.StatusBadRequest)
			registerTrySomeFields(sampleFields, textFields, "ƱƲƳƴƵƶƷƸƹƺƻƼƽƾƿǀǁǂǃǄǅǆǇǈǉǊǋǌǍǎǏǐǑǒǓǔǕǖǗǘǙǚǛǜǝǞǟǠǡǢ", http.StatusBadRequest)
			registerTrySomeFields(sampleFields, textFields, "ǣǤǥǦǧǨǩǪǫǬǭǮǯǰǱǲǳǴǵǶǷǸǹǺǻǼǽǾǿ", http.StatusBadRequest)

			registerTrySomeFields(sampleFields, textFields, "ɐɑɒɓɔɕɖɗɘəɚɛɜɝɞɟɠɡɢɣɤɥɦɧɨɩɪɫɬɭɮɯɰɱɲɳɴɵɶɷɸɹɺɻɼɽɾɿʀ", http.StatusBadRequest)
			registerTrySomeFields(sampleFields, textFields, "ʁʂʃʄʅʆʇʈʉʊʋʌʍʎʏʐʑʒʓʔʕʖʗʘʙʚʛʜʝʞʟʠʡʢʣʤʥʦʧʨʩʪʫʬʭ", http.StatusBadRequest)

			registerTrySomeFields(sampleFields, textFields, "ʰʱʲʳʴʵʶʷʸʹʺʻʼʽʾʿˀˁ˂˃˄˅ˆˇˈˉˊˋˌˍˎˏːˑ˒˓˔˕˖˗˘˙˚˛˜˝˞˟ˠ", http.StatusBadRequest)
			registerTrySomeFields(sampleFields, textFields, "ˡˢˣˤ˥˦˧˨˩˪˫ˬ˭ˮ", http.StatusBadRequest)

			registerTrySomeFields(sampleFields, textFields, "̡̢̧̨̛̖̗̘̙̜̝̞̟̠̣̤̥̦̩̪̫̬̭̮̯̰̀́̂̃̄̅̆̇̈̉̊̋̌̍̎̏̐̑̒̓̔̕̚", http.StatusBadRequest)
			registerTrySomeFields(sampleFields, textFields, "̴̵̶̷̸̱̲̳̹̺̻̼͇͈͉͍͎̽̾̿̀́͂̓̈́͆͊͋͌ͅ͏ͣͤͥͦͧͨͩͪͫͬͭͮͯ͢͠͡", http.StatusBadRequest)

			registerTrySomeFields(sampleFields, textFields, "ʹ͵ͺ;΄΅Ά·ΈΉΊΌΎΏΐΑΒΓΔΕΖΗΘΙΚΛΜΝΞΟΠΡΣΤΥΦΧΨΩΪΫάέήίΰαβγ", http.StatusBadRequest)
			registerTrySomeFields(sampleFields, textFields, "δεζηθικλμνξοπρςστυφχψωϊϋόύώϐϑϒϓϔϕϖϗϘϙϚϛϜϝϞϟϠϡϢϣϤϥϦ", http.StatusBadRequest)
			registerTrySomeFields(sampleFields, textFields, "ϧϨϩϪϫϬϭϮϯϰϱϲϳϴϵ϶", http.StatusBadRequest)

			registerTrySomeFields(sampleFields, textFields, "ЀЁЂЃЄЅІЇЈЉЊЋЌЍЎЏАБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯа", http.StatusBadRequest)
			registerTrySomeFields(sampleFields, textFields, "бвгдежзийклмнопрстуфхцчшщъыьэюяѐёђѓєѕіїјљњћќѝўџѠѡѢ", http.StatusBadRequest)
			registerTrySomeFields(sampleFields, textFields, "ѣѤѥѦѧѨѩѪѫѬѭѮѯѰѱѲѳѴѵѶѷѸѹѺѻѼѽѾѿ...", http.StatusBadRequest)

			registerTrySomeFields(sampleFields, textFields, "ԱԲԳԴԵԶԷԸԹԺԻԼԽԾԿՀՁՂՃՄՅՆՇՈՉՊՋՌՍՎՏՐՑՒՓՔՕՖՙ՚՛՜՝՞՟աբգդ", http.StatusBadRequest)
			registerTrySomeFields(sampleFields, textFields, "եզէըթժիլխծկհձղճմյնշոչպջռսվտրցւփքօֆև։֊", http.StatusBadRequest)

			registerTrySomeFields(sampleFields, textFields, "ְֱֲֳִֵֶַָֹֻּֽ֑֖֛֣֤֥֦֧֪֚֭֮֒֓֔֕֗֘֙֜֝֞֟֠֡֨֩֫֬֯־ֿ׀ׁׂ׃", http.StatusBadRequest)
			registerTrySomeFields(sampleFields, textFields, "ׄאבגדהוזחטיךכלםמןנסעףפץצקרשתװױײ׳״", http.StatusBadRequest)

			registerTrySomeFields(sampleFields, textFields, "،؛؟ءآأؤإئابةتثجحخدذرزسشصضطظعغـفقكلمنهوىيًٌٍَُِّْٓ", http.StatusBadRequest)
			registerTrySomeFields(sampleFields, textFields, "ٕٔ٠١٢٣٤٥٦٧٨٩٪٫٬٭ٮٯٰٱٲٳٴٵٶٷٸٹٺٻټٽپٿڀځڂڃڄڅچڇڈډڊڋڌڍڎڏ", http.StatusBadRequest)
			registerTrySomeFields(sampleFields, textFields, "ڐڑڒړڔڕږڗژڙښڛڜڝڞڟڠڡڢڣڤڥڦڧڨکڪګڬ...", http.StatusBadRequest)

			registerTrySomeFields(sampleFields, textFields, "܀܁܂܃܄܅܆܇܈܉܊܋܌܍܏ܐܑܒܓܔܕܖܗܘܙܚܛܜܝܞܟܠܡܢܣܤܥܦܧܨܩܪܫܬܱܴܰܲܳ", http.StatusBadRequest)
			registerTrySomeFields(sampleFields, textFields, "ܷܸܹܻܼܾ݂݄݆݈ܵܶܺܽܿ݀݁݃݅݇݉݊", http.StatusBadRequest)

			registerTrySomeFields(sampleFields, textFields, "ԀԁԂԃԄԅԆԇԈԉԊԋԌԍԎԏ", http.StatusBadRequest)
		})

		Convey("Valid entry", func() {
			form := url.Values{}
			for key, val := range sampleFields {
				form.Add(key, val)

			}
			req, err := http.NewRequest("POST", "http://"+Secrets["domain"]+"/api/auth/register", strings.NewReader(form.Encode()))
			req.Header.Add("Content-Type", "application/x-www-form-urlencoded")

			htClient := &http.Client{}
			resp, err := htClient.Do(req)

			So(err, ShouldBeNil)

			respBody, _ := ioutil.ReadAll(resp.Body)
			fmt.Println(string(respBody))

			So(resp.StatusCode, ShouldEqual, http.StatusOK)
		})

		dbmap.Exec("DELETE FROM users WHERE `username`='regtest'")
	})
}

func TestAuthLogin(t *testing.T) {
	Convey("Testing login.", t, func() {
		u := proto.User{
			Username: "logintest",
			Passhash: "2YrYFJVJP_Gj_H6FSGQYKbO-v2XS4SIcDfN4I-vI", // for password "logintest"
			Passsalt: "daXaCYz-HxjJj748FhmQ20qiibskXi7TTClX-lxiWsU=",
			ApiKey:   "logintestkey",
			Email:    "login@test.nanote.co",
			Verify:   "",
			StripeId: "",
		}
		err := dbmap.Insert(&u)

		if err != nil {
			panic(err)
		}

		path := "http://" + Secrets["domain"] + "/api/auth/login"

		Convey("Very long inputs", func() {
			form := url.Values{}
			form.Add("username", "99999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999")
			form.Add("password", "99999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999")

			resp, err := httpRequest(path, form)

			So(err, ShouldBeNil)
			So(resp.StatusCode, ShouldEqual, http.StatusBadRequest)
		})

		Convey("Missing inputs", func() {
			resp, err := httpRequest(path, url.Values{})

			So(err, ShouldBeNil)
			So(resp.StatusCode, ShouldEqual, http.StatusBadRequest)
		})

		Convey("Blank inputs", func() {
			form := url.Values{}
			form.Add("username", "")
			form.Add("password", "")

			resp, err := httpRequest(path, url.Values{})

			So(err, ShouldBeNil)
			So(resp.StatusCode, ShouldEqual, http.StatusBadRequest)
		})

		Convey("Sensitive characters", func() {
			form := url.Values{}
			form.Add("username", "!\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOP")
			form.Add("password", "QRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~")

			resp, err := httpRequest(path, form)

			So(err, ShouldBeNil)
			So(resp.StatusCode, ShouldEqual, http.StatusBadRequest)
		})

		Convey("Non-ASCII characters", func() {
			form := url.Values{}
			form.Add("username", "ЀЁЂЃЄЅІЇЈЉЊЋЌЍЎЏАБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯа")
			form.Add("password", "ϧϨϩϪϫϬϭϮϯϰϱϲϳϴϵ϶")

			resp, err := httpRequest(path, form)

			So(err, ShouldBeNil)
			So(resp.StatusCode, ShouldEqual, http.StatusBadRequest)
		})

		Convey("The correct login", func() {
			form := url.Values{}
			form.Add("username", "logintest")
			form.Add("password", "logintest")

			resp, err := httpRequest(path, form)

			So(err, ShouldBeNil)
			So(resp.StatusCode, ShouldEqual, http.StatusOK)
		})

		dbmap.Exec("DELETE FROM users WHERE username='logintest'")
	})
}

func TestNotes(t *testing.T) {
	Convey("Testing notes.", t, func() {
		u := proto.User{
			Username: "notetest",
			Passhash: "qPnjVeY50qY6ANGgqsV-VytvAIdI6TwvrYKoi9mW", // for password "test"
			Passsalt: "BfEtkwRPlNa18ofBwhRI89Pk2Ko_Khmjc9wI0TnFN48=",
			ApiKey:   "notetestkey",
			Email:    "note@test.nanote.co",
			Verify:   "",
			StripeId: "",
		}
		err := dbmap.Insert(&u)

		if err != nil {
			panic(err)
		}

		Convey("CRUD", func() {
			thisNote := -1
			// Create
			form := url.Values{}

			form.Add("api_key", "notetest:notetestkey")

			resp, err := httpRequest("http://"+Secrets["domain"]+"/api/note/create", form)

			So(err, ShouldBeNil)
			So(resp.StatusCode, ShouldEqual, http.StatusOK)

			respBody, _ := ioutil.ReadAll(resp.Body)

			var v map[string]interface{}

			err = json.Unmarshal(respBody, &v)

			So(v["success"].(bool), ShouldBeTrue)
			So(err, ShouldBeNil)

			thisNote = int(v["id"].(float64))

			So(thisNote, ShouldNotEqual, -1)

			// Read
			form = url.Values{}

			form.Add("api_key", "notetest:notetestkey")
			form.Add("note", strconv.Itoa(thisNote))

			resp, err = httpRequest("http://"+Secrets["domain"]+"/api/note/view", form)

			So(err, ShouldBeNil)
			So(resp.StatusCode, ShouldEqual, http.StatusOK)

			respBody, _ = ioutil.ReadAll(resp.Body)
			v = map[string]interface{}{}
			err = json.Unmarshal(respBody, &v)

			So(err, ShouldBeNil)
			So(v["success"].(bool), ShouldBeTrue)

			p := v["note"].(map[string]interface{})

			So(p["title"].(string), ShouldEqual, "") // note created with empty title
			So(p["slug"].(string), ShouldEqual, "")  // empty slug

			// Update
			form = url.Values{}

			form.Add("api_key", "notetest:notetestkey")
			form.Add("note", strconv.Itoa(thisNote))
			form.Add("title", "New title")
			form.Add("slug", "new-title")
			form.Add("public", "1")
			form.Add("content", "New content!")

			resp, err = httpRequest("http://"+Secrets["domain"]+"/api/note/save", form)

			So(err, ShouldBeNil)
			So(resp.StatusCode, ShouldEqual, http.StatusOK)

			respBody, _ = ioutil.ReadAll(resp.Body)
			fmt.Println(string(respBody))
			v = map[string]interface{}{}
			err = json.Unmarshal(respBody, &v)

			So(err, ShouldBeNil)
			So(v["success"].(bool), ShouldBeTrue)

			// Read to verify changes
			form = url.Values{}

			form.Add("api_key", "notetest:notetestkey")
			form.Add("note", strconv.Itoa(thisNote))

			resp, err = httpRequest("http://"+Secrets["domain"]+"/api/note/view", form)

			So(err, ShouldBeNil)
			So(resp.StatusCode, ShouldEqual, http.StatusOK)

			respBody, _ = ioutil.ReadAll(resp.Body)
			fmt.Println(string(respBody))
			v = map[string]interface{}{}
			err = json.Unmarshal(respBody, &v)

			So(err, ShouldBeNil)
			So(v["success"].(bool), ShouldBeTrue)

			p = v["note"].(map[string]interface{})

			So(p["title"].(string), ShouldEqual, "New title")
			So(p["slug"].(string), ShouldEqual, "new-title")
			So(p["public"].(bool), ShouldBeTrue)
			So(p["content"].(string), ShouldEqual, "New content!")

			// Delete
			form = url.Values{}

			form.Add("api_key", "notetest:notetestkey")
			form.Add("note", strconv.Itoa(thisNote))

			resp, err = httpRequest("http://"+Secrets["domain"]+"/api/note/delete", form)

			So(err, ShouldBeNil)
			So(resp.StatusCode, ShouldEqual, http.StatusOK)
			respBody, _ = ioutil.ReadAll(resp.Body)
			fmt.Println(string(respBody))
			v = map[string]interface{}{}
			err = json.Unmarshal(respBody, &v)

			So(err, ShouldBeNil)
			So(v["success"].(bool), ShouldBeTrue)

			// Note should no longer exist
			form = url.Values{}

			form.Add("api_key", "notetest:notetestkey")
			form.Add("note", strconv.Itoa(thisNote))

			resp, err = httpRequest("http://"+Secrets["domain"]+"/api/note/view", form)

			So(err, ShouldBeNil)
			So(resp.StatusCode, ShouldEqual, http.StatusBadRequest)

			respBody, _ = ioutil.ReadAll(resp.Body)
			fmt.Println(string(respBody))
			v = map[string]interface{}{}
			err = json.Unmarshal(respBody, &v)

			So(err, ShouldBeNil)
			So(v["success"].(bool), ShouldBeFalse)

			// Cleanup just in case
			dbmap.Exec("DELETE FROM notes WHERE `slug`='automated-test-note'")
		})

		Convey("Roundtrip of sensitive characters", func() {

		})

		Convey("Edit lock behavior", func() {

		})

		Convey("Invalid fields", func() {

		})

		dbmap.Exec("DELETE FROM users WHERE `username`='notetest'")
	})

}

func TestCategories(t *testing.T) {
	Convey("Test category behavior", t, func() {
		u := proto.User{
			Username: "cattest",
			Passhash: "qPnjVeY50qY6ANGgqsV-VytvAIdI6TwvrYKoi9mW", // for password "test"
			Passsalt: "BfEtkwRPlNa18ofBwhRI89Pk2Ko_Khmjc9wI0TnFN48=",
			ApiKey:   "cattestkey",
			Email:    "cat@test.nanote.co",
			Verify:   "",
			StripeId: "",
		}
		err := dbmap.Insert(&u)

		if err != nil {
			panic(err)
		}

		Convey("CRUD", func() {
			// create
			form := url.Values{}
			form.Add("api_key", "cattest:cattestkey")
			form.Add("name", "New test category")

			resp, err := httpRequest("http://"+Secrets["domain"]+"/api/cat/create", form)

			So(err, ShouldBeNil)
			So(resp.StatusCode, ShouldEqual, http.StatusOK)

			respBody, _ := ioutil.ReadAll(resp.Body)
			fmt.Println(string(respBody))
			v := map[string]interface{}{}
			err = json.Unmarshal(respBody, &v)

			// read
			So(err, ShouldBeNil)
			So(v["success"].(bool), ShouldBeTrue)

			catId := v["id"].(float64)

			form = url.Values{}
			form.Add("api_key", "cattest:cattestkey")
			form.Add("category", strconv.Itoa(int(catId)))

			resp, err = httpRequest("http://"+Secrets["domain"]+"/api/cat/view", form)

			So(err, ShouldBeNil)
			So(resp.StatusCode, ShouldEqual, http.StatusOK)

			respBody, _ = ioutil.ReadAll(resp.Body)
			fmt.Println(string(respBody))
			v = map[string]interface{}{}
			err = json.Unmarshal(respBody, &v)

			So(err, ShouldBeNil)
			So(v["success"].(bool), ShouldBeTrue)

			// we'll test the actual catmap in another section

			// update
			form = url.Values{}
			form.Add("category", strconv.Itoa(int(catId)))
			form.Add("name", "new name")
			form.Add("api_key", "cattest:cattestkey")

			resp, err = httpRequest("http://"+Secrets["domain"]+"/api/cat/edit", form)

			So(err, ShouldBeNil)
			So(resp.StatusCode, ShouldEqual, http.StatusOK)

			respBody, _ = ioutil.ReadAll(resp.Body)
			fmt.Println(string(respBody))
			v = map[string]interface{}{}
			err = json.Unmarshal(respBody, &v)

			So(err, ShouldBeNil)
			So(v["success"].(bool), ShouldBeTrue)

			// verify change
			form = url.Values{}
			form.Add("api_key", "cattest:cattestkey")
			form.Add("category", strconv.Itoa(int(catId)))

			resp, err = httpRequest("http://"+Secrets["domain"]+"/api/cat/view", form)

			So(err, ShouldBeNil)
			So(resp.StatusCode, ShouldEqual, http.StatusOK)

			respBody, _ = ioutil.ReadAll(resp.Body)
			fmt.Println(string(respBody))
			v = map[string]interface{}{}
			err = json.Unmarshal(respBody, &v)

			So(err, ShouldBeNil)
			So(v["success"].(bool), ShouldBeTrue)
			So(v["name"].(string), ShouldEqual, "new name")

			// delete
			form = url.Values{}
			form.Add("api_key", "cattest:cattestkey")
			form.Add("category", strconv.Itoa(int(catId)))

			resp, err = httpRequest("http://"+Secrets["domain"]+"/api/cat/delete", form)

			So(err, ShouldBeNil)
			So(resp.StatusCode, ShouldEqual, http.StatusOK)

			// verify deletion
			form = url.Values{}
			form.Add("api_key", "cattest:cattestkey")
			form.Add("category", strconv.Itoa(int(catId)))

			resp, err = httpRequest("http://"+Secrets["domain"]+"/api/cat/view", form)

			So(err, ShouldBeNil)
			So(resp.StatusCode, ShouldEqual, http.StatusBadRequest)

			respBody, _ = ioutil.ReadAll(resp.Body)
			fmt.Println(string(respBody))
			v = map[string]interface{}{}
			err = json.Unmarshal(respBody, &v)

			So(err, ShouldBeNil)
			So(v["success"].(bool), ShouldBeFalse)

		})

		dbmap.Exec("DELETE FROM users WHERE `username`='cattest'")
	})
}

func TestNoteCat(t *testing.T) {
	Convey("Note-category interactions", t, func() {
		Convey("Add category to note", func() {

		})

		Convey("Remove category from note", func() {

		})

		Convey("Clean catmap when note deleted", func() {

		})

		Convey("Clean catmap when category deleted", func() {

		})
	})
}

func httpRequest(path string, form url.Values) (*http.Response, error) {
	req, err := http.NewRequest("POST", path, strings.NewReader(form.Encode()))
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	htClient := &http.Client{}
	resp, err := htClient.Do(req)

	if err != nil {
		return nil, err
	}

	return resp, nil
}

// for tests that should return errors, try all combinations where all fields are valid except for one, which is replaced with a test string
func registerTryAllFields(sampleFields map[string]string, substitute string, expectedHttpCode int) {
	for testKey, _ := range sampleFields {
		form := url.Values{}
		// correctly fill in all keys except the one we're testing
		for key, val := range sampleFields {
			if key == testKey {
				form.Add(key, substitute)
			} else {
				form.Add(key, val)
			}
		}

		Convey("With key "+testKey+" set to "+substitute, func() {
			req, err := http.NewRequest("POST", "http://"+Secrets["domain"]+"/api/auth/register", strings.NewReader(form.Encode()))
			req.Header.Add("Content-Type", "application/x-www-form-urlencoded")

			htClient := &http.Client{}
			resp, err := htClient.Do(req)

			So(err, ShouldBeNil) // request should still succeed

			//respBody, _ := ioutil.ReadAll(resp.Body)

			So(resp.StatusCode, ShouldEqual, expectedHttpCode) // but request should be bad

			dbmap.Exec("DELETE FROM `users` WHERE (username='autotest' OR email='test@nanote.co')")
		})
	}
}

// similar to tryAllFields, but only replaces the text fields
func registerTrySomeFields(sampleFields map[string]string, testFieldKeys []string, substitute string, expectedHttpCode int) {
	for testKey, _ := range sampleFields {
		form := url.Values{}

		substituted := false

		// correctly fill in all keys except the one we're testing
		for key, val := range sampleFields {
			if key == testKey && stringInSlice(key, testFieldKeys) {
				form.Add(key, substitute)
				substituted = true
			} else {
				form.Add(key, val)
			}
		}

		if substituted {
			Convey("With key "+testKey+" set to "+substitute, func() {

				req, err := http.NewRequest("POST", "http://"+Secrets["domain"]+"/api/auth/register", strings.NewReader(form.Encode()))
				req.Header.Add("Content-Type", "application/x-www-form-urlencoded")

				htClient := &http.Client{}
				resp, err := htClient.Do(req)

				So(err, ShouldBeNil) // request should still succeed

				respBody, _ := ioutil.ReadAll(resp.Body)

				if resp.StatusCode != expectedHttpCode {
					fmt.Println("With key " + testKey + " set to " + substitute + "," + string(respBody))
				}

				So(resp.StatusCode, ShouldEqual, expectedHttpCode) // but request should be bad

				dbmap.Exec("DELETE FROM `users` WHERE (username='autotest' OR email='test@nanote.co')")
			})
		}
	}
}

func stringInSlice(needle string, haystack []string) bool {
	for _, b := range haystack {
		if b == needle {
			return true
		}
	}
	return false
}

$(function () {
	evilIcons.renderSprite()
	evilIcons.renderIcons()

	setTimeout(function () {
		let $verifyMessage = $("#verify-message")
		$.ajax(
			"/api/auth/verify",
			{
				data: {
					"code": $("#verify-code").val(),
					"email": $("#verify-email").val()
				},
				dataType: "json",
				method: "POST",
				success: function () {
					$verifyMessage.find("div").remove()
					$verifyMessage.prepend('<div data-icon="ei-check"></div>')

					$verifyMessage.addClass("good")
					$verifyMessage.find("p").text("Verified! Logging you in now...")
					evilIcons.renderIcons()


					setTimeout(function () {
						window.location = "/my/dash"
					}, 2000)
				},
				error: function (jqxhr, status, error) {
					$verifyMessage.find("div").remove()
					$verifyMessage.prepend('<div data-icon="ei-exclamation"></div>')

					$verifyMessage.addClass("bad")

					if (status == "timeout") {
						$verifyMessage.find("p").text("Error: Could not reach server. Please try again later.")

					} else {
						$verifyMessage.find("p").text("Error: " + $.parseJSON(jqxhr.responseText).message)
					}

					evilIcons.renderIcons()
				}
			}
		)
	}, 2000)


})
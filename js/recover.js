$(function () {
	evilIcons.renderSprite()
	evilIcons.renderIcons()

	function attemptSubmit() {
		if ($("#recover-form-newpass").val() != $("#recover-form-passconfirm").val()) {
			$("#recover-message").hide()
			$("#recover-message p").attr("class", "bad").text("Password and confirmation do not match.")
			$("#recover-message").fadeIn(200)

			return
		}

		$.ajax(
			"/api/auth/recover",
			{
				method: "POST",
				dataType: "json",
				data: {
					newpass: $("#recover-form-newpass").val(),
					username: $("#recover-form-username").val(),
					code: $("#recover-code").val()
				},
				success: function (data) {
					$("#recover-message").hide()
					$("#recover-message p").attr("class", "good").text("Password updated successfully. You can now log in with your new password.")
					$("#recover-message").fadeIn(200)

					$("#header-auth-form").fadeIn(200)
					$("#header-auth-username").focus().val($("#recover-form-username").val())
					$("#header-auth-login").text("close")
				},
				error: function (jqxhr, status, error) {
					$("#recover-message").hide()
					$("#recover-message p").attr("class", "bad").text($.parseJSON(jqxhr.responseText).message)
					$("#recover-message").fadeIn(200)
				}
			}
		)
	}

	$("#recover-form form input").on("keydown", function (event) {
		if (event.which == 13) {
			event.preventDefault()
			attemptSubmit()
		}
	})

	$("#recover-submit").on("click", function (event) {
		event.preventDefault()
		attemptSubmit()
	})
})
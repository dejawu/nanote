$(function () {
	evilIcons.renderSprite()
	evilIcons.renderIcons()

	$("#register-username, #register-password, #register-password-confirm, #register-email, #register-cc-name, #register-cc-number, #register-cc-month, #register-cc-year, #register-cc-code").on("keydown", function (e) {
		if (e.which == 13) {
			attemptRegister()
		}
	})

	$("#register-submit").on("click", function (e) {
		e.preventDefault()
		attemptRegister()
	})

	$("#container form").on("submit", function (e) {
		e.preventDefault()
		attemptRegister()
	})

	function attemptRegister() {
		if ($("#register-password").val() !== $("#register-password-confirm").val()) {
			setMessage("Password does not match confirmation.", "ei-exclamation", "bad")
			return
		}

		$.ajax(
			"/api/auth/register",
			{
				method: "POST",
				data: {
					username: $("#register-username").val(),
					password: $("#register-password").val(),
					email: $("#register-email").val(),
					cc_name: $("#register-cc-name").val(),
					cc_number: $("#register-cc-number").val(),
					cc_month: $("#register-cc-month").val(),
					cc_year: $("#register-cc-year").val(),
					cc_code: $("#register-cc-code").val()
				},
				dataType: "json",
				success: function () {
					setMessage("Registration successful! Please check your email to activate your account - you'll have to do this before you can log in.", "ei-check", "good")
					$("input").val("")
				},
				error: function (jqxhr, status, error) {
					setMessage($.parseJSON(jqxhr.responseText).message, "ei-exclamation", "bad")
				}
			}
		)
	}

	function setMessage(message, icon, className) {
		$("#register-message").stop()
		$("#register-message").hide()

		$("#register-message p").text(message)
		$("#register-message div").remove()
		$("#register-message").prepend('<div data-icon="' + icon + '"></div>')
		$("#register-message").attr("class", className)
		evilIcons.renderIcons()

		$("#register-message").fadeIn()
	}
})

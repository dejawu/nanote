class Dashboard {
	constructor() {
		evilIcons.renderSprite()
		evilIcons.renderIcons()

		this.mobileMode = ($(window).width() <= 800)

		$(window).resize(() => {
			this.mobileMode = ($(window).width() <= 800)

			if(!this.mobileMode) {
				$("#dash-cats").show()
			}
		})

		// preview renderer setup
		this.sd = new MarkdownRenderer()
		// store jQuery objects (more efficient)
		this.$dashNotes = $("#dash-notes")
		this.$dashCats = $("#dash-cats")

		// store structure of all notes, used for sorting
		this.allNotes = []

		// array of notes currently being displayed


		$.ajax(
			"/api/user/notes",
			{
				data: {},
				type: "POST",
				dataType: "json",
				success: (data)=> {
					this.allNotes = data.notes
				}
			}
		)

		// store structure of all categories
		this.cats = []

		// register events

		// title fadeouts when scrolling
		$("#container div").on("scroll", (event)=>{
			$(".dash-section-title", event.currentTarget).stop()
			if (this.$dashNotes.scrollTop() < 40) {
				$(".dash-section-title", event.currentTarget).fadeIn(100)
			} else {
				$(".dash-section-title", event.currentTarget).fadeOut(100)
			}
		})

		// toggle sort options menu
		$("#dash-notes-sorting-toggle").on("click", (event)=> {
			let $dashNotesSortingOptions = $("#dash-notes-sorting-options")
			event.preventDefault()
			if($dashNotesSortingOptions.is(":visible")) {
				$dashNotesSortingOptions.slideUp(150)
			} else {
				$dashNotesSortingOptions.slideDown(150)
			}
		})

		// sort option
		// TODO optimize
		$(".dash-notes-sorting-option").on("click", (event)=> {
			event.preventDefault()
			let $currentTarget = $(event.currentTarget)
			let sortType = $currentTarget.data("sortType")


			let $divs = $(".dash-note-block")

			$divs.sort((a, b)=> {
				// get IDs for a and b, and look up data objects
				let aId = $(a).attr("id").replace("dash-note-","")
				let bId = $(b).attr("id").replace("dash-note-","")

				let aObj = {}
				let bObj = {}

				// probably faster than $.grep
				for(let i = 0; i < this.allNotes.length; i += 1) {
					if(this.allNotes[i].id === aId) {
						aObj = this.allNotes[i]
					}

					if(this.allNotes[i].id === bId) {
						bObj = this.allNotes[i]
					}
				}

				// reverse sort for updated and created
				if(sortType === "updated" || sortType === "created") {
					let tempObj = aObj;
					aObj = bObj;
					bObj = tempObj;
				}

				return (aObj[sortType] < bObj[sortType]) ? -1 : (aObj[sortType] > bObj[sortType]) ? 1 : 0
			})

			$divs.remove()

			for(let d = 0; d < $divs.length; d += 1) {
				$("#dash-notes").append($divs[d])
			}

			// $("#dash-notes-sorting-options").slideUp(150)
		})

		// create browser history for viewing a category
		if (this.mobileMode) {
			window.onpopstate = ()=> {
				this.$dashNotes.hide()
				this.$dashCats.show()
			}
		}

		// note selected
		this.$dashNotes.on("click", ".dash-note-block", (event) => {
			$(".dash-note-block").removeClass("dash-note-selected")
			// apply "selected" class
			$(event.currentTarget).addClass("dash-note-selected")

			let noteId = $(event.currentTarget).attr("id").replace("dash-note-", "")

			this.showNote(noteId)
		})


		$("#dash-new-category").on("click", (event)=> {
			event.preventDefault()

			Modal.prompt("New category name: (Categories with at least one public note will be visible on your profile.)", "neutral", (newCatName)=> {
				this.createCategory(newCatName)
			})
		})

		this.$dashCats.on("click", ".dash-cat-block", (event)=> {
			let $currentTarget = $(event.currentTarget)
			$(".dash-cat-block").removeClass("dash-cat-selected")
			$currentTarget.addClass("dash-cat-selected")

			let catId = $currentTarget.data("catid")

			// Show dash categories on a separate page if on mobile
			if (this.mobileMode) {
				setTimeout(()=> {
					this.$dashCats.hide()
					this.$dashNotes.show()
					window.history.pushState({}, "", "")
				}, 150)
			}

			// reset new-note button
			let $dashNewNote = $("#dash-new-note")
			$dashNewNote.attr("href", "/my/editor/new")
			$dashNewNote.find("span").text("New note")

			if (catId === "all") {
				$("#dash-notes").find(".dash-section-title").text("Notes")
				$(".dash-note-block").show()

				return
			}

			// hide all notes - needed for anything other than "all" notes
			$(".dash-note-block").hide()

			if (catId === "uncategorized") {
				$.ajax(
					"/api/user/notes",
					{
						data: {
							filter: "uncategorized"
						},
						type: "POST",
						dataType: "json",
						success: (data)=> {
							this.$dashNotes.find(".dash-section-title").text("Uncategorized")
							for (let i = 0; i < data.notes.length; i++) {
								$("#dash-note-" + data.notes[i].id).show()
							}
						},
						error: function (jqxhr, status, error) {

						}
					}
				)
				return
			}

			// at this point, we must be showing a user-defined category.
			let catName = $(event.currentTarget).find("a").text()

			this.$dashNotes.find(".dash-section-title").text(catName)
			$("#dash-new-note").attr("href", "/my/editor/new/c/" + catId)
			$("#dash-new-note").find("span").text("New note in " + catName)

			this.showCategory(catId)
		})


		this.$dashCats.on("click", ".dash-cat-block .dash-cat-edit", (event)=> {
			let $currentTarget = $(event.currentTarget)
			let catId = $currentTarget.closest(".dash-cat-block").data("catid")
			let catName = $currentTarget.closest(".dash-cat-block").find("a").text()
			Modal.prompt(`Rename \"${catName}\" to:`, "neutral", (newName) => {
				this.updateCategory(catId, newName, event)
			})
		})


		$(".dash-note-delete").on("click", (event)=> {
			let noteId = $(event.currentTarget).attr("id").replace("dash-note-delete-", "")
			let noteTitle = $("#dash-note-" + noteId + " .dash-note-title").text()
			event.preventDefault()
			Modal.confirm("Really delete the note \"" + noteTitle + "\"?", "bad", () => {
				this.deleteNote(noteId);
			})
		})

		this.$dashCats.on("click", ".dash-cat-block .dash-cat-delete", (event)=> {
			let catId = $(event.currentTarget).closest(".dash-cat-block").data("catid")
			let catName = $(event.currentTarget).closest(".dash-cat-block").find("a").text()
			Modal.confirm("Really delete the category \"" + catName + "\"? (No notes will be deleted.)", "bad", ()=> {
				this.deleteCategory(catId, event)
			})
		})
	}

	// create HTML for new category
	createCategory(name) {
		$.ajax(
			"/api/cat/create",
			{
				data: {
					name: name
				},
				type: "POST",
				dataType: "json",
				success: (data)=> {
					// we're gonna cheat a bit and use the all-categories entry as a sort of prototype
					let $newCat = $("#dash-cat-all").clone()

					$newCat.data("catid", data.id)
						.addClass("dash-cat-block")
						.appendTo("#dash-cats")
					$newCat.hide()
					$newCat.find(".dash-cat-title").text(name)
					$newCat.append('<span><div data-icon="ei-trash" class="dash-cat-delete"></div></span>')
					$newCat.append('<span><div data-icon="ei-pencil" class="dash-cat-edit"></div></span>')

					$newCat.slideDown(150)

					// reset button and note state
					let $dashNewCategory = $("#dash-new-category")
					$dashNewCategory.find("span").text("new category")
					$dashNewCategory.find("div").attr("data-icon", "ei-plus")

					$(".dash-cat-selected").removeClass("dash-cat-selected")
					$newCat.trigger("click")

					evilIcons.renderSprite()
					evilIcons.renderIcons()

					Modal.close()
				},
				error: function (jqxhr, status, error) {
					Modal.alert(JSON.parse(jqxhr.responseText).message, "bad")
				}
			}
		)
	}

	deleteCategory(catId, event) {
		$.ajax(
			"/api/cat/delete",
			{
				data: {
					category: catId
				},
				type: "POST",
				dataType: "json",
				success: function (data) {
					// switch back to all categories
					$("#dash-cat-all").trigger("click")
					$(event.currentTarget).closest(".dash-cat-block").fadeOut(function () {
						$(this).remove()
					})

					Modal.close()
				},
				error: function (jqxhr, status, error) {
					Modal.alert("Could not delete category:\n" + $.parseJSON(jqxhr.responseText).message, "bad")
				}
			}
		)

	}

	deleteNote(noteId, event) {
		$.ajax(
			"/api/note/delete",
			{
				data: {
					note: noteId
				},
				type: "POST",
				dataType: "json",
				success: function (data) {
					$("#dash-note-" + noteId).fadeOut(200, function (e) {
						$(this).remove()
					})

					$(".dash-note-block").first().trigger("click")

					Modal.close()
				},
				error: function (jqxhr, status, error) {
					// server returns "Invalid note." when it can't find such a note that belongs to the user.
					// the only reason a note entry like that would be on the dashboard is if it was deleted already elsewhere.
					if (JSON.parse(jqxhr.responseText).message == "Invalid note.") {
						window.location = "/my/dash"
					} else {
						Modal.alert("Error deleting note: " + JSON.parse(jqxhr.responseText).message, "bad")
						window.location.reload() // refresh note
					}
				}
			}
		)
	}

	showCategory(catId) {
		$.ajax(
			"/api/cat/view",
			{
				data: {
					category: catId
				},
				type: "POST",
				dataType: "json",
				success: (data)=> {
					// restore only the notes that were listed in the AJAX response
					for (let i = 0; i < data.notes.length; i++) {
						$("#dash-note-" + data.notes[i].id).show()
					}

				},
				error: function (jqxhr, status, error) {

				}
			}
		)
	}

	showNote(noteId){
		$.ajax(
			"/api/note/view",
			{
				data: {
					note: noteId
				},
				type: "POST",
				dataType: "json",
				success: (data) => {
					$("#dash-previewMessage").hide()
					this.sd.renderToId(data.note.content, "md-rendered")
					$("#dash-md-title").text(data.note.title).show()
				},
				error: function (jqxhr, status, error) {
					$("#md-rendered").hide()
					$("#dash-md-title").hide()
					$("#dash-previewMessage").text("Unable to view note. You may be logged out, or it may have been deleted.").addClass("bad").show()
				}
			}
		)
	}

	updateCategory(catId, newName, event) {
		$.ajax(
			"/api/cat/update",
			{
				data: {
					category: catId,
					name: newName
				},
				type: "POST",
				dataType: "json",
				success: (data)=> {
					$("#dash-notes").find(".dash-section-title").text(newName)
					$(event.currentTarget).closest(".dash-cat-block").find(".dash-cat-title").text(newName)
					$(event.currentTarget).closest(".dash-cat-block").trigger("click")
					Modal.close()
				},
				error: function (jqxhr, status, error) {
					Modal.alert($.parseJSON(jqxhr.responseText).message, "bad")
				}
			}
		)
	}
}

$(function() {
	let d = new Dashboard()
})
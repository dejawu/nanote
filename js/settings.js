$(function () {
	evilIcons.renderSprite()
	evilIcons.renderIcons()

	$("#settings-private").on("click", function () {
		saveSettings(function () {
			$("#settings-private-remark div, #settings-private-remark span").fadeOut(200, function () {
				if ($("#settings-private").prop("checked")) {
					$("#settings-private-remark div").attr("data-icon", "ei-lock")
					$("#settings-private-remark span").text("Private mode is on.")
				} else {
					$("#settings-private-remark div").attr("data-icon", "ei-unlock")
					$("#settings-private-remark span").text("Private mode is off.")
				}

				evilIcons.renderIcons()

				$("#settings-private-remark div").fadeIn(200)
				$("#settings-private-remark span").fadeIn(200)
			})
		})
	})

	$("#settings-password-old, #settings-password-new, #settings-password-confirm").on("keydown", function (e) {
		if (e.which == 13) {
			e.preventDefault()
			savePassword()
		}
	})

	$("#settings-password-submit").on("click", function (e) {
		e.preventDefault()
		savePassword()
	})

	$("#settings-apikey-generate").on("click", function (e) {
		e.preventDefault()
		$.ajax(
			"/api/auth/genapikey",
			{
				data: {},
				dataType: "json",
				method: "POST",
				success: function (data) {
					$("#settings-apikey-remark").text("Your key:")
					$("#settings-apikey-value").text(data.api_key)
					setMessage("New key generated. Any apps using the old key will no longer be able to access your account.", "ei-check", "good")
				},
				error: function (jqxhr, status, error) {
					setMessage($.parseJSON(jqxhr.responseText).message, "ei-exclamation", "bad")
				}
			}
		)
	})

	$("#settings-cancel-sub").on("click", function (e) {
		e.preventDefault()
		Modal.confirm("You are about to destroy your account and all associated data! Do you really want to continue?", "bad", () => {
			$.ajax(
				"/api/auth/endsub",
				{
					data: {
						password: $("#settings-cancel-password").val()
					},
					method: "POST",
					dataType: "json",
					success: function (data) {
						window.location = "/"
					},
					error: function (jqxhr, status, error) {
						setMessage($.parseJSON(jqxhr.responseText).message, "ei-exclamation", "bad")
					}
				}
			)
		})
	})

	function savePassword() {
		if ($("#settings-password-new").val() !== $("#settings-password-confirm").val()) {
			setMessage("Your new password confirmation doesn't match.", "ei-exclamation", "bad")
			return
		}

		if ($("#settings-password-old").val() == "" || $("#settings-password-new").val() == "" || $("#settings-password-confirm").val() == "") {
			setMessage("Please fill in all three fields to change your password.", "ei-exclamation", "bad")
			return
		}

		$.ajax(
			"/api/auth/setpass",
			{
				data: {
					"old": $("#settings-password-old").val(),
					"new": $("#settings-password-new").val()
				},
				dataType: "json",
				method: "POST",
				success: function (data) {
					setMessage("Password successfully updated.", "ei-check", "good")
				},
				error: function (jqxhr, status, error) {
					setMessage($.parseJSON(jqxhr.responseText).message, "ei-exclamation", "bad")
				}
			}
		)
	}

	function saveSettings(callback) {
		$.ajax(
			"/api/user/update",
			{
				data: {
					"private": $("#settings-private").prop("checked")
				},
				method: "POST",
				dataType: "json",
				success: function (data) {
					setMessage("Settings saved.", "ei-check", "good")
					callback()
				},
				error: function () {
					setMessage("Error saving settings.", "ei-exclamation", "bad")
				}
			}
		)
	}

	function setMessage(message, icon, className) {
		//
		// $("#settings-message").stop()
		// $("#settings-message").hide()
		//
		// $("#settings-message p").text(message)
		// $("#settings-message div").remove()
		// $("#settings-message").prepend('<div data-icon="' + icon + '"></div>')
		// $("#settings-message").attr("class", className)
		// evilIcons.renderIcons()
		//
		// $("html, body").animate({
		// 	scrollTop: 0
		// }, 450, function () {
		// 	$("#settings-message").delay(200).fadeIn()
		// })

		Modal.alert(message, className)
	}
})
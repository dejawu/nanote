$(function () {
	evilIcons.renderSprite()
	evilIcons.renderIcons()

	let sd = new MarkdownRenderer()

	let demos = [
		{
			t: "Write equations and formulas",
			c: "The amount of spooky $f$ of $n$ is given by the formula\n$$\nf(n) = (n)spooky(2n)me\n$$\nFor example,\n$$\nf(2) = 2spooky4me\n$$\n and \n$$\nf'(n) = \\frac{dy}{dn} = (1)spooky(2)me = spooky2me\n$$\nThis should be intuitive, as something must be spooky to me before it can be *too* spooky for me."
		},
		{
			t: "Make a grocery list",
			c: "- Bread\n\t- both wheat and white\n- Eggs\n- Milk\n- Butter\n- Bagels\n- Burner phone\n- Fake passports\n- Plane tickets"
		},
		{
			t: "Remember your favorite recipes",
			c: "#Concentrated dark matter\n##Grandpa's homestyle recipe\n- 2 parts plutonic quarks\n- 1 part Cesium\n- Bottled water\n- Mix well"
		},
		{
			t: "Compose Vogon poetry",
			c: "#My Favourite Bathtime Gurgles\n>Relax mind.\n\n>Relax body.\n\n>Relax.\n\n>Do not fall over.\n\n>You are a cloud.\n\n>You are raining.\n\n>Do not rain.\n\n>Move with the wind.\n\n>Apologise where necessary.\n\n- Grunthos the Flatulent"
		},
		{
			t: "Write a ransom note",
			c: "To whom it may concern:\n\nShould you wish to ever see your dear beloved son Harry again you must meet our following demands:\n\n- $10 million in unmarked bills\n\t- and they better be **crisp**!\n- A Rolls-Royce Phantom\n- 16 gold dubloons\n\nSincerely yours,\n\nThe kidnappers"
		},
		{
			t: "Remember those words to look up later",
			c: '- "fracking"\n- "who up click like"\n- how exactly does one "play yourself"?\n- "affluenza"\n- what is "vape" and where can I get some'
		}
	]

	let demoInd = Math.floor(Math.random() * (demos.length))

	let $mdRaw = $("#md-raw")
	let $mdRendered = $("#md-rendered")

	$mdRaw.on("demoComplete", function () {
		typeDemo(demos[demoInd].t, demos[demoInd].c)
		demoInd++

		if (demoInd >= demos.length) {
			demoInd = 0;
		}
	})

	$mdRaw.on("keyup keypress keydown", function (event) {
		event.preventDefault()
	})

	let cm = CodeMirror.fromTextArea(document.getElementById('md-raw'), {
		mode: "gfm",
		theme: "nanote",
		lineWrapping: true,
		extraKeys: {"Enter": "newlineAndIndentContinueMarkdownList"},
		matchBrackets: true,
		cursorHeight: 0.9,
	})

	function typeDemo(title, content) {
		let i = 0

		sd.renderToId($mdRaw.val(), "md-rendered")
		cm.setValue("")
		$("#index-demo-caption").text(title)

		while (i < content.length) {

			(function (i) {
				setTimeout(function () {
					cm.setValue(cm.getValue() + content[i])
					sd.renderToId(cm.getValue(), "md-rendered")

					if (i >= content.length - 1) {
						setTimeout(function () {
							$mdRaw.trigger("demoComplete")
						}, 2500)
					}

				}, (i * 50) + Math.round(Math.random() * 30))
			})(i)

			i++
		}
	}


	// start demo
	$mdRaw.trigger("demoComplete")
})
class Modal {
	static alert(message, className = "neutral") {
		$(".modal").hide()
		let $modal = $("#modal-alert")

		let handleEnterKey = function(event) {
			if(event.which == 13) {
				Modal.close()
			}

			$(window).off("keydown", handleEnterKey)
		}

		$modal.find(".modal-message")
			.text(message)
			.removeClass("good bad neutral")
			.addClass(className)

		$modal.show()

		$(window).on("keydown", handleEnterKey)

		if (!$("#modal-overlay").is(":visible")) {
			$("#modal-overlay").fadeIn()
		}
	}

	static confirm(message, className = "neutral", acceptCallback=undefined) {
		$(".modal").hide()
		let $modal = $("#modal-confirm")

		let handleEnterKey = function(event) {
			if(event.which == 13) {
				acceptCallback(event)
			}

			$(window).off("keydown", handleEnterKey)
		}

		$modal
			.find(".modal-message")
			.removeClass("good bad neutral")
			.text(message)

		// if message is red, then the action is probably serious, so make the confirm button red on hover
		if (className === "bad") {
			$modal.find(".modal-button-accept").addClass("bad")
		}

		$modal.show()

		$(window).on("keydown", handleEnterKey)

		let $modalButtonAccept = $modal.find(".modal-button-accept")
		$modalButtonAccept.off()

		if (typeof acceptCallback !== "undefined") {
			$modalButtonAccept.on("click", acceptCallback)
		}

		if (!$("#modal-overlay").is(":visible")) {
			$("#modal-overlay").fadeIn()
		}
	}

	static prompt(message, className = "neutral", callback=undefined) {
		let handleEnterKey = function(event) {
			if(event.which == 13) {
				callback($("#modal-prompt-input").val())
			}

			$(window).off("keydown", handleEnterKey)
		}
		$(".modal").hide()
		let $modal = $("#modal-prompt")

		$modal
			.find(".modal-message")
			.text(message)

		$modal.show()

		$("#modal-prompt-input").on("keydown", handleEnterKey)

		let $modalButtonAccept = $modal.find(".modal-button-accept")

		$modalButtonAccept.off()

		if (typeof callback !== "undefined") {
			$modalButtonAccept.on("click", (event)=> {
				event.preventDefault()
				callback($("#modal-prompt-input").val())
			})
		}

		if (!$("#modal-overlay").is(":visible")) {
			$("#modal-overlay").fadeIn(() => {
				$("#modal-prompt-input").focus()
			})
		}
	}

	static close() {
		$("#modal-overlay").fadeOut(200, ()=> {
			$(".modal").hide()
		})
		$("#modal-prompt-input").val('')
	}
}

// jQuery bindings for modals
$(function () {
	$(".modal-button-cancel").on("click", function (event) {
		event.preventDefault()
		Modal.close()
	})
})

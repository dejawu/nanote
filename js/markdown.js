class MarkdownRenderer {
	constructor() {
		this.md = window.markdownit({
			html: false,
			linkify: true,
		})

		this.mdPlugins = {
			// lazy headers, courtesy of https://github.com/Galadirith/markdown-it-lazy-headers
			lazyHeadersPlugin(md) {
				function heading(state, startLine, endLine, silent) {
					var ch, level, tmp, token,
						pos = state.bMarks[startLine] + state.tShift[startLine],
						max = state.eMarks[startLine]

					ch = state.src.charCodeAt(pos)

					if (ch !== 0x23/* # */ || pos >= max) {
						return false
					}

					// count heading level
					level = 1
					ch = state.src.charCodeAt(++pos)
					while (ch === 0x23/* # */ && pos < max && level <= 6) {
						level++
						ch = state.src.charCodeAt(++pos)
					}

					if (level > 6) {
						return false
					}

					if (silent) {
						return true
					}

					// Let's cut tails like '    ###  ' from the end of string
					max = state.skipCharsBack(max, 0x20, pos) // space
					tmp = state.skipCharsBack(max, 0x23, pos) // #
					if (tmp > pos && state.src.charCodeAt(tmp - 1) === 0x20/* space */) {
						max = tmp
					}

					state.line = startLine + 1

					token = state.push('heading_open', 'h' + String(level), 1)
					token.markup = '########'.slice(0, level)
					token.map = [startLine, state.line]

					token = state.push('inline', '', 0)
					token.content = state.src.slice(pos, max).trim()
					token.map = [startLine, state.line]
					token.children = []

					token = state.push('heading_close', 'h' + String(level), -1)
					token.markup = '########'.slice(0, level)

					return true
				}

				md.block.ruler.at('heading', heading, {
					alt: ['paragraph', 'reference', 'blockquote']
				})
			},

			// KaTeX plugin, courtesy of https://github.com/waylonflinn/markdown-it-katex/
			katexPlugin(md, options) {
				// Test if potential opening or closing delimieter
				// Assumes that there is a "$" at state.src[pos]
				function isValidDelim(state, pos) {
					var prevChar, nextChar,
						max = state.posMax,
						can_open = true,
						can_close = true

					prevChar = pos > 0 ? state.src.charCodeAt(pos - 1) : -1
					nextChar = pos + 1 <= max ? state.src.charCodeAt(pos + 1) : -1

					// Check non-whitespace conditions for opening and closing, and
					// check that closing delimeter isn't followed by a number
					if (prevChar === 0x20/* " " */ || prevChar === 0x09/* \t */ ||
						(nextChar >= 0x30/* "0" */ && nextChar <= 0x39/* "9" */)) {
						can_close = false
					}
					if (nextChar === 0x20/* " " */ || nextChar === 0x09/* \t */) {
						can_open = false
					}

					return {
						can_open: can_open,
						can_close: can_close
					}
				}

				function math_inline(state, silent) {
					var start, match, token, res, pos, esc_count

					if (state.src[state.pos] !== "$") {
						return false
					}

					res = isValidDelim(state, state.pos)
					if (!res.can_open) {
						if (!silent) {
							state.pending += "$"
						}
						state.pos += 1
						return true
					}

					// First check for and bypass all properly escaped delimieters
					// This loop will assume that the first leading backtick can not
					// be the first character in state.src, which is known since
					// we have found an opening delimiter already.
					start = state.pos + 1
					match = start
					while ((match = state.src.indexOf("$", match)) !== -1) {
						// Found potential $, look for escapes, pos will point to
						// first non escape when complete
						pos = match - 1
						while (state.src[pos] === "\\") {
							pos -= 1
						}

						// Even number of escapes, potential closing delimiter found
						if (((match - pos) % 2) == 1) {
							break
						}
						match += 1
					}

					// No closing delimter found.  Consume $ and continue.
					if (match === -1) {
						if (!silent) {
							state.pending += "$"
						}
						state.pos = start
						return true
					}

					// Check if we have empty content, ie: $$.  Do not parse.
					if (match - start === 0) {
						if (!silent) {
							state.pending += "$$"
						}
						state.pos = start + 1
						return true
					}

					// Check for valid closing delimiter
					res = isValidDelim(state, match)
					if (!res.can_close) {
						if (!silent) {
							state.pending += "$"
						}
						state.pos = start
						return true
					}

					if (!silent) {
						token = state.push('math_inline', 'math', 0)
						token.markup = "$"
						token.content = state.src.slice(start, match)
					}

					state.pos = match + 1
					return true
				}

				function math_block(state, start, end, silent) {
					var firstLine, lastLine, next, lastPos, found = false, token,
						pos = state.bMarks[start] + state.tShift[start],
						max = state.eMarks[start]

					if (pos + 2 > max) {
						return false
					}
					if (state.src.slice(pos, pos + 2) !== '$$') {
						return false
					}

					pos += 2
					firstLine = state.src.slice(pos, max)

					if (silent) {
						return true
					}
					if (firstLine.trim().slice(-2) === '$$') {
						// Single line expression
						firstLine = firstLine.trim().slice(0, -2)
						found = true
					}

					for (next = start; !found;) {

						next++

						if (next >= end) {
							break
						}

						pos = state.bMarks[next] + state.tShift[next]
						max = state.eMarks[next]

						if (pos < max && state.tShift[next] < state.blkIndent) {
							// non-empty line with negative indent should stop the list:
							break
						}

						if (state.src.slice(pos, max).trim().slice(-2) === '$$') {
							lastPos = state.src.slice(0, max).lastIndexOf('$$')
							lastLine = state.src.slice(pos, lastPos)
							found = true
						}

					}

					state.line = next + 1

					token = state.push('math_block', 'math', 0)
					token.block = true
					token.content = (firstLine && firstLine.trim() ? firstLine + '\n' : '')
						+ state.getLines(start + 1, next, state.tShift[start], true)
						+ (lastLine && lastLine.trim() ? lastLine : '')
					token.map = [start, state.line]
					token.markup = '$$'
					return true
				}

				// Default options

				options = options || {}

				// set KaTeX as the renderer for markdown-it-simplemath
				var katexInline = function (latex) {
					options.displayMode = false
					try {
						return katex.renderToString(latex, options)
					}
					catch (error) {
						if (options.throwOnError) {
							console.log(error)
						}
						return latex
					}
				}

				var inlineRenderer = function (tokens, idx) {
					return katexInline(tokens[idx].content)
				}

				var katexBlock = function (latex) {
					options.displayMode = true
					try {
						return "<p>" + katex.renderToString(latex, options) + "</p>"
					}
					catch (error) {
						if (options.throwOnError) {
							console.log(error)
						}
						return latex
					}
				}

				var blockRenderer = function (tokens, idx) {
					return katexBlock(tokens[idx].content) + '\n'
				}

				md.inline.ruler.after('escape', 'math_inline', math_inline)
				md.block.ruler.after('blockquote', 'math_block', math_block, {
					alt: ['paragraph', 'reference', 'blockquote', 'list']
				})
				md.renderer.rules.math_inline = inlineRenderer
				md.renderer.rules.math_block = blockRenderer
			},

			// highlight plugin, adapted from subscript plugin
			highlightPlugin(md) {
				function highlight(state) {
					let found, content, token
					let max = state.posMax
					let start = state.pos

					if (state.src[start] != '^') {
						return false;
					}

					if (start + 2 >= max) {
						return false;
					}

					state.pos = start + 1;

					while (state.pos < max) {
						if (state.src[state.pos] == '^') {
							found = true;
							break;
						}

						state.md.inline.skipToken(state);
					}

					if (!found || start + 1 === state.pos) {
						state.pos = start;
						return false;
					}

					content = state.src.slice(start + 1, state.pos);

					state.posMax = state.pos;
					state.pos = start + 1;

					token = state.push('highlight_open', 'span class="highlight"', 1);
					token.markup = '^';

					token = state.push('text', '', 0);
					token.content = content

					token = state.push('highlight_close', 'span', -1);
					token.markup = '^';

					state.pos = state.posMax + 1;
					state.posMax = max;

					return true;
				}

				md.inline.ruler.after("emphasis", "highlight", highlight)
			},
		}

		this.md.use(this.mdPlugins.lazyHeadersPlugin)
		this.md.use(this.mdPlugins.katexPlugin)
		this.md.use(this.mdPlugins.highlightPlugin)
	}

	renderToId(content, targetId) {
		let $target = $(`#${targetId}`)
		$target.html(this.md.render(content))
		$target.emojify()
	}
}

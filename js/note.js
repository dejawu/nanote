$(function () {
	let sd = new MarkdownRenderer()

	let $mdRendered = $("#md-rendered")
	$mdRendered.hide()
	sd.renderToId(htmlDecode($mdRendered.text()), "md-rendered")
	$mdRendered.show()
})

// courtesy of SO
function htmlDecode(input) {
	var doc = new DOMParser().parseFromString(input, "text/html");
	return doc.documentElement.textContent;
}
$(function () {
	$("#header-auth-login").on("click", function (event) {
		let $authLogin = $(event.currentTarget)
		let $authForm = $("#header-auth-form")

		event.preventDefault()
		if ($authForm.is(":visible")) {
			$authForm.fadeOut(200)
			$authLogin.text("Log in")
		} else {
			$authForm.fadeIn(200)
			$("#header-auth-username").focus()
			$authLogin.text("Close")
		}
	})

	$("#header-login").find("input").on("keydown", function (event) {
		if (event.which === 13) {
			event.preventDefault()
			attemptLogin()
		}
	})

	$("#header-auth-submit").on("click", function (event) {
		event.preventDefault()
		attemptLogin()
	})

	$("#header-login").on("submit", function (event) {
		event.preventDefault()
		attemptLogin()
	})

	function attemptLogin() {
		$.ajax(
			"/api/auth/login",
			{
				data: {
					username: $("#header-auth-username").val(),
					password: $("#header-auth-password").val()
				},
				type: "POST",
				dataType: "json",
				timeout: 5000,
				success: function (data) {
					window.location = "/my/dash"
				},
				error: function (jqxhr, status, error) {
					let errorMessage = ""
					switch (status) {
						case "timeout":
							errorMessage = "Could not reach server."
							break
						case "error":
							errorMessage = $.parseJSON(jqxhr.responseText).message
							break
						case "abort":
							errorMessage = "Connection was broken."
							break
						default:
							errorMessage = "Failed to create note."
					}
					$("#header-auth-message").hide().text(errorMessage).fadeIn(200)
				}
			}
		)
	}

	function attemptLostPass() {
		$.ajax(
			"/api/auth/lostpass",
			{
				data: {
					username: $("#header-lostpass-username").val()
				},
				type: "POST",
				dataType: "json",
				success: function (data) {
					$("#header-auth-message").hide().text("Check your email for recovery instructions!").fadeIn(200)
				},
				error: function (jqxhr, status, error) {
					let errorMessage = ""
					switch (status) {
						case "timeout":
							errorMessage = "Could not reach server."
							break
						case "error":
							errorMessage = $.parseJSON(jqxhr.responseText).message
							break
						case "abort":
							errorMessage = "Connection was broken."
							break
						default:
							errorMessage = "Failed to create note."
					}
					$("#header-auth-message").hide().text(errorMessage).fadeIn(200)

				}
			}
		)
	}

	$("#header-lostpass-button").on("click", function (event) {
		event.preventDefault()
		$("#header-login").hide()
		$("#header-lostpass").show()
		$("#header-lostpass-username").focus()
	})

	$("#header-lostpass-cancel").on("click", function (event) {
		event.preventDefault()
		$("#header-lostpass").hide()
		$("#header-login").show()
	})

	let $lostpassUsername = $("#header-lostpass-username")

	$lostpassUsername.on("keydown", function (event) {
		if (event.which === 13) {
			event.preventDefault()
			attemptLostPass()
		}
	})

	$lostpassUsername.on("submit", function (event) {
		event.preventDefault()
		attemptLostPass()
	})

	let $lostpassSubmit = $("#header-lostpass-submit")

	$lostpassSubmit.on("click", function (event) {
		event.preventDefault()
		attemptLostPass()
	})

	$("#header-auth-logout").on("click", function (event) {
		event.preventDefault()
		$.ajax(
			"/api/auth/logout",
			{
				type: "POST",
				dataType: "json",
				success: function (data) {
					window.location = "/"
				},
				error: function (jqxhr, status, error) {
					Modal.alert("Unable to log out. This may happen if you are offline.", "bad")
				}
			}
		)
	})
})
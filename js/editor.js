class Editor {
	constructor() {
		evilIcons.renderSprite()
		evilIcons.renderIcons()

		// jQuery objects
		
		this.$cache = {
			editorRight: $("#editor-right"),
			editorMdRaw: $("#editor-md-raw"),
			editorMdTitle: $("#editor-md-title"),
			editorMetaTitle: $("#editor-meta-title"),
			editorMetaSlug: $("#editor-meta-slug"),
			editorSlugLink: $("#editor-slug-link"),
			editorCategories: $("#editor-categories"),
			editorCategoriesNew: $("#editor-categories-new"),
			editorCategoriesMessage: $("#editor-categories-message")
		}


		this.mobileMode = ($(window).width() <= 800)

		this.sd = new MarkdownRenderer()

		this.cm = {}

		this.noteId = window.location.href.substring(window.location.href.lastIndexOf("/") + 1).replace("/", "").replace("#", "")

		this.states = {
			versionTimestamp: 0, // formerly noteUpdated. Working timestamp-version of the note
			typeTimeout: undefined,
			unsavedChanges: false,
			wasNewNote: undefined,
		}

		this.render = {
			blockCache: [], // cache of each paragraph as individual LaTeX blocks
			paragraphOffsets: [], // vertical pixel offset of each paragraph in md-raw
			mdLineWidth: 0, // how many characters wide the raw MD is, formerly charWidth
		}

		// category data
		this.cats = {
			all: [], // all categories this user has. Formerly userCats
			used: [], // categories this page has. Formerly noteCats
			unused: [], /// categories this page does not have. Formerly availCats
		}

		let noteMetaDeferred = $.ajax(
			"/api/note/view",
			{
				data: {
					note: this.noteId,
				},
				type: "POST",
				dataType: "json",
				success: (res) => {
					this.states.versionTimestamp = res.note.updated


					// Now we can load note content.
					this.$cache.editorMdRaw.val(res.note.content)


					this.$cache.editorMetaTitle.val(res.note.title)
					this.$cache.editorMetaSlug.val(res.note.slug)


					this.cm = CodeMirror.fromTextArea(document.getElementById('editor-md-raw'), {
						mode: "gfm",
						theme: "nanote",
						lineWrapping: true,
						extraKeys: {
							"Enter": "newlineAndIndentContinueMarkdownList",
							"Home": "goLineLeft",
							"End": "goLineRight",
						},
						matchBrackets: true,
						cursorHeight: 0.9,
					})

					this.updateNoteStats()

					// TODO this code is copy-pasted, find a more elegant way to achieve this
					let path = EditorUtil.validateURL(this.$cache.editorMetaSlug.val())

					let baseUrl = this.$cache.editorMetaSlug.data("baseurl")

					$("#editor-slug-url").val(baseUrl + path).data("path", path)

					// update page link
					this.$cache.editorMetaSlug.attr("href", baseUrl + path)

					$("#editor-privacy-" + res.note.privacy).prop("checked", true)

					if(res.note.privacy !== "private") {
						$("#editor-settings-slug").show()
					} else {
						$("#editor-settings-slug").hide()
					}

					window.document.title = "Nanote > Editing: " + res.note.title
					this.$cache.editorMdTitle.text(this.$cache.editorMetaTitle.val())
					this.setCatMessage("Click a category to add it, or start typing.", "ei-pencil", "neutral")

					if(this.$cache.editorMdRaw.val() === "") {
						$("#editor-meta-title").focus().select()
					}

					// set slug link
					this.$cache.editorSlugLink.attr("href", this.$cache.editorSlugLink.attr("data-baseurl") + this.$cache.editorMetaSlug.val())
				},
				error: (jqxhr, status, error) => {
					this.setNoteStatus("Error loading note.", "ei-exclamation", "bad")
				}
			}
		)

		// fetch categories
		let allCatsDeferred = $.ajax(
			"/api/user/cats",
			{
				type: "POST",
				dataType: "json",
				success: (res) => {
					// store userCats with cat_id as key
					for (let cat of res.cats) {
						this.cats.all[cat.cat_id] = cat
					}
				},
				error: () => {
					this.setNoteStatus("Error retrieving categories.", "ei-exclamation", "bad")
				}
			}
		)

		// fetch this note's categories
		let usedCatsDeferred = $.ajax(
			"/api/note/listcats",
			{
				data: {
					note: this.noteId
				},
				type: "POST",
				dataType: "json",
				success: (lCats) => {
					// store noteCats with cat_id as key
					for (let key in lCats.cats) {
						this.cats.used[lCats.cats[key].cat_id] = lCats.cats[key]
					}

					evilIcons.renderIcons()
				},
				error: () => {
					this.setNoteStatus("Failed to get note categories.", "ei-exclamation", "bad")
				}
			}
		)

		$.when(noteMetaDeferred, allCatsDeferred, usedCatsDeferred).done(() => {
			// Mobile initialization would go here

			$(() => {
				$("#editor-mobile-viewSwitch").on("click", () => {
					let $viewSwitchP = $("#editor-mobile-viewSwitch").find("> p:first")
					if (this.$cache.editorRight.position().left !== 0) {
						// show rendered

						this.renderPreview()

						this.$cache.editorRight.animate({
							left: "0px"
						})
						$viewSwitchP.html("&raquo;")
					} else {
						// hide rendered
						this.$cache.editorRight.animate({
							left: "100%"
						})
						$viewSwitchP.html("&laquo;")
					}
				})
			})

			// === CATEGORY DATA ===
			// iterate through user's categories, separating into used and unused
			for (let key in this.cats.all) {
				if (!this.cats.used.hasOwnProperty(key)) {
					this.cats.unused[key] = this.cats.all[key]
				}
			}
			// populate used and unused category fields
			for (let key in this.cats.unused) {
				$('<div></div>')
					.addClass("editor-category-avail")
					.data("catId", this.cats.unused[key].cat_id)
					.attr("id", "editor-category-avail-" + this.cats.unused[key].cat_id)
					.append(`<div data-icon="ei-plus"></div>`)
					.append(`<span>${this.cats.unused[key].cat_name}</span>`)
					.appendTo("#editor-categories-avail")
			}
			for (let key in this.cats.used) {
				$('<div></div>')
					.addClass("editor-category-entry")
					.data("catId", this.cats.used[key].cat_id)
					.append(`<div data-icon="ei-close-o"></div>`)
					.append(`<span>${this.cats.used[key].cat_name}</span>`)
					.appendTo("#editor-categories-entries")
			}

			// === CUSTOM EVENTS

			// typed on editor
			this.cm.on("change", () => {
				this.saveNote()

				// in mobile mode, we only update when the preview is opened.
				if(!this.mobileMode) {
					this.renderPreview()
				}
			})

			// === GLOBAL BINDINGS ===

			//Prevent losing unsaved changes
			$(window).bind("beforeunload", () => {
				if (this.states.unsavedChanges) {
					return "You have unsaved changes, which will be lost if you leave the note.\nAre you sure you want to leave?"
				}
			})

			// Eat "ctrl-S" and "cmd-S" inputs
			$(window).bind("keydown", (event) => {
				if ((event.ctrlKey || event.metaKey) && event.which === 83) {
					event.stopImmediatePropagation()
					event.preventDefault()
				}
			})

			// === EDITOR BINDINGS ===
			$("#editor-left").on("click", (event) => {
				if($(event.target).attr("id") === "editor-left") {
					this.cm.focus()
				}
			})

			$("#editor-meta-title").on("keydown, keyup", (event) => {
				if (event.which === 13) {
					this.cm.focus()
				}

				if (!EditorUtil.isCharacterKey(event.which)) {
					return
				}

				let $metaTitle = $("#editor-meta-title")
				let $mdTitle = this.$cache.editorMdTitle

				EditorUtil.validateURL($metaTitle.val())

				// Update rendered title
				if ($mdTitle.text() !== $metaTitle.val()) {
					$mdTitle.text($metaTitle.val())
				}

				this.saveNote()
			})

			// === SETTINGS BINDINGS ===

			$("#editor-settings-button").on("click", (event) => {
				event.preventDefault()
				$("#editor-settings").fadeIn(200)
				if (!this.mobileMode) {
					$("#editor-categories-new").focus()
				}
			})

			$("#editor-settings-close").on("click", (event) => {
				event.preventDefault()
				$("#editor-settings").fadeOut(200)
				this.cm.focus()
			})

			this.$cache.editorMetaSlug.on("keydown keyup", (event) => {
				if (!EditorUtil.isCharacterKey(event.which)) {
					return
				}

				let path = EditorUtil.validateURL(this.$cache.editorMetaSlug.val())
				let $slugLink = this.$cache.editorSlugLink

				let baseUrl = $slugLink.data("baseurl")

				$("#editor-slug-url").val(baseUrl + path).data("path", path)

				// update page link
				$slugLink.attr("href", baseUrl + path)

				this.saveNote()
			})

			// = CATEGORY BINDINGS =

			// == Add category to note ==
			this.$cache.editorCategories.on("click", ".editor-category-avail .icon", (event) => {
                const catId = $(event.currentTarget).closest(".editor-category-avail").data("catId");
                this.addCat(catId, $(event.currentTarget).closest(".editor-category-avail"))
			})

			// == Remove category from note ==
			this.$cache.editorCategories.on("click", ".editor-category-entry .icon", (event) => {
                const catId = $(event.currentTarget).closest(".editor-category-entry").data("catId");
                this.removeCat(catId, $(event.currentTarget).closest(".editor-category-entry"))
			})

			// == Search/filter categories ==
			this.$cache.editorCategoriesNew.on("keyup keydown", (e) => {
				let key;
				if(this.$cache.editorCategoriesNew.val().length === 0) {
					$(".editor-category-avail").show()
					if (!this.$cache.editorCategoriesMessage.find("span").text().startsWith("Click a category")) {
						// reset to default
						this.setCatMessage("Click a category to add it, or start typing.", "ei-pencil", "neutral")
					}
					return
				}

				// hide all
				$(".editor-category-avail").hide()

				let hasMatches = false

				// then show the ones that match
				for (key in this.cats.unused) {
					if (this.cats.unused[key].cat_name.toLowerCase().startsWith(this.$cache.editorCategoriesNew.val().toLowerCase())) {
						$("#editor-category-avail-" + this.cats.unused[key].cat_id).show()
						hasMatches = true
					}
				}

				// still gotta check within noteCats
				for (key in this.cats.used) {
					if (this.cats.used[key].cat_name.toLowerCase().startsWith(this.$cache.editorCategoriesNew.val().toLowerCase())) {
						hasMatches = true
					}
				}

				if (!hasMatches) {
					// allow user to create the category
					if (!this.$cache.editorCategoriesMessage.find("span").text().startsWith("Hit enter to create ")) {
						this.setCatMessage(`Hit enter to create and add the category: ${this.$cache.editorCategoriesNew.val()}`, "ei-plus", "neutral")
					} else {
						this.$cache.editorCategoriesMessage.find("span").text(`Hit enter to create and add new category: ${this.$cache.editorCategoriesNew.val()}`)
					}

					if (e.which === 13 && e.type === "keydown") {
						$.ajax(
							"/api/cat/create",
							{
								data: {
									name: $("#editor-categories-new").val()
								},
								dataType: "json",
								method: "POST",
								success: (data) => {
									// create category entries
									// add to this page

									this.cats.unused[data.id] = {
										cat_name: this.$cache.editorCategoriesNew.val(),
										cat_id: data.id
									}

									this.addCat(data.id)
								},
								error: (jqxhr, status, error) => {
									let errorMessage = ""
									switch (status) {
										case "timeout":
											errorMessage = "Could not reach server."
											break
										case "error":
											errorMessage = $.parseJSON(jqxhr.responseText).message
											break
										case "abort":
											errorMessage = "Connection was broken."
											break
										default:
											errorMessage = "Failed to create category."
									}
									this.setCatMessage(errorMessage, "ei-exclamation", "bad")
								}
							}
						)
					}
				} else if (!this.$cache.editorCategoriesMessage.find("span").text().startsWith("Click a category")) {
					// reset to default
					this.setCatMessage("Click a category to add it, or start typing.", "ei-pencil", "neutral")
				}
			})

			// privacy
			$("#editor-settings-privacy").find("input:radio").on("change", () => {
				let checkedVal = $("input[name=editor-meta-privacy]:checked").val()

				if (checkedVal !== "private") {
					// generate slug if none present
					if (this.$cache.editorMetaSlug.val() === "") {
						this.$cache.editorMetaSlug.val(Math.random().toString(36).substr(2, 16))
						this.$cache.editorMetaSlug.trigger("keydown")
					}

					$("#editor-settings-slug").slideDown()
				} else {
					$("#editor-settings-slug").slideUp()
				}

				this.saveNote(() => {
					this.setCatMessage("Note access set to " + checkedVal + ".", "ei-check", "good")
				}, undefined)
			})

			// == Delete note ==
			$("#editor-settings-deleteNote").on("click", () => {
				Modal.confirm("Really delete this note?", "bad", () => {
					this.states.unsavedChanges = false // we have to override this
					this.deleteNote()
				})
			})

			// once everything is done, update the page
			this.renderPreview()
			evilIcons.renderIcons()
		})
	}

	saveNote(callback, errCallback) {

		// overwrite existing timeout (to re-extend the time)

		if(typeof(this.states.typeTimeout) === 'undefined') {
			this.setNoteStatus('Saving...', 'ei-spinner-2', 'neutral')
		}
		clearTimeout(this.states.typeTimeout)
		this.states.typeTimeout = setTimeout(() => {
			doSaveNote(callback, errCallback)
			this.states.typeTimeout = undefined
		}, 750)

		// update word count
		this.updateNoteStats()

		let doSaveNote = (callback, errCallback) => {
			// accrue note data
			let noteData = {
				title: $("#editor-meta-title").val(),
				slug: $("#editor-slug-url").data("path"),
				content: this.cm.getValue(),
				privacy: $("input[name=editor-meta-privacy]:checked").val(),
				note: this.noteId,
				updated: this.states.versionTimestamp // for edit locking
			}

			$.ajax(
				"/api/note/update",
				{
					data: noteData,
					type: "POST",
					dataType: "json",
					timeout: 5000,
					success: (data) => {
						this.states.unsavedChanges = false
						let now = new Date()
						this.setNoteStatus(`All changes saved. (${now.toLocaleTimeString().toString()})`, "ei-check", "good")
						this.setCatMessage("Click a category to add it, or start typing.", "ei-pencil", "neutral")

						this.states.versionTimestamp = data.updated

						if (typeof callback !== "undefined") {
							callback()
						}
					},
					error: (jqxhr, status, error) => {
						// TODO implement these more specific errors for all other AJAX calls
						let errorMessage = ""
						switch (status) {
							case "timeout":
								errorMessage = "Could not reach server."
								break
							case "error":
								errorMessage = $.parseJSON(jqxhr.responseText).message
								break
							case "abort":
								errorMessage = "Connection was broken."
								break
							default:
								errorMessage = "Failed to save note."
						}

						this.setNoteStatus(errorMessage, "ei-exclamation", "bad")

						if (typeof errCallback === "function") {
							errCallback(errorMessage)
						}

						if ("latest" in JSON.parse(jqxhr.responseText)) {
							// offer to jump to later version
							$("#editor-status-message").append(`
							<p>
								You can <a href="#" class="bad" id="editor-version-override">save anyway</a> or <a href="#" class="bad" id="editor-version-latest">jump to the latest version.</a>
							</p>`)

							// create bindings (fresh)
							$("#editor-version-override, #editor-version-latest").unbind()
							$("#editor-version-override").on("click", () => {
								this.states.versionTimestamp = JSON.parse(jqxhr.responseText)["latest"]
								this.saveNote(undefined) // save note again
							})

							$("#editor-version-latest").on("click", function () {
								window.location.reload()
							})
						}
					}
				}
			)
		}
	}

	deleteNote(callback) {
		$.ajax(
			"/api/note/delete",
			{
				data: {
					note: this.noteId
				},
				type: "POST",
				dataType: "json",
				success: function (data) {
					window.location = "/my/dash"
				},
				error: (jqxhr, status, error) => {
					this.setNoteStatus($.parseJSON(jqxhr.responseText).message, "ei-exclamation", "bad")
				}
			}
		)
	}

	addCat(catId, oldEl) {
		$.ajax(
			"/api/note/addcat",
			{
				data: {
					note: this.noteId,
					category: catId
				},
				type: "POST",
				dataType: "json",
				timeout: 5000,
				success: (data) => {
					this.setCatMessage(`Added category ${this.cats.unused[catId].cat_name}.`, "ei-check", "good")
					$("#editor-categories-new").val("")
					$(".editor-category-avail").show()
					$(`<div></div>`)
						.addClass("editor-category-entry")
						.data("catId", catId)
						.append(`<div data-icon="ei-close-o"></div>`)
						.append(`<span>${this.cats.unused[catId].cat_name}</span>`)
						.appendTo($("#editor-categories-entries"))
						.hide()
						.fadeIn()
					evilIcons.renderIcons()
					if (typeof oldEl !== "undefined") {
						$(oldEl).fadeOut().remove()
					}

					this.cats.used[catId] = this.cats.unused[catId]
					delete this.cats.unused[catId]
				},
				error: (jqxhr, status, error) => {
					let errorMessage = ""
					switch (status) {
						case "timeout":
							errorMessage = "Could not reach server."
							break
						case "error":
							errorMessage = $.parseJSON(jqxhr.responseText).message
							break
						case "abort":
							errorMessage = "Connection was broken."
							break
						default:
							errorMessage = "Failed to save note."
					}
					// fade out the message so it'll flash every time
					this.setCatMessage(errorMessage, "ei-exclamation", "bad")
				}
			}
		)
	}

	removeCat(catId, oldEl) {
		$.ajax(
			"/api/note/removecat",
			{
				data: {
					note: this.noteId,
					category: catId
				},
				type: "POST",
				dataType: "json",
				timeout: 5000,
				success: (data) => {
					this.setCatMessage(`Removed category ${this.cats.used[catId].cat_name}.`, "ei-check", "good")
					$("#editor-categories-new").val("")
					let $catDiv = $(`<div></div>`)
					$catDiv.addClass("editor-category-avail")
						.data("catId", catId)
						.append(`<div data-icon="ei-plus"></div>`)
						.append(`<span>${this.cats.used[catId].cat_name}</span>`)
						.appendTo($("#editor-categories-avail"))
						.hide()
						.fadeIn()
					evilIcons.renderIcons()
					$(oldEl).fadeOut().remove()

					this.cats.unused[catId] = this.cats.used[catId]
					delete this.cats.used[catId]
				},
				error: (jqxhr, status, error) => {
					let errorMessage = ""
					switch (status) {
						case "timeout":
							errorMessage = "Could not reach server."
							break
						case "error":
							errorMessage = $.parseJSON(jqxhr.responseText).message
							break
						case "abort":
							errorMessage = "Connection was broken."
							break
						default:
							errorMessage = "Failed to save note."
					}
					// fade out the message so it'll flash every time
					this.setCatMessage(errorMessage, "ei-exclamation", "bad")
				}
			}
		)
	}

	setNoteStatus(message, icon, className) {
		let $status = $("#editor-status")
		$status.finish().hide()
		$status.find("div").remove()
		$status.removeClass().addClass(className)
		$status.append(`<div data-icon="${icon}"></div>`)

		evilIcons.renderIcons()
		$("#editor-status-message").text(message)
		$status.fadeIn(300)
	}

	// this message is used to display category information, but we sometimes use it for other things.
	setCatMessage(message, icon, className) {
		let $message = this.$cache.editorCategoriesMessage
		if (message === $message.find("span").text()) {
			return
		}
		$message.stop()
		$message.hide(0)
		$message.find("div").remove()
		$message.removeClass().addClass(className)
		$message.prepend(`<div data-icon="${icon}"></div>`)
		evilIcons.renderIcons()
		$message.find("span").text(message)
		$message.fadeIn(300)
	}

	// handles rendering duties
	renderPreview() {
		// if there already is a timeout waiting for the user to stop typing, get rid of it
		// we don't want it trying to save multiple times

		// render preview. TODO Only do this if on desktop site.

		let cmValue = this.cm.getValue()

		// insert spaces on empty lines within code blocks.
		// this makes code blocks appear continuous.
		let codeBlocks = this.cm.getValue().split("```").filter((str, i) => {
			return (i % 2) === 1
		})

		codeBlocks.forEach((block, i) => {
			cmValue = cmValue.replace(block,block.split('\n').map((line) => {
				return (line === '') ? " " : line
			}).join('\n'))
		})

		// break markdown by paragraph, denoted by double-line-break \n\n
		let brokeDown = cmValue.split("\n\n")
		// diff each of those chunks against the contents of corresponding rendering block
        let block = 0;

        if (typeof this.render.blockCache === 'undefined') {
			$("#md-rendered")
				.prepend(`<div id="md-block-${block}"></div>`)
			this.render.blockCache = []
			this.render.blockCache[0] = brokeDown[0]
		}

		for (block = 0; block < brokeDown.length; block++) {
			if (this.render.blockCache[block] !== brokeDown[block]) { // block has been updated
				this.render.blockCache[block] = brokeDown[block]

				// create block if it doesn't exist
				if (!$("#md-block-" + block).length) {
					if (block === 0) {
						$('<div id="md-block-' + block + '"></div>').prependTo("#md-rendered").addClass("md-block")
					} else {
						$("#md-block-" + (block - 1)).after('<div id="md-block-' + block + '"></div>').addClass("md-block")
					}
				}

				this.sd.renderToId(brokeDown[block], "md-block-" + block)
			}
		}

		// if rendercache still has elements but brokeDown doesn't, then some blocks have been deleted and we can remove those
		// TODO test how well this handles blocks in the middle being deleted
		for (; block < this.render.blockCache.length; block++) {
			$("#md-block-" + block).remove()
			this.render.blockCache[block] = undefined
		}

		if (this.$cache.editorMdTitle.text() !== this.$cache.editorMetaTitle.val()) {
			this.$cache.editorMdTitle.text($("#editor-meta-title").val())
		}
	}

	updateNoteStats() {
		let wordCount = this.cm.getValue().split(" ").length - 1
		$("#editor-settings-wordcount").text(wordCount)
		$("#editor-settings-charcount").text(this.cm.getValue().length)
		$("#editor-settings-pagecount").text((wordCount / 250).toFixed(2));

	}
}

class EditorUtil {
	static isCharacterKey(keyCode) {
		switch (keyCode) {
			// case 8: backspace
			// case 9: // tab
			case 16: // shift
			case 17: // ctrl
			case 18: // alt (both)
			case 20: // caps lock
			case 33: // pg up
			case 34: // pg down
			case 37: // arrow keys
			case 38:
			case 39:
			case 40:
			case 42: // doesn't exist?
			case 91: // ctrl or cmd key
			case 92: // right window key
			case 112: // Function keys F1 - F12
			case 113:
			case 114:
			case 115:
			case 116:
			case 117:
			case 118:
			case 119:
			case 120:
			case 121:
			case 122:
			case 123:
				return false
			default: // normal keys
				return true
		}
	}

	static validateURL(url) {
		return url.replace(/[^a-z0-9-]/gi, '-').replace(/--/g, "").toLowerCase()
	}

	static encodeHTMLEntities(str) {
        const buf = [];
        for (let i = str.length - 1; i >= 0; i--) {
			buf.unshift(['&#', str[i].charCodeAt(), ''].join(''))
		}
		return buf.join('')
	}
}


$(function () {
	// visibleInEditor custom selector
	$.extend($.expr[':'], {
		visibleInEditor: function (el) {
			return ($(el).offset().top - $(document).scrollTop() - $("#editor-right").offset().top + $(el).outerHeight()) > 0
		}
	});

	let editor = new Editor()
})